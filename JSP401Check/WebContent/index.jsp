<%@page import="sun.misc.BASE64Decoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		BASE64Decoder decoder = new sun.misc.BASE64Decoder();
		boolean authenticated = false;
		String authorization = (request.getHeader("authorization")+"").trim();
		System.out.println("authorization:" + authorization);
		if (authorization != null) {
			
			if (authorization.startsWith("Basic")) {
				authorization = authorization.replace("Basic", "").trim();
				byte[] bytes = decoder.decodeBuffer(authorization);
				String[] params = new String(bytes,"UTF-8").split(":");
				System.out.println("userName:" + params[0]);
				System.out.println("password:" + params[1]);
				authenticated = params[0].equals("张三") && params[1].equals("123");

			} else if (authorization.startsWith("Digest")) {
				
				String userName = authorization.substring(authorization
						.indexOf("username="));
				userName = userName.substring("username=\"".length());
				userName = userName.substring(0, userName.indexOf('"'));
				String password = authorization.substring(authorization
						.indexOf("response="));
				password = password.substring("response=\"".length());
				password = password.substring(0, password.indexOf('"'));
				authenticated = userName.equals("abc") && password.equals("3cf1135d3b8e20dd9272d06288569a56");
			}
		}

		if (!authenticated) {
			// response.addHeader("WWW-Authenticate","Digest realm=\"Tomcat Manager Application\"");
			response.addHeader("WWW-Authenticate",
					"Basic realm=\"BSM DataCenter\"");
			response.sendError(401, "Unauthorized");
		} else {
			out.println("hello abc");
		}
	%>
</body>
</html>