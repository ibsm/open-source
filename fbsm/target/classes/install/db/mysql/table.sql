--系统用户表
CREATE TABLE `cmdb_sys_user` (
	`id`  varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' ,
	`name`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
	`pwd`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
	`state`  int(1) NULL DEFAULT NULL ,
	`note`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
	`create_user`  varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' ,
	`create_time`  datetime NOT NULL ,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;
--系统菜单表
CREATE TABLE `cmdb_sys_menu` (
	`id`  varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,f
	`name`  varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
	`note`  varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
	`url`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
	`icon`  varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
	`directory_nav`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ,
	`parent_id`  varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
	`has_child`  int(1) NOT NULL DEFAULT 0 COMMENT '0表示没有子菜单，1表示有子菜单，数据发生变更时自行维护' ,
	`sort`  int(11) NOT NULL DEFAULT 0 ,
	`state`  int(1) NOT NULL ,
	`create_time`  datetime NOT NULL ,
	`create_user`  varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci;
