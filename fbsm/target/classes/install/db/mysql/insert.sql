--系统用户表
INSERT INTO `cmdb_sys_user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1', '初始化数据，系统超级管理员', '1', '2015-10-18 23:11:21');
--初始化菜单
INSERT INTO `cmdb_sys_menu` (`id`, `name`, `note`, `url`, `icon`, `directory_nav`, `parent_id`, `has_child`, `sort`, `state`, `create_time`, `create_user`) VALUES ('1', '系统配置', NULL, NULL, NULL, NULL, '0', '1', '0', '1', '2015-10-18 12:01:23', '1');
INSERT INTO `cmdb_sys_menu` (`id`, `name`, `note`, `url`, `icon`, `directory_nav`, `parent_id`, `has_child`, `sort`, `state`, `create_time`, `create_user`) VALUES ('2', '权限架构', NULL, NULL, NULL, NULL, '1', '0', '0', '1', '2015-10-18 15:50:45', '1');
INSERT INTO `cmdb_sys_menu` (`id`, `name`, `note`, `url`, `icon`, `directory_nav`, `parent_id`, `has_child`, `sort`, `state`, `create_time`, `create_user`) VALUES ('3', '用户管理', NULL, NULL, NULL, NULL, '2', '0', '0', '1', '2015-12-07 03:36:29', '1');
INSERT INTO `cmdb_sys_menu` (`id`, `name`, `note`, `url`, `icon`, `directory_nav`, `parent_id`, `has_child`, `sort`, `state`, `create_time`, `create_user`) VALUES ('4', '系统角色', '<ol class=\"breadcrumb\"><li><a href=\"#\">系统配置</a></li><li><a href=\"#\">权限架构</a></li><li class=\"active\">系统角色</li></ol>', '#!sys/permission/role/sysRole.do', NULL, NULL, '2', '0', '0', '1', '2015-10-18 15:53:03', '1');
INSERT INTO `cmdb_sys_menu` (`id`, `name`, `note`, `url`, `icon`, `directory_nav`, `parent_id`, `has_child`, `sort`, `state`, `create_time`, `create_user`) VALUES ('5', '系统菜单', NULL, NULL, NULL, NULL, '2', '0', '0', '1', '2015-10-18 15:53:36', '1');

