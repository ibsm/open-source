package com.hm.wflow.service;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipInputStream;

import net.sf.json.JSONObject;

import org.activiti.engine.repository.DeploymentQuery;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.PageSet;
import com.hm.common.config.Configuration;
import com.hm.common.util.DataUtil.DateUtil;
import com.hm.common.util.DataUtil.StringUtil;
import com.hm.wflow.entity.Deployment;

@Service
public class DeploymentProcessDefineService extends BaseService {

	public void deploymentProcessDefine(Deployment deployment) throws Exception {
		repositoryService
				.createDeployment()
				.name(deployment.getName())
				.category(deployment.getCategory())
				.addZipInputStream(new ZipInputStream(new FileInputStream(Configuration.uploadPath()+"/"+deployment.getPath())))
				.deploy();
	}

	public PageSet queryDeployProcessList(Deployment domain, PageSet pageSet) {
		DeploymentQuery query = repositoryService.createDeploymentQuery();
		if(!StringUtil.isNullEmpty(domain.getName())){
			query = query.deploymentName(domain.getName());
		}
		List<org.activiti.engine.repository.Deployment> deployments = query.listPage((pageSet.getPage() - 1)*pageSet.getPageSize(), 
				pageSet.getPage()*pageSet.getPageSize());
		List<JSONObject> items = new ArrayList<JSONObject>();
		for (org.activiti.engine.repository.Deployment deployment : deployments) {
			JSONObject item = new JSONObject();
			item.put("id", deployment.getId());
			item.put("name", deployment.getName());
			item.put("category", transDictInsName4id(deployment.getCategory()));
			item.put("deploymentTime", DateUtil.yyyyMMdd(deployment.getDeploymentTime()));
			
			items.add(item);
		}
		pageSet.setRows(items);
		
		repositoryService.createDeploymentQuery();
		if(!StringUtil.isNullEmpty(domain.getName())){
			query = query.deploymentName(domain.getName());
		}
		pageSet.setTotal(Integer.parseInt(query.count()+""));
		return pageSet;
	}

	public void deleteDeployProcess(Deployment deployment) {
		repositoryService.deleteDeployment(deployment.getId(), true);//级联删除
	}

	public PageSet queryDeployProcessListByDeployment(Deployment deployment,PageSet pageSet) {
		ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
		if(!StringUtil.isNullEmpty(deployment.getId())){
			processDefinitionQuery.deploymentId(deployment.getId());
		}
		List<ProcessDefinition> definitions = processDefinitionQuery.listPage((pageSet.getPage() - 1)*pageSet.getPageSize(), 
				pageSet.getPage()*pageSet.getPageSize());
		List<JSONObject> items = new ArrayList<JSONObject>();
		for (ProcessDefinition processDefinition : definitions) {
			JSONObject item = new JSONObject();
			item.put("id", processDefinition.getId());
			item.put("name", processDefinition.getName());
			item.put("key", processDefinition.getKey());
			item.put("version", processDefinition.getVersion());
			item.put("resourceName", processDefinition.getResourceName());
			item.put("diagramResourceName", processDefinition.getDiagramResourceName());
			item.put("deploymentId", processDefinition.getDeploymentId());
			
			items.add(item);
		}
		pageSet.setRows(items);
		
		processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
		if(!StringUtil.isNullEmpty(deployment.getId())){
			processDefinitionQuery = processDefinitionQuery.deploymentId(deployment.getId());
		}
		pageSet.setTotal(Integer.parseInt(processDefinitionQuery.count()+""));
		return pageSet;
	}

	public InputStream outPutFlowPic(String deploymentId, String resourceName) {
		return repositoryService.getResourceAsStream(deploymentId, resourceName);
	}

}
