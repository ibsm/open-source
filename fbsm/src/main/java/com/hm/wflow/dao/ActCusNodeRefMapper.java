package com.hm.wflow.dao;

import com.hm.wflow.entity.ActCusNodeRef;

public interface ActCusNodeRefMapper {
    int deleteByNodeId(String id);

    int insert(ActCusNodeRef record);

    ActCusNodeRef selectActCusNodeRef(ActCusNodeRef record);

    int updateByPrimaryKey(ActCusNodeRef record);
}