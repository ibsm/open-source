package com.hm.monitor.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Swap;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.config.Configuration;
import com.hm.common.util.DataUtil;
import com.hm.common.util.DataUtil.Decimal;
import com.hm.common.util.R;
import com.hm.monitor.entity.FtlChart;
import com.hm.monitor.phy.res.Memory;
import com.hm.monitor.phy.su.PhyDeviceMonitor;

@Service
public class PhyMemoryInfoService extends BaseService {

	private PhyDeviceMonitor phyDeviceMonitor = new PhyDeviceMonitor();
	
	public List<String> getMemoryChartInfos() throws Exception {
		List<String> list = new ArrayList<String>();
		float used = 0,free = 0;
		
		Memory memory = phyDeviceMonitor.getMemory();
		//物理内存信息
		Mem mem = memory.getPhysicalMemory();
		{
			used = mem.getUsed();
			free = mem.getFree();
			FtlChart ftlChart = new FtlChart("物理内存使用情况", null,"GB");
			DecimalFormat format = new DecimalFormat("0.00");//格式化小数
			// 图表描述
			ftlChart.set("已用内存", format.format(DataUtil.Decimal.k2gb(used)));
			ftlChart.set("剩余内存", format.format(DataUtil.Decimal.k2gb(free)));
			
			list.add(new DataUtil.FtlFactory().build(Configuration.get(R.Charts.PIE)).process(ftlChart.get()));
		}
		//交换区内存
		Swap swap = memory.getWarpMemory();
		{
			used = swap.getUsed();
			free = swap.getFree();
			FtlChart ftlChart = new FtlChart("交换区内存使用情况", null,"GB");
			DecimalFormat format = new DecimalFormat("0.00");//格式化小数
			// 图表描述
			ftlChart.set("已用内存", format.format(DataUtil.Decimal.k2gb(used)));
			ftlChart.set("剩余内存", format.format(DataUtil.Decimal.k2gb(free)));
			
			list.add(new DataUtil.FtlFactory().build(Configuration.get(R.Charts.PIE)).process(ftlChart.get()));
		}
		
		return list;
	}

	public Map<String,Object> disposeWarpMemory(Swap warpMemory) {
		DecimalFormat format = new DecimalFormat("0.00");//格式化小数
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("free", format.format(Decimal.k2gb(warpMemory.getFree()))+"GB");
		map.put("pageIn", format.format(Decimal.kb2gb(warpMemory.getPageIn()))+"MB");
		map.put("pageOut", format.format(Decimal.kb2gb(warpMemory.getPageOut()))+"MB");
		map.put("total", format.format(Decimal.k2gb(warpMemory.getTotal()))+"GB");
		map.put("used", format.format(Decimal.k2gb(warpMemory.getUsed()))+"GB");
		
		return map;
	}

	public Map<String,Object> disposePhysicalMemory(Mem physicalMemory) {
		DecimalFormat format = new DecimalFormat("0.00");//格式化小数
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("actualFree", format.format(Decimal.k2gb(physicalMemory.getActualFree()))+"GB");
		map.put("actualUsed", format.format(Decimal.k2gb(physicalMemory.getActualUsed()))+"GB");
		map.put("free", format.format(Decimal.k2gb(physicalMemory.getFree()))+"GB");
		map.put("freePercent", format.format(physicalMemory.getFreePercent())+"%");
		map.put("ram", format.format(Decimal.kb2mb(physicalMemory.getRam()))+"MB");
		map.put("usedPercent", format.format(physicalMemory.getUsedPercent())+"%");
		map.put("total", format.format(Decimal.k2gb(physicalMemory.getTotal()))+"GB");
		map.put("used", format.format(Decimal.k2gb(physicalMemory.getUsed()))+"GB");
		
		return map;
	}

	
}
