package com.hm.monitor.entity;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author shishun.wang
 * @date 7:50:25 PM Jan 9, 2016
 * @version 1.0
 * @describe 图标模板
 */
public class FtlChart {

	private String chartId;
	
	private String title01;
	
	private String title02;
	
	private String css;
	
	private String style;
	
	private String dataFrom;
	
	private JSONArray dataColumns;
	
	private JSONArray chartDatas;
	
	private String unit;
	
	public FtlChart(String title01,String title02,String unit) {
		this.chartId = UUID.randomUUID().toString().replace("-", "");
		this.title01 = title01;
		this.title02 = null != title02?title02:"";
		this.unit = unit;
		this.dataFrom = "数据来自fbsm";
		this.dataColumns = new JSONArray();
		this.chartDatas = new JSONArray();
		
		this.style = "padding:5px 5px 0px 15px;height: 300px;";
		this.css = "panel-body";
	}

	public void set(String key,Object value) throws Exception{
		dataColumns.put(key);
		JSONObject item = new JSONObject();
		item.put("name", key);
		item.put("value", value);
		chartDatas.put(item);
	}
	
	public Map<String,String> get(){
		Map<String,String> mapper = new HashMap<String, String>();
		
		mapper.put("chartId", this.getChartId());
		mapper.put("style", this.getStyle());
		mapper.put("css", this.getCss());
		mapper.put("title01", this.getTitle01());
		mapper.put("title02", this.getTitle02());
		mapper.put("dataFrom", this.getDataFrom());
		mapper.put("dataColumns", this.getDataColumns().toString());
		mapper.put("unit", this.getUnit());
		mapper.put("chartDatas", this.getChartDatas().toString());
		return mapper;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getChartId() {
		return chartId;
	}

	public void setChartId(String chartId) {
		this.chartId = chartId;
	}

	public String getTitle01() {
		return title01;
	}

	public void setTitle01(String title01) {
		this.title01 = title01;
	}

	public String getTitle02() {
		return title02;
	}

	public void setTitle02(String title02) {
		this.title02 = title02;
	}

	public String getCss() {
		return css;
	}

	public void setCss(String css) {
		this.css = css;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getDataFrom() {
		return dataFrom;
	}

	public void setDataFrom(String dataFrom) {
		this.dataFrom = dataFrom;
	}

	public JSONArray getDataColumns() {
		return dataColumns;
	}

	public void setDataColumns(JSONArray dataColumns) {
		this.dataColumns = dataColumns;
	}

	public JSONArray getChartDatas() {
		return chartDatas;
	}

	public void setChartDatas(JSONArray chartDatas) {
		this.chartDatas = chartDatas;
	}
	
}
