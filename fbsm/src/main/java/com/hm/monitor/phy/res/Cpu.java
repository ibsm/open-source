package com.hm.monitor.phy.res;

import java.util.List;

import org.hyperic.sigar.CpuInfo;
import org.hyperic.sigar.CpuPerc;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe cpu 资源 
 */
public class Cpu {

	/**
	 * cpu个数
	 */
	private Integer cpuTotla;
	
	/**
	 * CPU的总量（单位：HZ）及CPU的相关信息
	 */
	private List<CpuInfo> cpuInfos;
	
	/**
	 * CPU的用户使用量、系统使用剩余量、总的剩余量、总的使用占用量等（单位：100%）
	 */
	private List<CpuUsageRateFormat> cpuUsageRateFormats;
	
	/**
	 * CPU的用户使用量、系统使用剩余量、总的剩余量、总的使用占用量等
	 */
	private List<CpuPerc> cpuPercs;
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe CPU的总量（单位：HZ）及CPU的相关信息
	 */
	public static class CpuTotalInfo{
		
		/**
		 * CPU的总量MHz
		 */
		private Integer mhz;
		
		/**
		 * cpu厂商
		 */
		private String vendor;
		
		/**
		 * cpu种类
		 */
		private String model;
		
		/**
		 * 缓冲存储器数量
		 */
		private Long cacheSize;
		
		public CpuTotalInfo (CpuInfo info){
			this.mhz = info.getMhz();
			this.vendor = info.getVendor();
			this.model = info.getModel();
			this.cacheSize = info.getCacheSize();
		}

		public Integer getMhz() {
			return mhz;
		}

		public void setMhz(Integer mhz) {
			this.mhz = mhz;
		}

		public String getVendor() {
			return vendor;
		}

		public void setVendor(String vendor) {
			this.vendor = vendor;
		}

		public String getModel() {
			return model;
		}

		public void setModel(String model) {
			this.model = model;
		}

		public Long getCacheSize() {
			return cacheSize;
		}

		public void setCacheSize(Long cacheSize) {
			this.cacheSize = cacheSize;
		}

		@Override
		public String toString() {
			return "CpuTotalInfo [mhz=" + mhz + ", vendor=" + vendor
					+ ", model=" + model + ", cacheSize=" + cacheSize + "]";
		}
		
	}
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe CPU的用户使用量、系统使用剩余量、总的剩余量、总的使用占用量等（单位：100%）
	 */
	public static class CpuUsageRateFormat{
		
		/**
		 * 用户使用率
		 */
		private String user;
		
		/**
		 * 系统使用率
		 */
		private String sys;
		
		/**
		 * 当前等待率
		 */
		private String wait;
		
		private String nice;
		
		/**
		 * 当前空闲率
		 */
		private String idle;
		
		/**
		 * 总的使用率
		 */
		private String combined;
		
		public CpuUsageRateFormat(CpuPerc perc){
			this.user = CpuPerc.format(perc.getUser());
			this.sys = CpuPerc.format(perc.getSys());
			this.wait = CpuPerc.format(perc.getWait());
			this.nice = CpuPerc.format(perc.getNice());
			this.idle = CpuPerc.format(perc.getIdle());
			this.combined = CpuPerc.format(perc.getCombined());
		}

		public String getUser() {
			return user;
		}

		public void setUser(String user) {
			this.user = user;
		}

		public String getSys() {
			return sys;
		}

		public void setSys(String sys) {
			this.sys = sys;
		}

		public String getWait() {
			return wait;
		}

		public void setWait(String wait) {
			this.wait = wait;
		}

		public String getNice() {
			return nice;
		}

		public void setNice(String nice) {
			this.nice = nice;
		}

		public String getIdle() {
			return idle;
		}

		public void setIdle(String idle) {
			this.idle = idle;
		}

		public String getCombined() {
			return combined;
		}

		public void setCombined(String combined) {
			this.combined = combined;
		}

		@Override
		public String toString() {
			return "CpuUsageRateFormat [user=" + user + ", sys=" + sys
					+ ", wait=" + wait + ", nice=" + nice + ", idle=" + idle
					+ ", combined=" + combined + "]";
		}
		
	}
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe CPU的用户使用量、系统使用剩余量、总的剩余量、总的使用占用量等
	 */
	public static class CpuUsageRate{
		
		/**
		 * 用户使用率
		 */
		private Double user;
		
		/**
		 * 系统使用率
		 */
		private Double sys;

		/**
		 * 当前等待率
		 */
		private Double wait;
		
		private Double nice;

		/**
		 * 当前空闲率
		 */
		private Double idle;
		
		/**
		 * 总的使用率
		 */
		private Double combined;

		public CpuUsageRate(CpuPerc perc){
			this.user = perc.getUser();
			this.sys = perc.getSys();
			this.wait = perc.getWait();
			this.nice = perc.getNice();
			this.idle = perc.getIdle();
			this.combined = perc.getCombined();
		}
		
		public Double getUser() {
			return user;
		}

		public void setUser(Double user) {
			this.user = user;
		}

		public Double getSys() {
			return sys;
		}

		public void setSys(Double sys) {
			this.sys = sys;
		}

		public Double getWait() {
			return wait;
		}

		public void setWait(Double wait) {
			this.wait = wait;
		}

		public Double getNice() {
			return nice;
		}

		public void setNice(Double nice) {
			this.nice = nice;
		}

		public Double getIdle() {
			return idle;
		}

		public void setIdle(Double idle) {
			this.idle = idle;
		}

		public Double getCombined() {
			return combined;
		}

		public void setCombined(Double combined) {
			this.combined = combined;
		}

		@Override
		public String toString() {
			return "CpuUsageRate [user=" + user + ", sys=" + sys + ", wait="
					+ wait + ", nice=" + nice + ", idle=" + idle
					+ ", combined=" + combined + "]";
		}
		
	}

	public List<CpuUsageRateFormat> getCpuUsageRateFormats() {
		return cpuUsageRateFormats;
	}

	public void setCpuUsageRateFormats(List<CpuUsageRateFormat> cpuUsageRateFormats) {
		this.cpuUsageRateFormats = cpuUsageRateFormats;
	}

	public List<CpuInfo> getCpuInfos() {
		return cpuInfos;
	}

	public void setCpuInfos(List<CpuInfo> cpuInfos) {
		this.cpuInfos = cpuInfos;
	}

	public List<CpuPerc> getCpuPercs() {
		return cpuPercs;
	}

	public void setCpuPercs(List<CpuPerc> cpuPercs) {
		this.cpuPercs = cpuPercs;
	}

	public Integer getCpuTotla() {
		return cpuTotla;
	}

	public void setCpuTotla(Integer cpuTotla) {
		this.cpuTotla = cpuTotla;
	}

	@Override
	public String toString() {
		return "Cpu [cpuTotla=" + cpuTotla + ", cpuInfos=" + cpuInfos
				+ ", cpuUsageRateFormats=" + cpuUsageRateFormats
				+ ", cpuPercs=" + cpuPercs + "]";
	}

}
