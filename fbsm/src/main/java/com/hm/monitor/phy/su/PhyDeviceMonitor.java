package com.hm.monitor.phy.su;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hyperic.sigar.CpuInfo;
import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.FileSystem;
import org.hyperic.sigar.NetFlags;
import org.hyperic.sigar.NetInterfaceConfig;
import org.hyperic.sigar.OperatingSystem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

import com.hm.monitor.phy.Monitor;
import com.hm.monitor.phy.res.Cpu;
import com.hm.monitor.phy.res.Cpu.CpuUsageRateFormat;
import com.hm.monitor.phy.res.Disk;
import com.hm.monitor.phy.res.Disk.DiskInfo;
import com.hm.monitor.phy.res.Memory;
import com.hm.monitor.phy.res.Network;
import com.hm.monitor.phy.res.Network.NetworkFlowInfo;
import com.hm.monitor.phy.res.PlatformInfo;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 物理设备监控
 */
public class PhyDeviceMonitor extends Monitor {

	private Sigar sigar = new Sigar();

	public Cpu getCpu(boolean cpuFormat) throws Exception {
		Cpu cpu = new Cpu();

		CpuInfo[] cpuInfoList = sigar.getCpuInfoList();
		{// CPU的总量（单位：HZ）及CPU的相关信息。
			cpu.setCpuInfos(Arrays.asList(cpuInfoList));
		}

		CpuPerc[] cpuPercList = sigar.getCpuPercList();
		// CPU的用户使用量、系统使用剩余量、总的剩余量、总的使用占用量等（单位：100%）
		if (cpuFormat) {// 有单位
			List<CpuUsageRateFormat> cpuUsageRateFormats = new ArrayList<Cpu.CpuUsageRateFormat>();
			for (int i = 0; i < cpuPercList.length; i++) {
				CpuPerc perc = cpuPercList[i];

				CpuUsageRateFormat cpuUsageRateFormat = new CpuUsageRateFormat(
						perc);
				cpuUsageRateFormats.add(cpuUsageRateFormat);
			}
			cpu.setCpuUsageRateFormats(cpuUsageRateFormats);
		} else {// 无单位
			cpu.setCpuPercs(Arrays.asList(cpuPercList));
		}

		// cpu个数
		cpu.setCpuTotla(cpuInfoList.length);

		return cpu;
	}

	public Memory getMemory() throws Exception {
		Memory memory = new Memory();
		memory.setPhysicalMemory(sigar.getMem());
		memory.setWarpMemory(sigar.getSwap());

		return memory;
	}

	public PlatformInfo getPlatformInfo() throws Exception {
		PlatformInfo platformInfo = new PlatformInfo();
		platformInfo.setOsName(getPlatformName());
		platformInfo.setOperatingSystem(OperatingSystem.getInstance());
		platformInfo.setWhos(Arrays.asList(sigar.getWhoList()));

		return platformInfo;
	}

	public Disk getDisk() throws Exception {
		Disk disk = new Disk();

		List<DiskInfo> diskInfos = new ArrayList<Disk.DiskInfo>();
		{
			List<FileSystem> fileSystems = Arrays.asList(sigar.getFileSystemList());
			for (FileSystem fileSystem : fileSystems) {
				DiskInfo diskInfo = new DiskInfo();
				diskInfo.setFileSystem(fileSystem);
				try {
					diskInfo.setFileSystemUsage(sigar.getFileSystemUsage(fileSystem.getDirName()));
				} catch (SigarException e) {
					e.printStackTrace();
				} finally{
					diskInfos.add(diskInfo);
				}
			}
		}

		disk.setDiskInfos(diskInfos);
		return disk;
	}

	public Network getNetwork() throws Exception {
		Network network = new Network();
		String domainName;
		try {
			domainName = getDomainName();
		} catch (Exception e) {
			domainName = "";
		}
		network.setDomainName(domainName);
		network.setLocalIp(getLocalIp());
		network.setLocalMac(getLocalMac());
		network.setNetInfo(sigar.getNetInfo());
		
		List<NetworkFlowInfo> networkFlowInfos = new ArrayList<Network.NetworkFlowInfo>();
		List<String> list = Arrays.asList(sigar.getNetInterfaceList());
		for (String name : list) {
			NetworkFlowInfo networkFlowInfo = new NetworkFlowInfo();
			NetInterfaceConfig netInterfaceConfig = sigar.getNetInterfaceConfig(name);
			networkFlowInfo.setNetInterfaceConfig(netInterfaceConfig);
			if (!((netInterfaceConfig.getFlags() & 1L) <= 0L)) {
				networkFlowInfo.setNetInterfaceStat(sigar.getNetInterfaceStat(name));
			}
			
			networkFlowInfos.add(networkFlowInfo);
		}
		network.setNetworkFlowInfos(networkFlowInfos);

		return network;
	}

	private String getLocalMac() {
		try {
			String[] ifaces = sigar.getNetInterfaceList();
			String hwaddr = null;
			for (int i = 0; i < ifaces.length; i++) {
				NetInterfaceConfig cfg = sigar.getNetInterfaceConfig(ifaces[i]);
				if (NetFlags.LOOPBACK_ADDRESS.equals(cfg.getAddress())
						|| (cfg.getFlags() & NetFlags.IFF_LOOPBACK) != 0
						|| NetFlags.NULL_HWADDR.equals(cfg.getHwaddr())) {
					continue;
				}
				/*
				 * 如果存在多张网卡包括虚拟机的网卡，默认只取第一张网卡的MAC地址，如果要返回所有的网卡（包括物理的和虚拟的）
				 * 则可以修改方法的返回类型为数组或Collection ，通过在for循环里取到的多个MAC地址。
				 */
				hwaddr = cfg.getHwaddr();
				break;
			}
			return hwaddr != null ? hwaddr : null;
		} catch (Exception e) {
			return null;
		}
	}

	private String getLocalIp() {
		String address = null;
		try {
			address = InetAddress.getLocalHost().getHostAddress();
			// 没有出现异常而正常当取到的IP时，如果取到的不是网卡循回地址时就返回
			// 否则再通过Sigar工具包中的方法来获取
			if (!NetFlags.LOOPBACK_ADDRESS.equals(address)) {
				return address;
			}
		} catch (UnknownHostException e) {
			// hostname not in DNS or /etc/hosts
		}
		try {
			address = sigar.getNetInterfaceConfig().getAddress();
		} catch (SigarException e) {
			address = NetFlags.LOOPBACK_ADDRESS;
		}
		return address;
	}

	private String getDomainName() throws UnknownHostException {
		return InetAddress.getLocalHost().getCanonicalHostName();
	}

	private String getPlatformName() {
		String hostname = "";
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (Exception exc) {
			Sigar sigar = new Sigar();
			try {
				hostname = sigar.getNetInfo().getHostName();
			} catch (SigarException e) {
				hostname = "localhost.unknown";
			} finally {
				sigar.close();
			}
		}
		return hostname;
	}
}
