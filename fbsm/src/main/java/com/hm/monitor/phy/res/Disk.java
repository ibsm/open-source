package com.hm.monitor.phy.res;

import java.util.List;

import org.hyperic.sigar.FileSystem;
import org.hyperic.sigar.FileSystemUsage;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 磁盘 
 */
public class Disk {

	private List<DiskInfo> diskInfos;
	
	public List<DiskInfo> getDiskInfos() {
		return diskInfos;
	}

	public void setDiskInfos(List<DiskInfo> diskInfos) {
		this.diskInfos = diskInfos;
	}

	public static class DiskInfo{

		private FileSystem fileSystem;
		
		private FileSystemUsage fileSystemUsage;

		public FileSystem getFileSystem() {
			return fileSystem;
		}

		public void setFileSystem(FileSystem fileSystem) {
			this.fileSystem = fileSystem;
		}

		public FileSystemUsage getFileSystemUsage() {
			return fileSystemUsage;
		}

		public void setFileSystemUsage(FileSystemUsage fileSystemUsage) {
			this.fileSystemUsage = fileSystemUsage;
		}

		@Override
		public String toString() {
			return "DiskInfo [fileSystem=" + fileSystem + ", fileSystemUsage="
					+ fileSystemUsage + "]";
		}
		
	}

	@Override
	public String toString() {
		return "Disk [diskInfos=" + diskInfos + "]";
	}
	
}
