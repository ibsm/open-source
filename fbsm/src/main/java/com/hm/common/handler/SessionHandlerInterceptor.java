package com.hm.common.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;


/**
 * @author shishun.wang
 * @version 1.0
 * @date 12:30:18 AM May 5, 2015
 * @statement Without my written permission, any unit and individual shall not
 *            in any way or reason of the above products, services, information,
 *            and any part of the material to use, copy, modify, transcribing,
 *            spread or with other products bound use and marketing. Hereby
 *            solemnly statement!
 * @team 
 * @email shishun156@gmail.com
 * @describe 
 */
public class SessionHandlerInterceptor implements HandlerInterceptor{

	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		// TODO Auto-generated method stub
//		String url = request.getRequestURI();
//		boolean tag = ("/fbsm/error/session.do".equals(url) || "/fbsm/check.do".equals(url) || "/fbsm/login.do".equals(url) || "/fbsm/".equals(url) || "/fbsm/index.do".equals(url));
//		if(!tag){
//			Object obj = SessionUtil.getSession(request).getAttribute("LOGIN_USER_INFO");
//			if(null == obj){
//				response.sendRedirect("/fbsm/error/session.do");
//			}
//		}
		return true;
	}

	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
