package com.hm.common;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.activiti.engine.FormService;
import org.activiti.engine.HistoryService;
import org.activiti.engine.ManagementService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;

import com.hm.sys.dao.CmdbSysDictInsMapper;
import com.hm.sys.dao.CmdbSysDictMapper;
import com.hm.sys.dao.CmdbSysUserMapper;
import com.hm.sys.entity.CmdbSysDict;
import com.hm.sys.entity.CmdbSysDictIns;

@SuppressWarnings({"rawtypes","unchecked"})
public abstract class BaseService {

	@Autowired
	protected RedisTemplate redisTemplate;

	@Resource
	protected RepositoryService repositoryService;

	@Resource
	protected RuntimeService runtimeService;

	@Resource
	protected TaskService taskService;

	@Resource
	protected HistoryService historyService;

	@Resource
	protected ManagementService managementService;

	@Resource
	protected FormService formService;
	
	@Autowired
	protected CmdbSysDictMapper sysDictMapper;
	
	@Autowired
	protected CmdbSysDictInsMapper sysDictInsMapper;
	
	@Autowired
	protected CmdbSysUserMapper sysUserMapper;

	public String generateId(){
		return UUID.randomUUID().toString().replace("-", "");
	}

	protected RedisSerializer<String> getRedisSerializer() {
		return redisTemplate.getStringSerializer();
	}
	
	protected PageSet assemblyPaging(PageSet tmp,Map<String,Object> params){
		PageSet pageSet = (PageSet) params.get("query");
		pageSet.setRows(tmp.getRows());
		pageSet.setRecords(tmp.getTotal());
		return pageSet;
	}
	
	protected String transDictName4id(String id){
		CmdbSysDict sysDict = sysDictMapper.selectByPrimaryKey(id);
		return null!=sysDict?sysDict.getName():"未知";
	}
	
	protected String transDictInsName4id(String id){
		CmdbSysDictIns sysDictIns = sysDictInsMapper.selectByPrimaryKey(id);
		return null != sysDictIns?sysDictIns.getName():"未知";
	}
	
	protected List<CmdbSysDictIns> getSysDictInsByDictType(String dictType){
		return sysDictInsMapper.getSysDictInsByDictType(dictType);
	}
}