package com.hm.common.config;

import java.util.Properties;

/**
 * @author shishun.wang
 * @version 1.0
 * @date Nov 17, 2015 9:58:17 PM
 * @statement Without my written permission, any unit and individual shall not
 *            in any way or reason of the above products, services, information,
 *            and any part of the material to use, copy, modify, transcribing,
 *            spread or with other products bound use and marketing. Hereby
 *            solemnly statement!
 * @team 
 * @email shishun156@gmail.com
 * @describe 加载配置信息 
 */
public class Configuration {

	private static Properties properties;
	
	static{
		try {
			properties = new Properties();
			properties.load(Configuration.class.getResourceAsStream("/init.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String uploadPath(){
		return properties.getProperty("uploadPath");
	}
	
	public static String downloadPath(){
		return properties.getProperty("downloadPath");
	}
	
	public static String get(String key){
		return properties.getProperty(key);
	}
	
	public static Integer getInt(String key){
		return Integer.parseInt(get(key));
	}
}
