package com.hm.common.plugin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.executor.parameter.DefaultParameterHandler;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.MappedStatement.Builder;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import com.hm.common.PageSet;

@SuppressWarnings("rawtypes")
@Intercepts({ @Signature(type = Executor.class, method = "query", args = 
{ MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class } )})
public class PageableInterceptor implements Interceptor {

	static int MAPPED_STATEMENT_INDEX = 0;
	static int PARAMETER_INDEX = 1;
	static int ROWBOUNDS_INDEX = 2;
	static int RESULT_HANDLER_INDEX = 3;
	
	private Dialect dialect;
	
	public Object intercept(Invocation invocation) throws Throwable {
		// TODO Auto-generated method stub
		final Object[] queryArgs = invocation.getArgs();
		PageSet page = checkPaging(queryArgs[PARAMETER_INDEX]);
		if(null != page){
			final MappedStatement ms = (MappedStatement)queryArgs[MAPPED_STATEMENT_INDEX];
	        final Object parameter = queryArgs[PARAMETER_INDEX];
			final BoundSql boundSql = ms.getBoundSql(parameter);
			
			page.setTotal(queryTotal(boundSql.getSql(), ms, boundSql));
			
			String limitSql = dialect.getLimitString(boundSql.getSql(), (page.getPage() - 1)*page.getPageSize(), page.getPage()*page.getPageSize());			
			
			queryArgs[ROWBOUNDS_INDEX] = new RowBounds(RowBounds.NO_ROW_OFFSET,RowBounds.NO_ROW_LIMIT);
			queryArgs[MAPPED_STATEMENT_INDEX] = copyFromNewSql(ms, boundSql, limitSql);
			
			List rows = (List) invocation.proceed();
			page.setRows(rows);
			
			List<PageSet> tmp = new ArrayList<PageSet>(1);
			tmp.add(page);
			
			return tmp;
		}
		return invocation.proceed();
	}

	public Object plugin(Object target) {
		if (Executor.class.isAssignableFrom(target.getClass())) {
            return Plugin.wrap(target, this);
        }
		return target;
	}

	public void setProperties(Properties properties) {
		String dialectClass = properties.getProperty("dialectClass");
		try {
            setDialect((Dialect) Class.forName(dialectClass).newInstance());
		} catch (Exception e) {
			throw new RuntimeException("cannot create dialect instance by dialectClass:"+dialectClass,e);
		}
	}
	
	private int queryTotal(String sql, MappedStatement mappedStatement,BoundSql boundSql) throws Exception {
		Connection connection = null;
        PreparedStatement countStmt = null;
        ResultSet rs = null;
        try {
        	connection = mappedStatement.getConfiguration().getEnvironment().getDataSource().getConnection();
        	
    		String countSql = this.dialect.getCountString(sql);
            countStmt = connection.prepareStatement(countSql);
            BoundSql countBoundSql = new BoundSql(mappedStatement.getConfiguration(), countSql,
            		boundSql.getParameterMappings(), boundSql.getParameterObject());
            setParameters(countStmt, mappedStatement, countBoundSql, boundSql.getParameterObject());
            
            rs = countStmt.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(null != rs){
				rs.close();
			}
			if(null != countStmt){
				countStmt.close();
			}
			if(null != connection){
				connection.close();
			}
		}
		return 0;
	}
	
	private void setParameters(PreparedStatement ps, MappedStatement mappedStatement, BoundSql boundSql,
            Object parameterObject) throws Exception {
        ParameterHandler parameterHandler = new DefaultParameterHandler(mappedStatement, parameterObject, boundSql);
        parameterHandler.setParameters(ps);
    }
	
	public PageSet checkPaging(Object params) throws Exception{
		if(null == params){
			return null;
		}
//		if(PageSet.class.isAssignableFrom(params.getClass())){
//			return (PageSet) params;
//		}
		if(Map.class.isAssignableFrom(params.getClass())){
			@SuppressWarnings("unchecked")
			Map<String,Object> mapper = (Map<String, Object>) params;
			for (Map.Entry<String, Object> entry : mapper.entrySet()) {
				Object paramValue = entry.getValue();
				if(paramValue != null && PageSet.class.isAssignableFrom(paramValue.getClass())) {
					return (PageSet) paramValue;
				}  
			}
			return null;
		}
		return null;
	}

	public Dialect getDialect() {
		return dialect;
	}

	public void setDialect(Dialect dialect) {
		this.dialect = dialect;
	}

	private MappedStatement copyFromNewSql(MappedStatement ms,
			BoundSql boundSql, String sql) {
		BoundSql newBoundSql = copyFromBoundSql(ms, boundSql, sql);
		return copyFromMappedStatement(ms, new BoundSqlSqlSource(newBoundSql));
	}
	
	public static class BoundSqlSqlSource implements SqlSource {
		BoundSql boundSql;
		public BoundSqlSqlSource(BoundSql boundSql) {
			this.boundSql = boundSql;
		}
		public BoundSql getBoundSql(Object parameterObject) {
			return boundSql;
		}
	}
	
	private BoundSql copyFromBoundSql(MappedStatement ms, BoundSql boundSql,
			String sql) {
		BoundSql newBoundSql = new BoundSql(ms.getConfiguration(),sql, boundSql.getParameterMappings(), boundSql.getParameterObject());
		for (ParameterMapping mapping : boundSql.getParameterMappings()) {
		    String prop = mapping.getProperty();
		    if (boundSql.hasAdditionalParameter(prop)) {
		        newBoundSql.setAdditionalParameter(prop, boundSql.getAdditionalParameter(prop));
		    }
		}
		return newBoundSql;
	}
	
	//see: MapperBuilderAssistant
	private MappedStatement copyFromMappedStatement(MappedStatement ms,SqlSource newSqlSource) {
		Builder builder = new Builder(ms.getConfiguration(),ms.getId(),newSqlSource,ms.getSqlCommandType());
		
		builder.resource(ms.getResource());
		builder.fetchSize(ms.getFetchSize());
		builder.statementType(ms.getStatementType());
		builder.keyGenerator(ms.getKeyGenerator());
		if(ms.getKeyProperties() != null && ms.getKeyProperties().length !=0){
            StringBuffer keyProperties = new StringBuffer();
            for(String keyProperty : ms.getKeyProperties()){
                keyProperties.append(keyProperty).append(",");
            }
            keyProperties.delete(keyProperties.length()-1, keyProperties.length());
			builder.keyProperty(keyProperties.toString());
		}
		
		//setStatementTimeout()
		builder.timeout(ms.getTimeout());
		
		//setStatementResultMap()
		builder.parameterMap(ms.getParameterMap());
		
		//setStatementResultMap()
        builder.resultMaps(ms.getResultMaps());
		builder.resultSetType(ms.getResultSetType());
	    
		//setStatementCache()
		builder.cache(ms.getCache());
		builder.flushCacheRequired(ms.isFlushCacheRequired());
		builder.useCache(ms.isUseCache());
		
		return builder.build();
	}
}
