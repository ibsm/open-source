package com.hm.common.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 11:56:23 AM Apr 4, 2015
 * @statement Without my written permission, any unit and individual shall not
 *            in any way or reason of the above products, services, information,
 *            and any part of the material to use, copy, modify, transcribing,
 *            spread or with other products bound use and marketing. Hereby
 *            solemnly statement!
 * @team 
 * @email shishun156@gmail.com
 * @describe 
 */
public class Encrypt {

	/**
	 * @author 
	 * @version 1.0
	 * @date 12:59:51 PM May 31, 2014
	 * @statement Without my written permission, any unit and individual shall not
	 *            in any way or reason of the above products, services, information,
	 *            and any part of the material to use, copy, modify, transcribing,
	 *            spread or with other products bound use and marketing. Hereby
	 *            solemnly statement!
	 * @team chasing (personal<author></author>)
	 * @email 
	 * @describe md5加密
	 */
	public static class Md5 {
		private final static String[] strDigits = { "0", "1", "2", "3", "4",
				"5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

		// 返回形式为数字跟字符串
		private static String byteToArrayString(byte bByte) {
			int iRet = bByte;
			if (iRet < 0) {
				iRet += 256;
			}
			int iD1 = iRet / 16;
			int iD2 = iRet % 16;
			return strDigits[iD1] + strDigits[iD2];
		}

		// 转换字节数组为16进制字串
		private static String byteToString(byte[] bByte) {
			StringBuffer sBuffer = new StringBuffer();
			for (int i = 0; i < bByte.length; i++) {
				sBuffer.append(byteToArrayString(bByte[i]));
			}
			return sBuffer.toString();
		}

		public static String getMD5Code(String strObj) {
			String resultString = null;
			if(null != strObj){
				try {
					resultString = new String(strObj);
					MessageDigest md = MessageDigest.getInstance("MD5");
					// md.digest() 该函数返回值为存放哈希值结果的byte数组
					resultString = byteToString(md.digest(strObj.getBytes()));
				} catch (NoSuchAlgorithmException ex) {
					ex.printStackTrace();
				}
			}
			return resultString;
		}
	}

}
