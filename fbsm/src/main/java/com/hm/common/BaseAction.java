package com.hm.common;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.hm.common.abs.ForwardView;
import com.hm.common.abs.Work;
import com.hm.sys.entity.CmdbSysUser;
import com.hm.sys.service.SysMenuService;
import com.hm.sys.service.SysUserService;

import net.sf.json.JSONObject;

public abstract class BaseAction {
	
	private static Logger logger = Logger.getLogger(BaseAction.class.getName());
	
	@Resource
	protected SysUserService sysUserService;
	
	@Resource
	protected SysMenuService sysMenuService;
	
	protected HttpServletRequest request;
	
	protected HttpServletResponse response;
	
	@ModelAttribute
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response){  
        this.request = request;  
        this.response = response;  
    }
	
	public PageSet getPageSet(){
		PageSet pageSet = new PageSet();
		//设置当前页
		pageSet.setPage(Integer.parseInt(request.getParameter("page")));
		pageSet.setPageSize(Integer.parseInt(request.getParameter("rows")));
		pageSet.setSidx(request.getParameter("sidx"));
		return pageSet;
	}
	
	public void putJson(Object obj){
		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(JSONObject.fromObject(obj));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void sendMsg(Exception e){
		JSONObject obj = new JSONObject();
		obj.put("state", "successful");
		obj.put("msg", "operation successful");
		if(null != e){
			obj.put("state", "exception");
			obj.put("msg", e.getMessage());
		}
		putJson(obj);
	}
	
	protected void doWork(Work work,Integer opType){
		try {
			work.execute(opType);
			sendMsg(null);
		} catch (Exception e) {
			sendMsg(e);
			e.printStackTrace();
		}
	}
	
	protected void doWork(Work work){
		try {
			work.execute();
			sendMsg(null);
		} catch (Exception e) {
			sendMsg(e);
			logger.error(e);
			e.printStackTrace();
		}
	}
	
	protected <T> String forward(ForwardView view,Model model,T entity){
		try {
			String url = view.execute(model,entity);
			return (null != url)?url:"redirect:/error/session.do";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected <T> String forward(ForwardView view,Model model,Integer opType,T entity){
		try {
			String url = view.execute(opType,model,entity);
			return (null != url)?url:"redirect:/error/session.do";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected String reqGetData(String data) throws Exception{
		if(null != data){
			return new String(data.getBytes("iso8859-1"),"UTF-8");
		}
		return null;
	}
	
	protected Object reqParameter(String key) {
		return request.getParameter(key);
	}
	
	protected String getLoginUser(){
		Subject subject=SecurityUtils.getSubject();
		return ((CmdbSysUser)subject.getSession().getAttribute("LOGIN_USER_INFO")).getId();
	}
}
