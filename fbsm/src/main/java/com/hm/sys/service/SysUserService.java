package com.hm.sys.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.PageSet;
import com.hm.common.util.DataUtil;
import com.hm.common.util.DataUtil.StringUtil;
import com.hm.common.util.Encrypt;
import com.hm.sys.dao.CmdbSysRoleMapper;
import com.hm.sys.dao.CmdbSysUserRoleMapper;
import com.hm.sys.entity.CmdbSysRole;
import com.hm.sys.entity.CmdbSysUser;
import com.hm.sys.entity.CmdbSysUserRole;

@Service
@SuppressWarnings("unchecked")
public class SysUserService extends BaseService {

	@Autowired
	private CmdbSysRoleMapper sysRoleMapper;
	
	@Autowired
	private CmdbSysUserRoleMapper sysUserRoleMapper;
	
	public CmdbSysUser checkLoginUser(CmdbSysUser user) {
		user.setPwd(Encrypt.Md5.getMD5Code(user.getPwd()));
		return sysUserMapper.checkLoginUser(user);
	}

	public PageSet queryList(CmdbSysUser sysUser, PageSet pageSet){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("name", sysUser.getName());
		params.put("query", pageSet);
		pageSet = assemblyPaging(sysUserMapper.queryList(params),params);
		List<CmdbSysUser> cmdbSysUsers = (List<CmdbSysUser>) pageSet.getRows();
		JSONArray items = new JSONArray();
		for (CmdbSysUser cmdbSysUser : cmdbSysUsers) {
			JSONObject item = new JSONObject();
			item.put("id", cmdbSysUser.getId());
			item.put("name", cmdbSysUser.getName());
			item.put("note", cmdbSysUser.getNote());
			item.put("createTime", DataUtil.DateUtil.yyyyMMddhhmm(cmdbSysUser.getCreateTime()));
			
			items.add(item);
		}
		pageSet.setRows(items);
		return pageSet;
	}

	public void addSysUser(CmdbSysUser sysUser) {
		sysUser.setId(generateId());
		sysUser.setState(1);
		sysUser.setCreateTime(new Date());
		sysUser.setPwd(Encrypt.Md5.getMD5Code(sysUser.getPwd()));
		sysUserMapper.insert(sysUser);
	}

	public void deleteSysUser(CmdbSysUser sysUser) {
		sysUserMapper.deleteByPrimaryKey(sysUser.getId());
	}

	public CmdbSysUser getSysUserById(CmdbSysUser sysUser) {
		return sysUserMapper.selectByPrimaryKey(sysUser.getId());
	}

	public void updateSysUser(CmdbSysUser sysUser) {
		sysUser.setPwd(Encrypt.Md5.getMD5Code(sysUser.getPwd()));
		sysUserMapper.updateByPrimaryKey(sysUser);
	}

	public String getSysUserRoles(String id) {
		List<CmdbSysRole> sysRoles = sysRoleMapper.selectSysRole(new CmdbSysRole());
		JSONArray items = new JSONArray();
		CmdbSysUserRole record = new CmdbSysUserRole();
		for (CmdbSysRole sysRole : sysRoles) {
			JSONObject item = new JSONObject();
			item.put("id", sysRole.getId());
			item.put("pId", "0");
			item.put("name", sysRole.getName());
			{
				record.setUserId(id);
				record.setRoleId(sysRole.getId());
				item.put("checked",null != sysUserRoleMapper.selectSysUserRole(record)?"true":"false");
			}
			item.put("open", "true");
			items.add(item);
		}
		return items.toString();
	}

	public void saveUserRoles(CmdbSysUser sysUser, String roles) {
		//清空之前所有数据
		{
			CmdbSysUserRole record = new CmdbSysUserRole();
			record.setUserId(sysUser.getId());
			sysUserRoleMapper.clearSysUserRoles(record);
		}
		//添加数据
		if(!StringUtil.isBlank(roles)){
			String[] roleIds = StringUtil.dislogeBatchTagSEIds(roles,false);
			for (String roleId : roleIds) {
				CmdbSysUserRole record = new CmdbSysUserRole();
				record.setUserId(sysUser.getId());
				record.setId(generateId());
				record.setRoleId(roleId);
				sysUserRoleMapper.insert(record);
			}
		}
	}

}
