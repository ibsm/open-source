package com.hm.sys.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.util.DataUtil.DateUtil;
import com.hm.sys.dao.CmdbSysMenuMapper;
import com.hm.sys.dao.CmdbSysRoleMenuMapper;
import com.hm.sys.dao.CmdbSysUserRoleMapper;
import com.hm.sys.entity.CmdbRoleMenu;
import com.hm.sys.entity.CmdbSysMenu;
import com.hm.sys.entity.CmdbSysUserRole;

@Service
public class SysMenuService extends BaseService {
	
	@Autowired
	private CmdbSysMenuMapper sysMenuMapper;
	
	@Autowired
	private CmdbSysUserRoleMapper sysUserRoleMapper;
	
	@Autowired
	private CmdbSysRoleMenuMapper sysRoleMenuMapper;

	public Map<String,String> getMenusByLoginUser(String userId){
		Map<String,String> memoryMap = new HashMap<String,String>();
		
		List<CmdbSysUserRole> userAllRoles = sysUserRoleMapper.getUserAllRoles(userId);
		for (CmdbSysUserRole userRole : userAllRoles) {
			List<CmdbRoleMenu> roleAllMenus = sysRoleMenuMapper.getRoleAllMenus(userRole.getRoleId());
			for (CmdbRoleMenu roleMenu : roleAllMenus) {
				memoryMap.put(roleMenu.getMenuId(), roleMenu.getId());
			}
		}
		
		return memoryMap;
	}
	
	public JSONArray loadSuperNav(CmdbSysMenu entity, boolean recursive) {
		List<CmdbSysMenu> cmdbSysMenus = sysMenuMapper.selectByParentId(entity.getParentId());
		JSONArray items = new JSONArray();
		for(CmdbSysMenu sysMenu:cmdbSysMenus){
			JSONObject item = new JSONObject();
			
			item.put("id", sysMenu.getId());
			item.put("name", sysMenu.getName());
			item.put("path", sysMenu.getUrl());
			item.put("icon", sysMenu.getIcon());
			item.put("navMenu", sysMenu.getNote()+"");
			item.put("parentId", sysMenu.getParentId());
			if(recursive){
				sysMenu.setParentId(sysMenu.getId());
				JSONArray array = loadSuperNav(sysMenu,recursive);
				for (int i = 0; i < array.size(); i++) {
					items.add(array.get(i));
				}
			}
			
			items.add(item);
		}
		return items;
	}
	
	public JSONArray loadNav(CmdbSysMenu entity, boolean recursive,Map<String,String> memoryMap) {
		List<CmdbSysMenu> cmdbSysMenus = sysMenuMapper.selectByParentId(entity.getParentId());
		JSONArray items = new JSONArray();
		for(CmdbSysMenu sysMenu:cmdbSysMenus){
			if(memoryMap.containsKey(sysMenu.getId())){
				JSONObject item = new JSONObject();
				
				item.put("id", sysMenu.getId());
				item.put("name", sysMenu.getName());
				item.put("path", sysMenu.getUrl());
				item.put("icon", sysMenu.getIcon());
				item.put("navMenu", sysMenu.getNote()+"");
				item.put("parentId", sysMenu.getParentId());
				if(recursive){
					sysMenu.setParentId(sysMenu.getId());
					JSONArray array = loadNav(sysMenu,recursive,memoryMap);
					for (int i = 0; i < array.size(); i++) {
						items.add(array.get(i));
					}
				}
				
				items.add(item);
			}
		}
		//可以对输出到浏览器的json格式数据处理
		return items;
	}

	public CmdbSysMenu getSysMenuById(CmdbSysMenu sysMenu) {
		return sysMenuMapper.selectByPrimaryKey(sysMenu.getId());
	}

	public JSONArray queryList(String id) {
		List<CmdbSysMenu> cmdbSysMenus = sysMenuMapper.selectByParentId(id);
		JSONArray items = new JSONArray();
		for(CmdbSysMenu sysMenu:cmdbSysMenus){
			JSONObject item = new JSONObject();
			
			item.put("id", sysMenu.getId());
			item.put("name", sysMenu.getName());
			item.put("path", sysMenu.getUrl());
			item.put("sort", sysMenu.getSort());
			item.put("createTime", DateUtil.yyyyMMddhhmm(sysMenu.getCreateTime()));
			item.put("parent", id);
			item.put("expanded", "true");//默认全部展开
			item.put("loaded", "true");
			item.put("level", id);
			
			JSONArray array = queryList(sysMenu.getId());
			{
				item.put("isLeaf", 0!=array.size()?"false":"true");
				items.add(item);
			}
			for (int i = 0; i < array.size(); i++) {
				items.add(array.get(i));
			}
		}
		
		return items;
	}

	public void addSysMenu(CmdbSysMenu sysMenu) {
		sysMenu.setId(generateId());
		sysMenu.setCreateTime(new Date());
		sysMenu.setState(1);
		sysMenuMapper.insert(sysMenu);
	}

	public void updateSysMenu(CmdbSysMenu sysMenu) {
		sysMenuMapper.updateByPrimaryKey(sysMenu);
	}

	public void deleteSysMenu(CmdbSysMenu sysMenu) {
		sysMenuMapper.deleteByPrimaryKey(sysMenu.getId());
	}

}
