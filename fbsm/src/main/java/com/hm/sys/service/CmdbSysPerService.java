package com.hm.sys.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.PageSet;
import com.hm.common.util.DataUtil.DateUtil;
import com.hm.sys.dao.CmdbSysPerMapper;
import com.hm.sys.entity.CmdbSysPer;

@Service
public class CmdbSysPerService extends BaseService {

	@Autowired
	private CmdbSysPerMapper sysPerMapper;

	public CmdbSysPer getSysPerById(CmdbSysPer sysPer) {
		return sysPerMapper.selectByPrimaryKey(sysPer.getId());
	}

	public void addSysPer(CmdbSysPer sysPer) {
		sysPer.setId(generateId());
		sysPer.setState(1);
		sysPer.setCreateTime(new Date());
		sysPerMapper.insert(sysPer);
	}

	public void updateSysPer(CmdbSysPer sysPer) {
		sysPerMapper.updateByPrimaryKey(sysPer);
	}

	public void deleteSysPer(CmdbSysPer sysPer) {
		sysPerMapper.deleteByPrimaryKey(sysPer.getId());
	}

	public PageSet queryList(CmdbSysPer sysPer, PageSet pageSet) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("name", sysPer.getName());
		params.put("preType", sysPer.getPreType());
		params.put("query", pageSet);
		pageSet = assemblyPaging(sysPerMapper.queryList(params),params);
		@SuppressWarnings("unchecked")
		List<CmdbSysPer> sysPers = (List<CmdbSysPer>) pageSet.getRows();
		JSONArray items = new JSONArray();
		for (CmdbSysPer per : sysPers) {
			JSONObject item = new JSONObject();
			item.put("id", per.getId());
			item.put("name", per.getName());
			item.put("code", per.getCode());
			item.put("note", per.getNote());
			item.put("perType", transDictInsName4id(per.getPreType()));
			item.put("createTime", DateUtil.yyyyMMddhhmm(per.getCreateTime()));
			
			items.add(item);
		}
		pageSet.setRows(items);
		return pageSet;
	}
	
}
