package com.hm.sys.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.PageSet;
import com.hm.common.util.DataUtil.DateUtil;
import com.hm.common.util.DataUtil.StringUtil;
import com.hm.sys.entity.CmdbSysDictIns;

@Service
@SuppressWarnings("unchecked")
public class SysDictInsService extends BaseService {
	
	public PageSet queryList(CmdbSysDictIns sysDictIns, PageSet pageSet){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("name", sysDictIns.getName());
		params.put("dictType", sysDictIns.getDictType());
		params.put("query", pageSet);
		pageSet = assemblyPaging(sysDictInsMapper.queryList(params),params);
		List<CmdbSysDictIns> dictInss = (List<CmdbSysDictIns>) pageSet.getRows();
		JSONArray items = new JSONArray();
		for (CmdbSysDictIns dictIns : dictInss) {
			JSONObject item = new JSONObject();
			item.put("id", dictIns.getId());
			item.put("name", dictIns.getName());
			item.put("note", dictIns.getNote());
			item.put("sort", dictIns.getSort());
			item.put("dictType", transDictName4id(dictIns.getDictType()));
			item.put("system", dictIns.getSystem());
			item.put("createTime", DateUtil.yyyyMMddhhmm(dictIns.getCreateTime()));
			
			items.add(item);
		}
		pageSet.setRows(items);
		return pageSet;
	}

	public void addSysDictIns(CmdbSysDictIns sysDictIns) {
		sysDictIns.setId(generateId());
		sysDictIns.setCreateTime(new Date());
		sysDictIns.setState(1);
		sysDictInsMapper.insert(sysDictIns);
	}

	public CmdbSysDictIns getSysDictInsById(CmdbSysDictIns sysDictIns) {
		return sysDictInsMapper.selectByPrimaryKey(sysDictIns.getId());
	}

	public void updateSysDictIns(CmdbSysDictIns sysDictIns) {
		sysDictInsMapper.updateByPrimaryKey(sysDictIns);
	}

	public void deleteSysDictIns(CmdbSysDictIns sysDictIns) {
		sysDictInsMapper.deleteByPrimaryKey(sysDictIns.getId());
	}

	public void batchDeleteSysDictIns(CmdbSysDictIns sysDictIns) {
		String ids[] = StringUtil.dislogeBatchTagSEIds(sysDictIns.getId(),false);
		for (String id : ids) {
			sysDictInsMapper.deleteByPrimaryKey(id);
		}
	}

}
