package com.hm.sys.dao;

import java.util.List;
import java.util.Map;

import com.hm.common.PageSet;
import com.hm.sys.entity.CmdbSysDict;

public interface CmdbSysDictMapper {
    int deleteByPrimaryKey(String id);

    int insert(CmdbSysDict record);

    CmdbSysDict selectByPrimaryKey(String id);

    int updateByPrimaryKey(CmdbSysDict record);
    
    PageSet queryList(Map<String, Object> pageSet);

	List<CmdbSysDict> getAllDictTypes();
}