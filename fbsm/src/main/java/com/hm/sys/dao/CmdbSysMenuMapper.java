package com.hm.sys.dao;

import java.util.List;

import com.hm.sys.entity.CmdbSysMenu;

public interface CmdbSysMenuMapper {
    int deleteByPrimaryKey(String id);

    int insert(CmdbSysMenu record);

    CmdbSysMenu selectByPrimaryKey(String id);
    
    List<CmdbSysMenu> selectByParentId(String parentId);

    int updateByPrimaryKey(CmdbSysMenu record);
}