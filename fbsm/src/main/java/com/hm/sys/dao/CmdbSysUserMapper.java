package com.hm.sys.dao;

import java.util.List;
import java.util.Map;

import com.hm.common.PageSet;
import com.hm.sys.entity.CmdbSysUser;

public interface CmdbSysUserMapper {
	
	List<CmdbSysUser> selectAllUser();
	
	int deleteByPrimaryKey(String id);

	int insert(CmdbSysUser record);

	CmdbSysUser selectByPrimaryKey(String id);
	
	CmdbSysUser selectByAccount(String account);

	CmdbSysUser checkLoginUser(CmdbSysUser record);

	int updateByPrimaryKey(CmdbSysUser record);
	
	PageSet queryList(Map<String, Object> pageSet);
}