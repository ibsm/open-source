package com.hm.sys.action;

import java.io.FileOutputStream;
import java.io.InputStream;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.hm.common.BaseAction;
import com.hm.common.config.Configuration;
import com.hm.common.util.DataUtil.FileUtil;
import com.hm.common.util.DataUtil.QrCodeUtil;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 文件管理
 */
@Controller
@RequestMapping("sys/file")
public class FileAction extends BaseAction {

	@RequestMapping("zip")
	public String zip(){
		return "sys/file/zip";
	}
	
	@RequestMapping(value="zipSave",method=RequestMethod.POST)
	public void generalSave(@RequestParam MultipartFile[] files) throws Exception{
		JSONArray items = new JSONArray();
		for (MultipartFile multipartFile : files) {
			if (!multipartFile.isEmpty()) {
				String fileName = multipartFile.getOriginalFilename();
				String prefix=fileName.substring(fileName.lastIndexOf(".")+1);
				
				String path = FileUtil.generationUpdateFilePath();
				{//判断文件夹存在否
					FileUtil.exists(Configuration.uploadPath() +"/"+ path, true);
				}
				path += "/"+System.currentTimeMillis()+"."+prefix;
				FileOutputStream os = new FileOutputStream(Configuration.uploadPath() +"/"+ path);
				InputStream in = multipartFile.getInputStream();
				int b = 0;  
                while((b=in.read()) != -1){  
                    os.write(b);  
                }  
                os.flush();  
                os.close();  
                in.close(); 
                
                items.add(path);
			}
		}
		JSONObject obj = new JSONObject();
		obj.put("path", items.toString());
		putJson(obj);
	}
	
	@RequestMapping(value="qrCode",method=RequestMethod.GET)
	public void qrCode() throws Exception{
		new QrCodeUtil().encode("http://www.baidu.com", response.getOutputStream());
	}
}
