package com.hm.sys.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hm.common.BaseAction;
import com.hm.sys.entity.CmdbSysDict;
import com.hm.sys.entity.CmdbSysDictIns;
import com.hm.sys.service.SysDictInsService;
import com.hm.sys.service.SysDictTypeService;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 数据字典类型管理
 */
@Controller
@RequestMapping("sys/dictIns")
public class SysDictInsAction extends BaseAction {
	
	@Resource
	private SysDictTypeService dictTypeService;
	
	@Resource
	private SysDictInsService dictInsService;
	
	@RequestMapping(value="index")
	public String index(Model model){
		model.addAttribute("dictTypes", dictTypeService.getAllDictTypes());
		return "sys/dictIns/index";
	}
	
	@RequestMapping(value="add")
	public String add(Model model){
		model.addAttribute("dictTypes", dictTypeService.getAllDictTypes());
		return "sys/dictIns/add";
	}
	
	@RequestMapping(value="update")
	public String update(CmdbSysDictIns sysDictIns,Model model){
		model.addAttribute("dictTypes", dictTypeService.getAllDictTypes());
		model.addAttribute("result", dictInsService.getSysDictInsById(sysDictIns));
		return "sys/dictIns/update";
	}
	
	@RequestMapping(value="detail")
	public String detail(CmdbSysDictIns sysDictIns,Model model){
		CmdbSysDictIns dictIns = dictInsService.getSysDictInsById(sysDictIns); 
		{
			CmdbSysDict sysDict = new CmdbSysDict();
			sysDict.setId(dictIns.getDictType());
			dictIns.setDictType(dictTypeService.getSysDictById(sysDict).getName());
		}
		model.addAttribute("result", dictIns);
		return "sys/dictIns/detail";
	}
	
	@RequestMapping(value="saveAdd")
	public void saveAdd(HttpServletRequest request,HttpServletResponse response,CmdbSysDictIns sysDictIns){
		sysDictIns.setCreateUser(getLoginUser());
		dictInsService.addSysDictIns(sysDictIns);
	}
	
	@RequestMapping(value="saveUpdate")
	public void saveUpdate(HttpServletRequest request,HttpServletResponse response,CmdbSysDictIns sysDictIns){
		dictInsService.updateSysDictIns(sysDictIns);
	}
	
	@RequestMapping(value="saveDelete")
	public void saveDelete(HttpServletRequest request,HttpServletResponse response,CmdbSysDictIns sysDictIns){
		dictInsService.deleteSysDictIns(sysDictIns);
	}
	
	@RequestMapping(value="saveBatchDelete")
	public void saveBatchDelete(HttpServletRequest request,HttpServletResponse response,CmdbSysDictIns sysDictIns){
		dictInsService.batchDeleteSysDictIns(sysDictIns);
	}
	
	@RequestMapping(value="queryList")
	public void queryList(CmdbSysDictIns sysDictIns){
		putJson(dictInsService.queryList(sysDictIns, getPageSet()));
	}
	
}
