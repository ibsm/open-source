package com.hm.sys.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hm.common.BaseAction;
import com.hm.sys.entity.CmdbSysRole;
import com.hm.sys.service.SysRoleService;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 角色管理
 */
@Controller
@RequestMapping("sys/role")
public class SysRoleAction extends BaseAction {
	
	@Resource
	private SysRoleService sysRoleService;
	
	@RequestMapping(value="index")
	public String index(){
		return "sys/role/index";
	}
	
	@RequestMapping(value="add")
	public String add(){
		return "sys/role/add";
	}
	
	@RequestMapping(value="update")
	public String update(CmdbSysRole sysRole,Model model){
		model.addAttribute("result", sysRoleService.getSysRoleById(sysRole));
		return "sys/role/update";
	}
	
	@RequestMapping(value="detail")
	public String detail(CmdbSysRole sysRole,Model model){
		model.addAttribute("result", sysRoleService.getSysRoleById(sysRole));
		return "sys/role/detail";
	}
	
	@RequestMapping(value="menuPer")
	public String menuPer(CmdbSysRole sysRole,Model model){
		model.addAttribute("roleId",sysRole.getId());
		model.addAttribute("menuPers", sysRoleService.getSysRoleMenuPers(sysRole,"0").toString());
		return "sys/role/menuPer";
	}
	
	@RequestMapping(value="dataPre")
	public String dataPre(CmdbSysRole sysRole,Model model){
		model.addAttribute("roleId",sysRole.getId());
		model.addAttribute("pers", sysRoleService.getSysRoleDataPers(sysRole).toString());
		return "sys/role/dataPre";
	}
	
	@RequestMapping(value="saveAdd")
	public void saveAdd(HttpServletRequest request,HttpServletResponse response,CmdbSysRole sysRole){
		sysRole.setCreateUser(getLoginUser());
		sysRoleService.addSysRole(sysRole);
	}
	
	@RequestMapping(value="saveUpdate")
	public void saveUpdate(HttpServletRequest request,HttpServletResponse response,CmdbSysRole sysRole){
		sysRoleService.updateSysRole(sysRole);
	}
	
	@RequestMapping(value="saveDelete")
	public void saveDelete(HttpServletRequest request,HttpServletResponse response,CmdbSysRole sysRole){
		sysRoleService.deleteSysRole(sysRole);	
	}
	
	@RequestMapping(value="saveMenuPer")
	public void saveMenuPer(HttpServletRequest request,HttpServletResponse response,CmdbSysRole sysRole){
		sysRoleService.saveRoleMenus(sysRole,request.getParameter("menus"));	
	}
	
	@RequestMapping(value="saveDataPre")
	public void saveDataPre(HttpServletRequest request,HttpServletResponse response,CmdbSysRole sysRole){
		sysRoleService.saveRoleDataPres(sysRole,request.getParameter("pres"));	
	}
	
	@RequestMapping(value="queryList")
	public void queryList(CmdbSysRole sysRole){
		putJson(sysRoleService.queryList(sysRole, getPageSet()));
	}
	
}
