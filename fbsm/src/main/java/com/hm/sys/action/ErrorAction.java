package com.hm.sys.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.hm.common.BaseAction;

@Controller
@RequestMapping("error")
public class ErrorAction extends BaseAction{

	@RequestMapping(value="session")
	public ModelAndView index() throws Exception{
		return new ModelAndView("error/session");
	}
	
}
