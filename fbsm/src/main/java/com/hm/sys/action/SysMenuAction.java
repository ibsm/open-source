package com.hm.sys.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hm.common.BaseAction;
import com.hm.sys.entity.CmdbSysMenu;

/**
 * @author shishun.wang
 * @date 6:35:31 PM Jan 1, 2016
 * @version 1.0
 * @describe 菜单管理
 */
@Controller
@RequestMapping("sys/menu")
public class SysMenuAction extends BaseAction{

	@RequestMapping(value="index")
	public String index(){
		return "sys/menu/index";
	}
	
	@RequestMapping(value="add")
	public String add(){
		return "sys/menu/add";
	}
	
	@RequestMapping(value="update")
	public String update(CmdbSysMenu sysMenu,Model model) {
		model.addAttribute("result", sysMenuService.getSysMenuById(sysMenu));
		return "sys/menu/update";
	}
	
	@RequestMapping(value="detail")
	public String detail(CmdbSysMenu sysMenu,Model model){
		model.addAttribute("result", sysMenuService.getSysMenuById(sysMenu));
		return "sys/menu/detail";
	}
	
	@RequestMapping(value="saveAdd")
	public void saveAdd(HttpServletRequest request,HttpServletResponse response,CmdbSysMenu sysMenu){
		sysMenu.setCreateUser(getLoginUser());
		sysMenuService.addSysMenu(sysMenu);
	}
	@RequestMapping(value="saveUpdate")
	public void saveUpdate(HttpServletRequest request,HttpServletResponse response,CmdbSysMenu sysMenu){
		sysMenuService.updateSysMenu(sysMenu);
	}
	
	@RequestMapping(value="saveDelete")
	public void saveDelete(HttpServletRequest request,HttpServletResponse response,CmdbSysMenu sysMenu){
		sysMenuService.deleteSysMenu(sysMenu);
	}
	
	@RequestMapping(value="queryList")
	public void queryList(){
		JSONObject item = new JSONObject();
		item.put("response", sysMenuService.queryList("0"));
		putJson(item);
	}
}
