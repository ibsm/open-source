<%@page import="com.hm.common.StaticNature.DictTypeCofig"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="hm" uri="http://hm-ssw.do"%>
<%@include  file="../../../global/dialog/head_general.jsp"%>
	<style>
		#form_layout4table{
			margin-top:20px;
			width: :100%;
		}
		#form_layout4table td{
			text-align: right;
			padding:5px;
		}
	
		#form_layout4table td:first-child{
			padding-right:20px;
		}
		
		#form_layout4table td:nth-child(2){
			padding-right:20px;
		}
		
		input.error { 
			border: 1px solid red; 
		}
		
		span.error{
			color:red;
			font-size:11px;
		}
	</style>
	<script type="text/javascript" src="resource/ui/plugins/validate/jquery.validate.js"></script>
</head>
<body>
<div class="container">
	
	
	<form id="updateForm">
		<input type="hidden" value="${result.id }" name="id"/>
		<input type="hidden" value="${result.processState }" name="processState"/>
		<table id="form_layout4table" style="width: 100%">
			<tr>
				<td width="30%">请假天数:</td>
				<td width="70%"><input type="text" name="days" class="form-control input-sm" minlength="1" required value="${result.days }"  disabled="disabled"/></td>
			</tr>
			<tr>
				<td width="30%">请假内容:</td>
				<td width="70%"><input name="content" class="form-control input-sm" disabled="disabled" value="${result.content }"/></td>
			</tr>
			<tr>
				<td width="30%">备注:</td>
				<td width="70%"><textarea name="remark" class="form-control input-sm" disabled="disabled">${result.remark }</textarea></td>
			</tr>
			<tr>
				<td width="30%">批注:</td>
				<td width="70%"><textarea name="notation" class="form-control input-sm"></textarea></td>
			</tr>
			<tr>
				<td width="30%">审批状态:</td>
				<td width="70%">
					<hm:DictInstance dictType="<%=DictTypeCofig.approvalStatus %>"/>
				</td>
			</tr>
		</table>
	</form>
	
	<script type="text/javascript">
	
		function validate(){
			return $("#updateForm").validate({
				rules: {
					name: {
						required: true,
						minlength: 2
					}
				},
				messages: {
					days: {
						required: "<span class='error'>请输入请假天数</span>",
						minlength: "<span class='error'>名称最少是1个字符</span>"
					}
				}
			}).form();
		}
		
		function saveData(){
			var msg = $.ajax({
		         type : "post",  
		         url : "null",
		         data : $('#updateForm').serialize(), 
		         async : false 
		     }).responseText;
			return jQuery.parseJSON(msg);
		}
	</script>
	
</div>
<%@include  file="../../../global/dialog/footer_general.jsp"%>
