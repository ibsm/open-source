<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<h4 style="font-size: 14px;">说明</h4>
	   		<p style="color:#23527C;">系统用户,是系统提供给数据管理员的统一入口。</p>
	   </div>
	</div>
</div>


<div class="col-md-12">
   	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">查询条件<button class="btn btn-default btn-sm pull-right" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="add()">新增</button></h3>
	   </div>
	   <div class="panel-body">
	   		
	   		<form class="form-inline">
			  <input type="text" class="span3" placeholder="名称" id="userName">
			  
			  <button class="btn btn-default btn-sm active" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="search()">搜索</button>
			</form>
	   		
	   </div>
	</div>
</div>

<div class="col-md-12">
	<table id="jqGrid" data-toggle="context"></table>
	<div id="jqGridPager"></div>
</div>


<script type="text/javascript"> 
	function delData(id,batch){
		delGridRow("workflow/common/deploy/saveOpData.do?id="+id+"&opType=3");
	}

	function add(){
		var tokenTag = true;
		layer.open({
		    type: 2,
		    area: ['420px', '260px'],
		    fix: false, //不固定
		    title:'部属流程定义',
		    content: 'workflow/common/deploy/jump.do?opType=1',
		    btn: ['确定', '取消'],
		    yes: function(index, layero){
		    	var iframeWin = window[layero.find('iframe')[0]['name']];
		    	var valid = iframeWin.validate();
		    	if(valid && tokenTag){
		    		tokenTag = false;
		    		var data = iframeWin.deployProcess();
		    		close(data,index,"流程部属成功");
		    		search();
		    	}
		    },
		    cancel: function(index){
		    },
		    zIndex: layer.zIndex,
		    success: function(layero){
		        layer.setTop(layero);
		    }
		});
	}
	
	function updateSysRole(id){
		var tokenTag = true;
		layer.open({
		    type: 2,
		    area: ['420px', '260px'],
		    fix: false, //不固定
		    title:'修改角色',
		    content: 'sys/permission/role/updateSysRole.do?id='+id,
		    btn: ['确定', '取消'],
		    yes: function(index, layero){
		    	var iframeWin = window[layero.find('iframe')[0]['name']];
		    	var valid = iframeWin.validate();
		    	if(valid && tokenTag){
		    		tokenTag = false;
		    		var data = iframeWin.saveSysUser();
		    		close(data,index,"修改成功");
		    		search();
		    	}
		    },
		    cancel: function(index){
		    }
		});
	}
	
	function search(){
		var queryData = {
			'name':$('#userName').val()
		};
		$("#jqGrid").jqGrid("setGridParam",{ postData: queryData }).trigger("reloadGrid");
	}

	$(document).ready(function () {
		$("#jqGrid").jqGrid({
	        url: 'workflow/common/deploy/getProcessDeployList.do',
	        mtype: 'POST',
	        datatype: "json",
	        colModel: [			
				{ label: '部属编号', name: 'id', width: 45, key: true },
				{ label: '部属名称', name: 'name', width: 75 },
				{ label: '部属类型', name: 'category', width: 90 },
				{ label: '部属时间', name: 'deploymentTime', width: 90 },
				{ label: '操作', name: 'id', width: 90,
					formatter:function(cellvalue, options, rowObject){
						return '<a href="javascript:delData(\''+cellvalue+'\',false)" style="margin-left:10px;">删除</a>';
					}
				}
	        ],
			loadonce: false,
			viewrecords: true,
			autowidth : true,
			styleUI : "Bootstrap",
			width : '100%',
			height : 200,
	        rowNum: 10,
	        rowList : [5,10,15],
	        rownumbers: true, 
	        rownumWidth: 22, 
	        multiselect: false,
	        pager: "#jqGridPager",
	        jsonReader: {
	        	repeatitems : false
	        },
	        subGrid: true,
	        // define the icons in subgrid
	        subGridOptions: {
	              "plusicon"  : "ui-icon-triangle-1-e",
	              "minusicon" : "ui-icon-triangle-1-s",
	              "openicon"  : "ui-icon-arrowreturn-1-e",
		          // load the subgrid data only once
		          // and the just show/hide
		          "reloadOnExpand" : false,
		          // select the row when the expand column is clicked
		          "selectOnExpand" : true
	        },
	        subGridRowExpanded: function(subgrid_id, row_id) {
	            var subgrid_table_id, pager_id;
	            subgrid_table_id = subgrid_id+"_t";
	            pager_id = "p_"+subgrid_table_id;
	            $("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
	            jQuery("#"+subgrid_table_id).jqGrid({
		              url:"workflow/common/deploy/getProcessDefineList.do?id="+row_id,
		              datatype: "json",
		              colNames: ['定义编号','名称','流程KEY','流程版本','流程资源描述','流程资源名称','查看流程图'],
		              colModel: [
			                {name:"id",index:"id"},
			                {name:"name",index:"name"},
			                {name:"key",index:"key"},
			                {name:"version",index:"version"},
			                {name:"resourceName",index:"resourceName"},
			                {name:"diagramResourceName",index:"diagramResourceName"},
			                {name:"deploymentId",index:"deploymentId",
			                	formatter:function(cellvalue, options, rowObject){
			                		return "<a target='_blank' href='workflow/common/deploy/outPutFlowPic.do?deploymentId="+cellvalue+"&resourceName="+rowObject.diagramResourceName+"'>查看</a>";
			                	}
			                }
		              ],
		              styleUI : "Bootstrap",
		              viewrecords: true,
	  				  autowidth : true,
	  				  styleUI : "Bootstrap",
	  			      width : '100%',
	                  rowNum:10,
	                  pager: pager_id,
	                  height: '100%'
	            });
	            jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{search:false,edit:false,add:false,del:false})
	        }
	    });
	});
	
	function showFlowPic(deploymentId,diagramResourceName){
		//弹窗展示流程图片
	}
</script>













