<%@page import="com.hm.common.util.R"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include  file="../../global/dialog/head_general.jsp"%>
	<style>
		*{
			font-family: "Microsoft YaHei" ! important;
		}
		#form_layout4table{
			margin-top:20px;
			width: :100%;
		}
		#form_layout4table td{
			text-align: right;
			padding:5px;
		}
	
		#form_layout4table td:first-child{
			padding-right:20px;
		}
		
		#form_layout4table td:nth-child(2){
			padding-right:20px;
		}
		
		input.error { 
			border: 1px solid red; 
		}
		
		span.error{
			color:red;
			font-size:11px;
		}
	</style>
	<script type="text/javascript" src="resource/ui/plugins/validate/jquery.validate.js"></script>
</head>
<body>
<div class="container">
	
	<form id="form">
		<input type="hidden" name="id"  value="${result.id }"/>
		<table id="form_layout4table" style="width: 100%">
			<tr>
				<td width="30%">名&nbsp;&nbsp;&nbsp;称:</td>
				<td width="70%"><input type="text" name="name" class="form-control input-sm" minlength="2" required value="${result.name }"/></td>
			</tr>
			<tr>
				<td width="30%">类&nbsp;&nbsp;&nbsp;型:</td>
				<td width="70%">
					<hm:DictInstance dictType="<%=R.DictType.FLOW_TYPE %>" name="flowType" value="${result.flowType }"/>
				</td>
			</tr>
			<tr>
				<td width="30%">描&nbsp;&nbsp;&nbsp;述:</td>
				<td width="70%"><textarea name="note" class="form-control input-sm">${result.note }</textarea></td>
			</tr>
		</table>
	</form>
	
	<script type="text/javascript">
		function validate(){
			return $("#form").validate({
				rules: {
					name: {
						required: true,
						minlength: 2
					},
					flowType:{
						required: true,
						minlength: 1
					}
				},
				messages: {
					name: {
						required: "<span class='error'>请输入名称</span>",
						minlength: "<span class='error'>名称最少是2个字符</span>"
					},
					flowType:{
						required: "<span class='error'>请输入流程类型</span>",
						minlength: "<span class='error'>流程类型最少是1个字符</span>"
					}
				}
			}).form();
		}
		
		function submit(index,call){
			var msg = $.ajax({
		         type : "post",  
		         url : "wflow/actCusNode/saveUpdate.do?msg=修改失败",
		         data : $('#form').serialize(), 
		         async : false 
		    }).responseText;
			layer.close(index);
			call(jQuery.parseJSON(msg));
		}
	</script>
	
</div>
<%@include  file="../../global/dialog/footer_general.jsp"%>
