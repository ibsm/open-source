<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include  file="../../global/dialog/head_general.jsp"%>
<link rel="stylesheet" href="resource/ui/plugins/tree/css/zTreeStyle/zTreeStyle.css" type="text/css"/>
<script type="text/javascript" src="resource/ui/plugins/tree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="resource/ui/plugins/tree/js/jquery.ztree.excheck-3.5.min.js"></script>
<script type="text/javascript">
var setting = {
		check: {
			enable: true
		},
		data: {
			simpleData: {
				enable: true
			}
		}
	};

	var zNodes = ${flowNodeExeUsers};
	var treeObj;
	
	$(document).ready(function(){
		treeObj = $.fn.zTree.init($("#treeDemo"), setting, zNodes);
	});
	
	function validate(){
		return true;
	}
	
	function submit(index,call) {
       var nodes = treeObj.getCheckedNodes(true);
       var ids = "";
       for (var i = 0; i < nodes.length; i++) {
    	   ids += nodes[i].id + ",";
       }
       $('#pres').val(ids);
       var msg = $.ajax({
	         type : "post",  
	         url : "wflow/actCusNode/saveExeConfig.do?msg=工作流节点处理人授权失败",
	         data : $('#form').serialize(), 
	         async : false 
	    }).responseText;
		layer.close(index);
		call(jQuery.parseJSON(msg));
    }
	
</script>
</head>
<body>
<div class="container">
	<form id="form">
		<input type="hidden" name="id" value="${nodeId }"/>
		<input type="hidden" name="pres" id="pres"/>
	</form>
	<ul id="treeDemo" class="ztree"></ul>
</div>
<%@include  file="../../global/dialog/footer_general.jsp"%>
