<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 

<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<p style="color:#23527C;"><span style="font-size: 14px;padding-right: 10px;color:black;">说明:</span>数据字典实例</p>
	   </div>
	</div>
</div>

<div class="col-md-12">
   	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">查询条件</h3>
	   </div>
	   <div class="panel-body">
	   		
	   		<form class="form-inline">
			  <select id="dictType" class="span3" placeholder="字典类型" style="width:150px;height:24px;">
			  	<option value="">--请选择字典类型--</option>
		        <c:forEach var="item" items="${dictTypes }">
		        	<option value="${item.id }">${item.name }</option>
		        </c:forEach>
			  </select>
			  <input type="text" class="span3" placeholder="名称" id="name">
			  
			  <button class="btn btn-default btn-sm active" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="search()">搜索</button>
			</form>
	   		
	   </div>
	</div>
</div>

<div class="col-md-12">
	<div class="btn-group">
	  <button type="button" class="btn btn-default" onclick="addRow()">添加</button>
	  <button type="button" class="btn btn-default" onClick="printContent()">打印</button>
	  <button type="button" class="btn btn-default" onclick="batchDelRows()">批量删除</button>
	  <div class="btn-group-vertical">
	    <button type="button" class="btn btn-default dropdown-toggle" 
	      data-toggle="dropdown" style="border-left:0px;">更多<span class="caret"></span>
	    </button>
	    <ul class="dropdown-menu">
	      <li><a href="#">导出World</a></li>
	      <li><a href="#">导出Excel</a></li>
	      <li><a href="#">导出PDF</a></li>
	    </ul>
	  </div>
	</div>
	<table id="jqGrid" data-toggle="context"></table>
	<div id="jqGridPager"></div>
</div>

<script type="text/javascript"> 
	
	function addRow(){
		openWindow('sys/dictIns/add.do','添加字典实例',480,340,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'添加成功');
				layer.close(index);
				search();
			});
		});
	}
	
	function editRow(id){
		openWindow('sys/dictIns/update.do?id='+id,'修改字典实例',480,340,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'修改成功');
				layer.close(index);
				search();
			});
		});
	}
	
	function delRow(id){
		cusConfirm(null,'删除提示','你真的要删除该数据？',function(){
			senSynReq('sys/dictIns/saveDelete.do?msg=删除失败&id='+id,null,function(msg){
				opStateMsgTip(msg,'删除成功');
				search();
			});
		});
	}
	
	function detailRow(id){
		openDetailWindow('sys/dictIns/detail.do?id='+id,'字典实例详情',480,340);
	}
	
	function batchDelRows(){
		cusConfirm(null,'删除提示','你真的要批量删除这些数据？',function(){
            var rowKey = $("#jqGrid").getGridParam("selrow");
            if (!rowKey){
            	error('至少选中一行数据');
            }else {
                var selectedIds = $("#jqGrid").getGridParam("selarrrow");
                var result = "";
                for (var i = 0; i < selectedIds.length; i++) {
                	 result += selectedIds[i] + ",";
                }
                senSynReq('sys/dictIns/saveBatchDelete.do?msg=批量删除失败&id='+result,null,function(msg){
    				opStateMsgTip(msg,'批量删除成功');
    				search();
    			});
            }
		});
	}
	
	function search(){
		var queryData = {
			'name':$('#name').val(),
			'dictType':$('#dictType').val()
		};
		$("#jqGrid").jqGrid("setGridParam",{ postData: queryData }).trigger("reloadGrid");
	}
	
	$(document).ready(function () {
		
		$("#jqGrid").jqGrid({
	        url: 'sys/dictIns/queryList.do',
	        mtype: 'POST',
	        datatype: "json",
	        colModel: [			
				{ label: '编号', name: 'id', width: 45, key: true,hidden:true },
				{ label: '名称', name: 'name', width: 75 },
				{ label: '类型', name: 'dictType', width: 75 },
				{ label: '系统配置', name: 'system', width: 75,
					formatter:function(cellvalue, options, rowObject){
						return (cellvalue==0)?"否":"是";
					}
				},
				{ label: '描述', name: 'note', width: 90 },
				{ label: '创建时间', name: 'createTime', width: 90 },
				{ label: '排序', name: 'sort', width: 75 },
				{ label: '操作', name: 'id', width: 90,
					formatter:function(cellvalue, options, rowObject){
						var format = '';
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:editRow(\''+cellvalue+'\')">编辑</a>';
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:delRow(\''+cellvalue+'\')">删除</a>';
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:detailRow(\''+cellvalue+'\')">详情</a>';
						if(format.length == 0){
							format = '不可操作';
						}
						return format;
					}
				}
	        ],
	        onSelectRow: function(id){
// 	        	openDetailWindow('sys/dictIns/detail.do?id='+id,'字典实例详情',480,340);
	        },
			loadonce: false,
			viewrecords: true,
			autowidth : true,
			styleUI : "Bootstrap",
			width : '100%',
			height : 200,
	        rowNum: 10,
	        rowList : [5,10,15],
	        rownumbers: true, 
	        rownumWidth: 22, 
	        multiselect: true,
	        pager: "#jqGridPager",
	        jsonReader: {
	        	repeatitems : false
	        }
	    });
	});

</script>