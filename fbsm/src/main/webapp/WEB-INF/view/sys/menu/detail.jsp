<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include  file="../../global/dialog/head_general.jsp"%>
	<style>
		*{
			font-family: "Microsoft YaHei" ! important;
		}
		#form_layout4table{
			margin-top:20px;
			width: :100%;
		}
		#form_layout4table td{
			text-align: right;
			padding:5px;
		}
	
		#form_layout4table td:first-child{
			padding-right:20px;
		}
		
		#form_layout4table td:nth-child(2){
			padding-right:20px;
		}
	</style>
</head>
<body>
<div class="container">
	
	<form id="form">
		<table id="form_layout4table" style="width: 100%">
			<tr>
				<td width="30%">名&nbsp;&nbsp;&nbsp;称:</td>
				<td width="70%"><input type="text" name="name" class="form-control input-sm" minlength="2" required value="${result.name }"/></td>
			</tr>
			<tr>
				<td width="30%">上&nbsp;&nbsp;&nbsp;级:</td>
				<td width="70%"><input type="text" name="parentId" class="form-control input-sm" minlength="1" required value="${result.parentId }"/></td>
			</tr>
			<tr>
				<td width="30%">地&nbsp;&nbsp;&nbsp;址:</td>
				<td width="70%"><input type="text" name="url" class="form-control input-sm" value="${result.url }"/></td>
			</tr>
			<tr>
				<td width="30%">图&nbsp;&nbsp;&nbsp;标:</td>
				<td width="70%"><input type="text" name="icon" class="form-control input-sm" value="${result.icon }"/></td>
			</tr>
			<tr>
				<td width="30%">排&nbsp;&nbsp;&nbsp;序:</td>
				<td width="70%"><input type="number" name="sort" class="form-control input-sm" minlength="1" requiredonkeyup="if(!/^\d+$/.test(this.value)) {alert('只能输入数字 !'); this.value=this.value.replace(/[^\d]+/g,'');}" value="${result.sort }"/></td>
			</tr>
			<tr>
				<td width="30%">描&nbsp;&nbsp;&nbsp;述:</td>
				<td width="70%"><textarea name="note" class="form-control input-sm" disabled="disabled">${result.note }</textarea></td>
			</tr>
		</table>
	</form>
	
	<script type="text/javascript">
		$(function(){
			 $("input:text").attr("disabled",true);
		});
	</script>
	
</div>
<%@include  file="../../global/dialog/footer_general.jsp"%>
