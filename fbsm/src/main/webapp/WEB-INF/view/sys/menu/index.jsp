<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<p style="color:#23527C;"><span style="font-size: 14px;padding-right: 10px;color:black;">说明:</span>菜单管理。</p>
	   </div>
	</div>
</div>

<div class="col-md-12">
   	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">系统菜单<button class="btn btn-default btn-sm pull-right" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="search()">刷新</button></h3>
	   </div>
	   <div class="panel-body">
	   		
<!-- 	   		<form class="form-inline"> -->
<!-- 			  <input type="text" class="span3" placeholder="名称" id="name"> -->
<!-- 			  <button class="btn btn-default btn-sm active" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="search()">搜索</button> -->
<!-- 			</form> -->
	   		<div class="btn-group">
	  <button type="button" class="btn btn-default" onclick="addRow()">添加</button>
	  <button type="button" class="btn btn-default" onClick="printContent()">打印</button>
	  <div class="btn-group-vertical">
	    <button type="button" class="btn btn-default dropdown-toggle" 
	      data-toggle="dropdown" style="border-left:0px;">更多<span class="caret"></span>
	    </button>
	    <ul class="dropdown-menu">
	      <li><a href="#">导出World</a></li>
	      <li><a href="#">导出Excel</a></li>
	      <li><a href="#">导出PDF</a></li>
	    </ul>
	  </div>
	</div>
	<table id="jqGrid" data-toggle="context"></table>
	<div id="jqGridPager"></div>
	   </div>
	</div>
</div>

<div class="col-md-12">
	
</div>

<script type="text/javascript"> 

	function addRow(){
		openWindow('sys/menu/add.do','添加菜单',480,380,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'添加成功');
				layer.close(index);
				search();
			});
		});
	}
	
	function editRow(id){
		openWindow('sys/menu/update.do?id='+id,'修改菜单',480,380,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'修改成功');
				layer.close(index);
				search();
			});
		});
	}
	
	function delRow(id){
		cusConfirm(null,'删除提示','你真的要删除该数据？',function(){
			senSynReq('sys/menu/saveDelete.do?msg=删除失败&id='+id,null,function(msg){
				opStateMsgTip(msg,'删除成功');
				search();
			});
    	});
	}
	
	function detailRow(id){
		openDetailWindow('sys/menu/detail.do?id='+id,'菜单详情',480,380);
	}

	function search(){
		$("#jqGrid").jqGrid("setGridParam",{ postData: {} }).trigger("reloadGrid");
	}
	
	$(document).ready(function () {
		
		$("#jqGrid").jqGrid({
		   url: 'sys/menu/queryList.do',
	       mtype: 'POST',
	       datatype: "json",
		   height: "auto",
		   loadui: "disable",
		   colModel: [
				{ label: 'Id', name: 'id', width: 200, key: true,hidden:true},
				{ label: '名称', name: 'name', width: 200, resizable: false },
				{ label: 'url地址', name: 'path', width: 90,
					formatter:function(cellvalue, options, rowObject){
						if(undefined != cellvalue){
							return "<a href='"+cellvalue+"' target='_blank'>"+cellvalue+"</a>";
						}
						return "";
					}
				},
				{ label: '创建时间', name: 'createTime', width: 90 },
				{ label: '排序', name: 'sort', width: 45},
				{ label: '操作', name: 'id', width: 90,
					formatter:function(cellvalue, options, rowObject){
						var format = '';
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:editRow(\''+cellvalue+'\')">编辑</a>';
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:delRow(\''+cellvalue+'\')">删除</a>';
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:detailRow(\''+cellvalue+'\')">详情</a>';
						if(format.length == 0){
							format = '不可操作';
						}
						return format;
					}
				}
		   ],
		   treeGrid: true,
		   loadonce: false,
		   viewrecords: true,
		   autowidth : true,
		   styleUI : "Bootstrap",
		   width : '100%',
		   treeGridModel: "adjacency",
		   ExpandColumn: "name",
		   treeIcons: {leaf:'ui-icon-document-b'},
		   autowidth: true,
		   ExpandColClick: true,
		   jsonReader: {
		       repeatitems: false,
		       root: "response"
		   }
		});
	});

</script>
