<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include  file="../../global/dialog/head_general.jsp"%>
	<style>
		*{
			font-family: "Microsoft YaHei" ! important;
		}
		#form_layout4table{
			margin-top:20px;
			width: :100%;
		}
		#form_layout4table td{
			text-align: right;
			padding:5px;
		}
	</style>
	<link href="resource/ui/plugins/upload/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
	<script src="resource/ui/plugins/upload/js/fileinput.js" type="text/javascript"></script>
	<script src="resource/ui/plugins/upload/js/fileinput_locale_zh.js" type="text/javascript"></script>
</head>
<body>
<div class="container">
	
	<form id="form" enctype="multipart/form-data" method="post">
		<table id="form_layout4table" style="width: 100%">
			<tr>
				<td width="100%">
					<input id="file-general" type="file" name="files">					
				</td>
			</tr>
		</table>
	</form>
	
	<script type="text/javascript">
		var uploadFilePath;
		
		$(function(){
			 $("#file-general").fileinput({
			        uploadUrl: 'sys/file/zipSave.do',
			        allowedFileExtensions : ['jpg', 'png','gif','doc','zip'],
			        overwriteInitial: false,
			        maxFileSize: 10240,
			        maxFilesNum: 1,
			        enctype: 'multipart/form-data',
	                showUpload: true, //是否显示上传按钮
	                showCaption: false,//是否显示标题
			 });

			 $("#file-general").on("fileuploaded", function(event, data, previewId, index) {
				 var obj = data.response;
				 $.each(obj,function(n,value) {
					 uploadFilePath = value;
				 });
				 successful('文件上传成功');
			 });
		})
		
		function validate(){
			return true;
		}
		
		function submit(index,call){
			call(uploadFilePath);
		}
	</script>
	
</div>
<%@include  file="../../global/dialog/footer_general.jsp"%>
