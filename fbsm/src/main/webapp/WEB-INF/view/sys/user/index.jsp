<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>

<div class="col-md-12">
   	<div id="aa" class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<p style="color:#23527C;"><span style="font-size: 14px;padding-right: 10px;color:black;">说明:</span>系统用户,是系统提供给数据管理员的统一入口。</p>
	   </div>
	</div>
</div>

<div class="col-md-12">
   	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">查询条件</h3>
	   </div>
	   <div class="panel-body">
	   		
	   		<form class="form-inline">
			  <input type="text" class="span3" placeholder="名称" id="userName">
			  
			  
			  <button class="btn btn-default btn-sm active" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="search()">搜索</button>
			</form>
	   		
	   </div>
	</div>
</div>

<div class="col-md-12">
	<div class="btn-group">
		<shiro:hasPermission name="user:add">
			<button type="button" class="btn btn-default" onClick="addRow()">添加</button>
		</shiro:hasPermission>
		<a type="button" class="btn btn-default" href="sys/user/print.do" target= "_blank" >打印</a>
		<shiro:hasPermission name="user:delBatch">
		  <button type="button" class="btn btn-default">批量删除</button>
		</shiro:hasPermission>
	  <div class="btn-group-vertical">
	    <button type="button" class="btn btn-default dropdown-toggle" 
	      data-toggle="dropdown" style="border-left:0px;">更多<span class="caret"></span>
	    </button>
	    <ul class="dropdown-menu">
	      <li><a href="javascript:exportWord()">导出World</a></li>
	      <li><a href="javascript:exportExcel()">导出Excel</a></li>
	      <li><a href="#">导出PDF</a></li>
	    </ul>
	  </div>
	</div>
	<table id="jqGrid" data-toggle="context"></table>
	<div id="jqGridPager"></div>
</div>
<script type="text/javascript"> 
	
	function exportWord(){
		callAjaxPostBak("sys/user/exportWord.do");
	}

	function exportExcel(){
		var ids=$("#jqGrid").jqGrid('getGridParam','selarrrow');
		callAjaxPostBak("sys/user/exportExcelByUserId.do?id="+ids);
	}

	function search(){
		var queryData = {
			'name':$('#userName').val()
		};
		$("#jqGrid").jqGrid("setGridParam",{ postData: queryData }).trigger("reloadGrid");
	}
	
	<shiro:hasPermission name="user:add">
	function addRow(){
		openWindow('sys/user/add.do','添加用户',480,300,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'添加成功');
				layer.close(index);
				search();
			});
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="user:update">
	function editRow(id){
		openWindow('sys/user/update.do?id='+id,'修改用户',480,300,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'修改成功');
				layer.close(index);
				search();
			});
		});
	}
	</shiro:hasPermission>
	
	<shiro:hasPermission name="user:delete">
	function delRow(id){
		cusConfirm(null,'删除提示','你真的要删除该数据？',function(){
			senSynReq('sys/user/saveDelete.do?msg=删除失败&id='+id,null,function(msg){
				opStateMsgTip(msg,'删除成功');
				search();
			});
    	});
	}
	</shiro:hasPermission>
	
	function detailRow(id){
		openDetailWindow('sys/user/detail.do?id='+id,'用户详情',480,300);
	}
	
	function userRole(id){
		openWindow('sys/user/userRole.do?id='+id,'用户角色权限',480,500,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'用户角色授权成功');
				layer.close(index);
				search();
			});
		});
	}
	
	$(document).ready(function () {
		
		$("#jqGrid").jqGrid({
	        url: 'sys/user/queryList.do',
	        mtype: 'POST',
	        datatype: "json",
	        colModel: [			
				{ label: '编号', name: 'id', width: 45, key: true,hidden:true },
				{ label: '名称', name: 'name', width: 75 },
				{ label: '描述', name: 'note', width: 90 },
				{ label: '创建时间', name: 'createTime', width: 90 },
				{ label: '操作', name: 'id', width: 90,
					formatter:function(cellvalue, options, rowObject){
						var format = '';
						<shiro:hasPermission name="user:update">
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:editRow(\''+cellvalue+'\')">编辑</a>';
						</shiro:hasPermission>
						<shiro:hasPermission name="user:delete">
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:delRow(\''+cellvalue+'\')">删除</a>';
						</shiro:hasPermission>
						format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:detailRow(\''+cellvalue+'\')">详情</a>';
						format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:userRole(\''+cellvalue+'\')">用户角色授权</a>';
						if(format.length == 0){
							format = '不可操作';
						}
						return format;
					}
				}
	        ],
	        onSelectRow: function(id){
	        	//openDetailWindow('sys/user/detail.do?id='+id,'用户详情',480,240);
	        },
			loadonce: false,
			viewrecords: true,
			autowidth : true,
			styleUI : "Bootstrap",
			width : '100%',
			height : 340,
			setGridHeight:'auto',
	        rowNum: 10,
	        rowList : [5,10,15],
	        rownumbers: true, 
	        rownumWidth: 22, 
	        multiselect: true,
	        pager: "#jqGridPager",
	        jsonReader: {
	        	repeatitems : false
	        }
	    });
	});

</script>