<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
.cus-chart:HOVER{
	border:1px solid red;
	cursor:pointer;
/* 	box-shadow: 5px 5px 2px #888888; */
}
</style>
<script src="resource/ui/plugins/charts/com/www/js/echarts.js"></script>

<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<p style="color:#23527C;"><span style="font-size: 14px;padding-right: 10px;color:black;">说明:</span>硬盘设备性能监控</p>
	   </div>
	</div>
</div>


<div class="col-md-12">
	<c:forEach var="dick" items="${dickChartInfos }">
		<div class="col-md-3">
		   	<div class="panel panel-default cus-chart" style="background: #E9E9E9;">
		   		${dick }
			</div>
		</div>
	</c:forEach>
</div>

<div class="col-md-12">
   	<fieldset>
		<legend>磁盘使用详情</legend>
		<table id="jqGrid" data-toggle="context"></table>
	</fieldset>
</div>
<script type="text/javascript">
	$(function(){
		$("#jqGrid").jqGrid({
	        url: 'monitor/phyDevice/queryDiskInfoList.do',
	        mtype: 'POST',
	        datatype: "json",
	        colModel: [			
				{ label: '分区盘符名称', name: 'devName', width: 45, key: true },
				{ label: '文件系统类型1', name: 'sysTypeName', width: 75 },
				{ label: '文件系统类型名', name: 'typeName', width: 90 },
				{ label: '文件系统类型2', name: 'type', width: 90 ,hidden:true},
				{ label: '文件系统总大小(GB)', name: 'total', width: 90 },
				{ label: '文件系统剩余大小(GB)', name: 'free', width: 90 },
				{ label: '文件系统可用大小(GB)', name: 'avail', width: 90 },
				{ label: '文件系统已经使用量(GB)', name: 'used', width: 90 },
				{ label: '文件系统资源的利用率(%)', name: 'usePercent', width: 90,
					formatter:function(cellvalue, options, rowObject){
						return (cellvalue >= 90)?"<font color='red'>"+cellvalue+"</font>":"<font color='green'>"+cellvalue+"</font>";
					}
				},
				{ label: '读文件系统(MB/S)', name: 'diskReads', width: 90 },
				{ label: '写文件系统(MB/S)', name: 'diskWrites', width: 90 }
	        ],
			loadonce: false,
			viewrecords: true,
			autowidth : true,
			styleUI : "Bootstrap",
			width : '100%',
			height : 160,
			setGridHeight:'auto',
	        rowNum: 1000,
	        rownumbers: true, 
	        rownumWidth: 22, 
	        jsonReader: {
	        	repeatitems : false
	        }
	    });
	})
</script>


     