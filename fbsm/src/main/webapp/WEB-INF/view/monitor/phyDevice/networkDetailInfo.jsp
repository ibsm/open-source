<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="col-md-12">
	<div id="networkChart" class="panel-body" style="padding:5px 5px 0px 15px;height: 300px;"></div>
	<script type="text/javascript">
	var sendData = [0],acceptData = [0],timeData = [0],chart;
	var option = {
		    title : {
		        text: '网络流量关系图',
		        subtext: '',
		        x: 'center'
		    },
		    tooltip : {
		        trigger: 'axis',
		        formatter: function(params) {
		            return params[0].name + '<br/>'
		                   + params[0].seriesName + ' : ' + params[0].value + ' (kbps)<br/>'
		                   + params[1].seriesName + ' : ' + params[1].value + ' (kbps)';
		        }
		    },
		    legend: {
		        data:['发送','接收'],
		        x: 'left'
		    },
		    toolbox: {
		        show : false,
		        feature : {
		            mark : {show: true},
		            dataView : {show: true, readOnly: false},
		            magicType : {show: true, type: ['line', 'bar']},
		            restore : {show: true},
		            saveAsImage : {show: true}
		        }
		    },
		    dataZoom : {
		        show : true,
		        realtime : true,
		        start : 0,
		        end : 100
		    },
		    xAxis : [
		        {
		            type : 'category',
		            boundaryGap : false,
		            axisLine: {onZero: false},
		            data : timeData
		        }
		    ],
		    yAxis : [
		        {
		            name : '发送(kbps)',
		            type : 'value',
		            max : 50000
		        },
		        {
		            name : '接收(kbps)',
		            type : 'value',
		            axisLabel : {
		                formatter: function(v){
		                    return - v;
		                }
		            }
		        }
		    ],
		    series : [
		        {
		            name:'发送',
		            type:'line',
		            itemStyle: {normal: {areaStyle: {type: 'default'}}},
		            data:sendData
		        },
		        {
		            name:'接收',
		            type:'line',
		            yAxisIndex:1,
		            itemStyle: {normal: {areaStyle: {type: 'default'}}},
		            data: (function(){
		                var oriData = acceptData;
		                var len = oriData.length;
		                while(len--) {
		                    oriData[len] *= -1;
		                }
		                return oriData;
		            })()
		        }
		    ]
		};
	
	function requireCallback(ec) {
		var domMain = document.getElementById('networkChart');
	    chart = ec.init(domMain, 'macarons').setOption(option, true);
	}
	
	require.config({
		paths: {
			echarts: './resource/ui/plugins/charts/com/www/js'
		}
	});
	
	launch();
	
	function launch() {
	    require(
	        [
	            'echarts',
	            'resource/ui/plugins/charts/com/theme/macarons',
	            'echarts/chart/line',
	        ],
	        requireCallback
	    );
	}
	

	var i = 0;  
	function loadChartData(){  
	    if(i < ${executeTime}){
	    	i = i + ${executeIntervalTime};
	    	$.ajax({
				type:'post',
				url:'monitor/phyDevice/getNetworkChartData.do?belongNetworkCard=${belongNetworkCard}',
				success: function(data){
					var json = jQuery.parseJSON(data);
					sendData.push(json.send);
					acceptData.push(json.accept);
					timeData.push(json.time);
					
// 					chart.dispose();
// 					var domMain = document.getElementById('networkChart');
					chart.setOption(option);
				}
			});
	    }
	}  
	setInterval(loadChartData,${executeIntervalTime});  
	
</script>
</div>

<div class="col-md-12">
	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">网络详情概览</h3>
	   </div>
	   <div class="panel-body">
	   		<div class="col-md-4">
				<fieldset>
					<legend>基本信息</legend>
					<table class="form_layout4table" style="width: 100%">
						<tr>
							<td width="30%">当前机器的IP地址:</td>
							<td width="70%" class="cus-details-info">${baseInfo.domainName }</td>
						</tr>
						<tr>
							<td width="30%">正式域名:</td>
							<td width="70%" class="cus-details-info">${baseInfo.localIp }</td>
						</tr>
						<tr>
							<td width="30%">本机mac地址:</td>
							<td width="70%" class="cus-details-info">${baseInfo.localMac }</td>
						</tr>
						<tr>
							<td width="30%">默认网关:</td>
							<td width="70%" class="cus-details-info">${baseInfo.netInfo.defaultGateway }</td>
						</tr>
						<tr>
							<td width="30%">本机名称:</td>
							<td width="70%" class="cus-details-info">${baseInfo.netInfo.hostName }</td>
						</tr>
						<tr>
							<td width="30%">首选DNS服务器:</td>
							<td width="70%" class="cus-details-info">${baseInfo.netInfo.primaryDns }</td>
						</tr>
						<tr>
							<td width="30%">备用DNS服务器:</td>
							<td width="70%" class="cus-details-info">${baseInfo.netInfo.secondaryDns }</td>
						</tr>
					</table>
				</fieldset>
			</div>
			<div class="col-md-4">
				<fieldset>
					<legend>使用率详情</legend>
					<table class="form_layout4table" style="width: 100%">
						<tr>
							<td width="30%">IP地址:</td>
							<td width="70%" class="cus-details-info">${netInterfaceConfig.address }</td>
						</tr>
						<tr>
							<td width="30%">子网掩码:</td>
							<td width="70%" class="cus-details-info">${netInterfaceConfig.broadcast }</td>
						</tr>
						<tr>
							<td width="30%">网卡MAC地址:</td>
							<td width="70%" class="cus-details-info">${netInterfaceConfig.hwaddr }</td>
						</tr>
						<tr>
							<td width="30%">网卡描述信息:</td>
							<td width="70%" class="cus-details-info">${netInterfaceConfig.description }</td>
						</tr>
					</table>
				</fieldset>
			</div>
			<div class="col-md-4">
				<fieldset>
					<legend>使用率详情</legend>
					<table class="form_layout4table" style="width: 100%">
						<tr>
							<td width="30%">接收的总包裹数:</td>
							<td width="70%" class="cus-details-info">${netInterfaceStat.rxPackets }</td>
						</tr>
						<tr>
							<td width="30%">发送的总包裹数:</td>
							<td width="70%" class="cus-details-info">${netInterfaceStat.txPackets }</td>
						</tr>
						<tr>
							<td width="30%">接收到的总字节数:</td>
							<td width="70%" class="cus-details-info">${netInterfaceStat.rxBytes }</td>
						</tr>
						<tr>
							<td width="30%">发送的总字节数:</td>
							<td width="70%" class="cus-details-info">${netInterfaceStat.txBytes }</td>
						</tr>
						<tr>
							<td width="30%">接收到的错误包数:</td>
							<td width="70%" class="cus-details-info">${netInterfaceStat.rxErrors }</td>
						</tr>
						<tr>
							<td width="30%">发送数据包时的错误数:</td>
							<td width="70%" class="cus-details-info">${netInterfaceStat.txErrors }</td>
						</tr>
						<tr>
							<td width="30%">接收时丢弃的包数:</td>
							<td width="70%" class="cus-details-info">${netInterfaceStat.rxDropped }</td>
						</tr>
						<tr>
							<td width="30%">发送时丢弃的包数:</td>
							<td width="70%" class="cus-details-info">${netInterfaceStat.txDropped }</td>
						</tr>
					</table>
				</fieldset>
			</div>
	   </div>
	</div>
</div>
