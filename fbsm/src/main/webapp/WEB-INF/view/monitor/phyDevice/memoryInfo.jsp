<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
.cus-chart:HOVER{
	border:1px solid red;
	cursor:pointer;
/* 	box-shadow: 5px 5px 2px #888888; */
}

.form_layout4table{
	margin-top:0px;
	width: :100%;
}
.form_layout4table td{
	text-align: left;
	padding:5px;
}

.form_layout4table td:first-child{
	padding-right:20px;
}

.form_layout4table td:nth-child(2){
	padding-right:20px;
}
</style>
<script src="resource/ui/plugins/charts/com/www/js/echarts.js"></script>

<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<p style="color:#23527C;"><span style="font-size: 14px;padding-right: 10px;color:black;">说明:</span>内存设备性能监控</p>
	   </div>
	</div>
</div>

<div class="col-md-12">
	<c:forEach var="memory" items="${memoryChartInfos }">
		<div class="col-md-6">
		   	<div class="panel panel-default cus-chart" style="background: #E9E9E9;">
		   		${memory }
			</div>
		</div>
	</c:forEach>
</div>

<div class="col-md-12">
	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">内存概览</h3>
	   </div>
	   <div class="panel-body">
	   		<div class="col-md-5">
				<fieldset>
					<legend>物理内存</legend>
					<table class="form_layout4table" style="width: 100%">
						<tr>
							<td width="30%">实际内存剩余:</td>
							<td width="70%" class="cus-details-info" colspan="3">${physicalMemory.actualFree }</td>
						</tr>
						<tr>
							<td width="30%">实际内存使用:</td>
							<td width="70%" class="cus-details-info">${physicalMemory.actualUsed }</td>
						</tr>
						<tr>
							<td width="30%">当前内存剩余:</td>
							<td width="70%" class="cus-details-info">${physicalMemory.free }</td>
						</tr>
						<tr>
							<td width="30%">缓冲存储器数量:</td>
							<td width="70%" class="cus-details-info">${physicalMemory.ram }</td>
						</tr>
						<tr>
							<td width="30%">内存总量:</td>
							<td width="70%" class="cus-details-info">${physicalMemory.total }</td>
						</tr>
						<tr>
							<td width="30%">当前内存使用量:</td>
							<td width="70%" class="cus-details-info">${physicalMemory.used }</td>
						</tr>
						<tr>
							<td width="30%">当前内存使用百分比:</td>
							<td width="70%" class="cus-details-info">${physicalMemory.usedPercent }</td>
						</tr>
						<tr>
							<td width="30%">当前内存剩余百分比:</td>
							<td width="70%" class="cus-details-info">${physicalMemory.freePercent }</td>
						</tr>
					</table>
				</fieldset>
			</div>
			<div class="col-md-7">
				<fieldset>
					<legend>交换区内存</legend>
					<table class="form_layout4table" style="width: 100%">
						<tr>
							<td width="30%">当前交换区剩余量:</td>
							<td width="70%" class="cus-details-info" colspan="3">${warpMemory.free }</td>
						</tr>
						<tr>
							<td width="30%">页面缓冲池:</td>
							<td width="70%" class="cus-details-info">${warpMemory.pageIn }</td>
						</tr>
						<tr>
							<td width="30%">非页面缓冲池:</td>
							<td width="70%" class="cus-details-info">${warpMemory.pageOut }</td>
						</tr>
						<tr>
							<td width="30%">交换区总量:</td>
							<td width="70%" class="cus-details-info">${warpMemory.total }</td>
						</tr>
						<tr>
							<td width="30%">当前交换区使用量:</td>
							<td width="70%" class="cus-details-info">${warpMemory.used }</td>
						</tr>
					</table>
				</fieldset>
			</div>
	   </div>
	</div>
</div>