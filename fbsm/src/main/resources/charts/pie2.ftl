<div id="${chartId}" class="${css}" style="${style}"></div>
<script type="text/javascript">

function requireCallback_${chartId}(ec) {
	var option = {
	    title : {
	        text: '${title01}',
	        subtext: '${title02}',
	        x:'center'
	    },
	    tooltip : {
	        trigger: 'item',
	        formatter: "{a} <br/>{b} : {c}${unit} ({d}%)"
	    },
	    legend: {
	        orient : 'vertical',
	        x : 'left',
	        data:${dataColumns}
	    },
	    toolbox: {
	        show : true,
	        feature : {
	            mark : {show: false},
	            dataView : {show: false, readOnly: false},
	            restore : {show: true},
	            saveAsImage : {show: false}
	        }
	    },
	    calculable : true,
	    series : [
	        {
	            name:'${dataFrom}',
	            type:'pie',
	            radius : ['50%', '70%'],
	            itemStyle : {
	                normal : {
	                    label : {
	                        show : true
	                    },
	                    labelLine : {
	                        show : true
	                    }
	                },
	                emphasis : {
	                    label : {
	                        show : true,
	                        position : 'center',
	                        textStyle : {
	                            fontSize : '30',
	                            fontWeight : 'bold'
	                        }
	                    }
	                }
	            },
	            data:${chartDatas}
	        }
	    ]
	};

	var domMain = document.getElementById('${chartId}');
    ec.init(domMain, 'macarons').setOption(option, true);
}

require.config({
	paths: {
		echarts: './resource/ui/plugins/charts/com/www/js'
	}
});

launch_${chartId}();

function launch_${chartId}() {
    require(
        [
            'echarts',
            'resource/ui/plugins/charts/com/theme/macarons',
            'echarts/chart/pie'
        ],
        requireCallback_${chartId}
    );
}
</script>