/*系统用户表*/
CREATE TABLE `cmdb_sys_user` (
  `id` varchar(36) NOT NULL,
  `name` varchar(64) NOT NULL,
  `pwd` varchar(64) NOT NULL,
  `note` varchar(128) DEFAULT NULL,
  `state` int(11) NOT NULL,
  `create_user` varchar(36) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*系统菜单表*/
CREATE TABLE `cmdb_sys_menu` (
  `id` varchar(36) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `url` varchar(200) DEFAULT NULL,
  `icon` varchar(200) DEFAULT NULL,
  `parent_id` varchar(36) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `create_user` varchar(36) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*系统管理角色表*/
CREATE TABLE `cmdb_sys_role` (
  `id` varchar(36) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `note` varchar(128) DEFAULT NULL,
  `state` int(1) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `create_user` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*字典类型*/
CREATE TABLE `cmdb_sys_dict` (
  `id` varchar(36) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `note` varchar(128) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `create_user` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*字典实例*/
CREATE TABLE `cmdb_sys_dictins` (
  `id` varchar(36) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `note` varchar(128) DEFAULT NULL,
  `dict_type` varchar(36) DEFAULT NULL,
  `system` int(1) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `state` int(1) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `create_user` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*系统权限*/
CREATE TABLE `cmdb_sys_per` (
  `id` varchar(36) NOT NULL,
  `pre_type` varchar(36) NOT NULL COMMENT '权限分类，数据字典配置',
  `name` varchar(64) NOT NULL,
  `code` varchar(128) DEFAULT NULL,
  `note` varchar(128) DEFAULT NULL,
  `state` int(1) NOT NULL,
  `create_time` datetime NOT NULL,
  `create_user` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*用户角色*/
CREATE TABLE `cmdb_sys_user_role` (
  `id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `role_id` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*角色系统数据权限*/
CREATE TABLE `cmdb_sys_role_per` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) NOT NULL,
  `per_id` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*角色菜单权限*/
CREATE TABLE `cmdb_sys_role_menu` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) NOT NULL,
  `menu_id` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*工作流节点定义*/
CREATE TABLE `act_cus_node` (
  `id` varchar(36) NOT NULL,
  `name` varchar(64) DEFAULT NULL,
  `flow_type` varchar(36) DEFAULT NULL,
  `note` varchar(128) DEFAULT NULL,
  `state` int(1) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `create_user` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*工作流节点和处理人之间关系定义*/
CREATE TABLE `act_cus_node_ref` (
  `id` varchar(36) NOT NULL,
  `exe_id` varchar(36) DEFAULT NULL,
  `node_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;