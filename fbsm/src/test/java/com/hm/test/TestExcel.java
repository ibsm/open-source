package com.hm.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.hm.common.util.CellNature;
import com.hm.common.util.ExportExcel;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 测试excel
 */
public class TestExcel{

	private HttpServletRequest request;  
    private HttpServletResponse response;  
      
    @Before  
    public void setUp(){  
          
    	request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
    } 
	
    @Test
    public void testExcel4Obj() throws Exception{
    	List<TestEntity> content = new ArrayList<TestExcel.TestEntity>();
    	{
    		TestEntity testEntity = new TestEntity();
    		testEntity.setId(UUID.randomUUID().toString());
    		testEntity.setName("张三");
    		testEntity.setBirthday(new Date());
    		testEntity.setAge(90);
    		
    		content.add(testEntity);
    	}
    	
    	ExportExcel excel = new ExportExcel().createAdvanced().content(content, TestEntity.class);
    	{
    		excel.setCell(new CellNature().simple("编号", "id"));
    		excel.setCell(new CellNature().simple("名称", "name"));
    		excel.setCell(new CellNature().simple("年龄", "age"));
    		excel.setCell(new CellNature().simple("生日", "birthday"));
    	}
    	
    	excel.write2(response);
    }
    
	@Test
	public void testExcel4simple(){
		try {
			List<List<String>> content = new ArrayList<List<String>>();
			{
				List<String> data = new ArrayList<String>();
				data.add(UUID.randomUUID().toString());
				data.add("张三");
				data.add("zhangsan");
				data.add("60");
				
				content.add(data);
			}
			{
				List<String> data = new ArrayList<String>();
				data.add(UUID.randomUUID().toString());
				data.add("李四");
				data.add("lisi");
				data.add("630");
				
				content.add(data);
			}
			
			ExportExcel excel = new ExportExcel().createSimple().content(content);
			
			{
				excel.setHeader("编号");
				excel.setHeader("名称");
				excel.setHeader("备注");
				excel.setHeader("金额");
			}
			
			excel.write(response);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	class TestEntity{
		private String id;
		private String name;
		private Date birthday;
		private int age;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Date getBirthday() {
			return birthday;
		}
		public void setBirthday(Date birthday) {
			this.birthday = birthday;
		}
		public int getAge() {
			return age;
		}
		public void setAge(int age) {
			this.age = age;
		}
	}
}
