package com.hiwan.api.restful;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hiwan.api.entity.CmdbSysDict;
import com.hiwan.api.service.SysDictTypeService;
import com.hiwan.common.BaseAction;

@Controller
@RequestMapping("sys/dictType")
public class SysDictTypeApi extends BaseAction {

	/*
	 * GET（SELECT）：从服务器取出资源（一项或多项）。 
	 * POST（CREATE）：在服务器新建一个资源。
	 * PUT（UPDATE）：在服务器更新资源（客户端提供改变后的完整资源）。 
	 * PATCH（UPDATE）：在服务器更新资源（客户端提供改变的属性）。
	 * DELETE（DELETE）：从服务器删除资源。
	 */
	
	@Resource
	private SysDictTypeService sysDictTypeService;

	@RequestMapping(value="get/all")
	public void getAllDictType(){
		JSONObject obj = new JSONObject();
		obj.put("3456", "fghjkl;");
		
		List<CmdbSysDict> allDictType = sysDictTypeService.getAllDictType();
		putJsonArray(allDictType);
	}
}
