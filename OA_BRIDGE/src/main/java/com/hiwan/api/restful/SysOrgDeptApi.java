package com.hiwan.api.restful;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.InputStream;

import javax.annotation.Resource;
import javax.imageio.ImageIO;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hiwan.api.entity.SysOrgDept;
import com.hiwan.api.service.SysOrgDeptService;
import com.hiwan.common.BaseAction;
import com.hiwan.common.Configuration;
import com.hiwan.common.DataUtil.QrCodeUtil;
import com.hiwan.common.PageSet;
import com.hiwan.common.R;

@Controller
@RequestMapping("sys/orgDep")
public class SysOrgDeptApi extends BaseAction {

	@Resource
	private SysOrgDeptService sysOrgDeptService;
	
	//http://localhost:8080/OA_BRIDGE/sys/orgDep/get/all.do
	@RequestMapping(value="get/all")
	public void selectAll(){
		System.out.println(sysOrgDeptService);
		JSONObject obj = new JSONObject();
		obj.put("3456", "fghjkl;");
//		putJson(obj);
		putJsonArray(sysOrgDeptService.selectAll());
	}
	
	//http://localhost:8080/OA_BRIDGE/sys/orgDep/queryList.do?page=1&rows=10
	/**
	 * 分页请求
	 * @param sysOrgDept
	 */
	@RequestMapping(value="queryList")
	public void queryList(SysOrgDept sysOrgDept){
		PageSet pageSet = sysOrgDeptService.queryList(sysOrgDept,getPageSet());
		putJson(pageSet);
	}
	
	//http://localhost:8080/OA_BRIDGE/sys/orgDep/qrCode.do
	@RequestMapping(value="qrCode",method=RequestMethod.GET)
	public void qrCode() throws Exception{
		String content = "使用若干个与二进制相对应的几何形体来表示文字数值信息，通过图象输入设备或光电扫描设备自动识读以实现信息自动处理：它具有条码技术的一些共性：每种码制有其特定的字符集；每个字符占有一定的宽度；具有一定的校验功能等。同时还具有对不同行的信息自动识别功能、及处理图形旋转变化点。";
//		content = "http://www.baidu.com";
		InputStream inputStream = SysOrgDeptApi.class.getResourceAsStream(Configuration.get(R.QrCode.LOGO_PATH));
		BufferedImage logo = ImageIO.read(inputStream);
		new QrCodeUtil().encode(content, logo, response.getOutputStream(), Color.white);
	}
}
