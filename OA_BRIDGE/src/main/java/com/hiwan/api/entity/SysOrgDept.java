package com.hiwan.api.entity;

import java.util.Date;

public class SysOrgDept {
    private String fdId;

    private Long fdOrgType;

    private String fdName;

    private String fdNamePinyin;

    private Long fdOrder;

    private String fdNo;

    private String fdKeyword;

    private Short fdIsAvailable;

    private Short fdIsAbandon;

    private Short fdIsBusiness;

    private String fdImportInfo;

    private Short fdFlagDeleted;

    private String fdLdapDn;

    private String fdMemo;

    private String fdHierarchyId;

    private Date fdCreateTime;

    private Date fdAlterTime;

    private String fdThisLeaderid;

    private String fdViceLeaderid;

    private String fdSuperLeaderid;

    private String fdParentorgid;

    private String fdParentid;

    private String fdCateid;

    public String getFdId() {
        return fdId;
    }

    public void setFdId(String fdId) {
        this.fdId = fdId == null ? null : fdId.trim();
    }

    public Long getFdOrgType() {
        return fdOrgType;
    }

    public void setFdOrgType(Long fdOrgType) {
        this.fdOrgType = fdOrgType;
    }

    public String getFdName() {
        return fdName;
    }

    public void setFdName(String fdName) {
        this.fdName = fdName == null ? null : fdName.trim();
    }

    public String getFdNamePinyin() {
        return fdNamePinyin;
    }

    public void setFdNamePinyin(String fdNamePinyin) {
        this.fdNamePinyin = fdNamePinyin == null ? null : fdNamePinyin.trim();
    }

    public Long getFdOrder() {
        return fdOrder;
    }

    public void setFdOrder(Long fdOrder) {
        this.fdOrder = fdOrder;
    }

    public String getFdNo() {
        return fdNo;
    }

    public void setFdNo(String fdNo) {
        this.fdNo = fdNo == null ? null : fdNo.trim();
    }

    public String getFdKeyword() {
        return fdKeyword;
    }

    public void setFdKeyword(String fdKeyword) {
        this.fdKeyword = fdKeyword == null ? null : fdKeyword.trim();
    }

    public Short getFdIsAvailable() {
        return fdIsAvailable;
    }

    public void setFdIsAvailable(Short fdIsAvailable) {
        this.fdIsAvailable = fdIsAvailable;
    }

    public Short getFdIsAbandon() {
        return fdIsAbandon;
    }

    public void setFdIsAbandon(Short fdIsAbandon) {
        this.fdIsAbandon = fdIsAbandon;
    }

    public Short getFdIsBusiness() {
        return fdIsBusiness;
    }

    public void setFdIsBusiness(Short fdIsBusiness) {
        this.fdIsBusiness = fdIsBusiness;
    }

    public String getFdImportInfo() {
        return fdImportInfo;
    }

    public void setFdImportInfo(String fdImportInfo) {
        this.fdImportInfo = fdImportInfo == null ? null : fdImportInfo.trim();
    }

    public Short getFdFlagDeleted() {
        return fdFlagDeleted;
    }

    public void setFdFlagDeleted(Short fdFlagDeleted) {
        this.fdFlagDeleted = fdFlagDeleted;
    }

    public String getFdLdapDn() {
        return fdLdapDn;
    }

    public void setFdLdapDn(String fdLdapDn) {
        this.fdLdapDn = fdLdapDn == null ? null : fdLdapDn.trim();
    }

    public String getFdMemo() {
        return fdMemo;
    }

    public void setFdMemo(String fdMemo) {
        this.fdMemo = fdMemo == null ? null : fdMemo.trim();
    }

    public String getFdHierarchyId() {
        return fdHierarchyId;
    }

    public void setFdHierarchyId(String fdHierarchyId) {
        this.fdHierarchyId = fdHierarchyId == null ? null : fdHierarchyId.trim();
    }

    public Date getFdCreateTime() {
        return fdCreateTime;
    }

    public void setFdCreateTime(Date fdCreateTime) {
        this.fdCreateTime = fdCreateTime;
    }

    public Date getFdAlterTime() {
        return fdAlterTime;
    }

    public void setFdAlterTime(Date fdAlterTime) {
        this.fdAlterTime = fdAlterTime;
    }

    public String getFdThisLeaderid() {
        return fdThisLeaderid;
    }

    public void setFdThisLeaderid(String fdThisLeaderid) {
        this.fdThisLeaderid = fdThisLeaderid == null ? null : fdThisLeaderid.trim();
    }

    public String getFdViceLeaderid() {
        return fdViceLeaderid;
    }

    public void setFdViceLeaderid(String fdViceLeaderid) {
        this.fdViceLeaderid = fdViceLeaderid == null ? null : fdViceLeaderid.trim();
    }

    public String getFdSuperLeaderid() {
        return fdSuperLeaderid;
    }

    public void setFdSuperLeaderid(String fdSuperLeaderid) {
        this.fdSuperLeaderid = fdSuperLeaderid == null ? null : fdSuperLeaderid.trim();
    }

    public String getFdParentorgid() {
        return fdParentorgid;
    }

    public void setFdParentorgid(String fdParentorgid) {
        this.fdParentorgid = fdParentorgid == null ? null : fdParentorgid.trim();
    }

    public String getFdParentid() {
        return fdParentid;
    }

    public void setFdParentid(String fdParentid) {
        this.fdParentid = fdParentid == null ? null : fdParentid.trim();
    }

    public String getFdCateid() {
        return fdCateid;
    }

    public void setFdCateid(String fdCateid) {
        this.fdCateid = fdCateid == null ? null : fdCateid.trim();
    }
}