package com.hiwan.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hiwan.api.dao.CmdbSysDictMapper;
import com.hiwan.api.entity.CmdbSysDict;
import com.hiwan.common.BaseService;

@Service
public class SysDictTypeService extends BaseService {

	@Autowired
	private CmdbSysDictMapper cmdbSysDictMapper;
	
	public List<CmdbSysDict> getAllDictType(){
		return cmdbSysDictMapper.getAllDictType();
	}
	
}
