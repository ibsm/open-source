package com.hiwan.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hiwan.api.dao.SysOrgDeptMapper;
import com.hiwan.api.entity.SysOrgDept;
import com.hiwan.common.BaseService;
import com.hiwan.common.PageSet;

@Service
public class SysOrgDeptService extends BaseService {

	@Autowired
	private SysOrgDeptMapper sysOrgDeptMapper;
	
	public List<SysOrgDept> selectAll(){
		return sysOrgDeptMapper.selectAll();
	}

	public PageSet queryList(SysOrgDept sysOrgDept, PageSet pageSet) {
		Map<String,Object> params = new HashMap<String,Object>();
		{
//			params.put("name", sysOrgDept.getName());
			params.put("query", pageSet);
		}
		pageSet = assemblyPaging(sysOrgDeptMapper.queryList(params),params);
		return pageSet;
	}
	
}
