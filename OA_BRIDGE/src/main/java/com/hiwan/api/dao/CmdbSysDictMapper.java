package com.hiwan.api.dao;

import java.util.List;

import com.hiwan.api.entity.CmdbSysDict;

public interface CmdbSysDictMapper {
	int deleteByPrimaryKey(String id);

	int insert(CmdbSysDict record);

	CmdbSysDict selectByPrimaryKey(String id);

	int updateByPrimaryKey(CmdbSysDict record);
	
	List<CmdbSysDict> getAllDictType();
}