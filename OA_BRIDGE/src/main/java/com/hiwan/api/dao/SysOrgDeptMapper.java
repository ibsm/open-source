package com.hiwan.api.dao;

import java.util.List;
import java.util.Map;

import com.hiwan.api.entity.SysOrgDept;
import com.hiwan.common.PageSet;

public interface SysOrgDeptMapper {
    int deleteByPrimaryKey(String fdId);

    int insert(SysOrgDept record);

    SysOrgDept selectByPrimaryKey(String fdId);
    
    List<SysOrgDept> selectAll();

    int updateByPrimaryKey(SysOrgDept record);
    
    PageSet queryList(Map<String, Object> pageSet);
}