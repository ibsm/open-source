package com.hiwan.common;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.web.bind.annotation.ModelAttribute;

public abstract class BaseAction {

	protected HttpServletRequest request;
	
	protected HttpServletResponse response;
	
	@ModelAttribute
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response){  
        this.request = request;  
        this.response = response;  
    }
	
	public PageSet getPageSet(){
		PageSet pageSet = new PageSet();
		//设置当前页
		pageSet.setPage(Integer.parseInt(request.getParameter("page")));
		pageSet.setPageSize(Integer.parseInt(request.getParameter("rows")));
//		pageSet.setSidx(request.getParameter("sidx"));
		return pageSet;
	}
	
	public void putJson(Object obj){
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().print(JSONObject.fromObject(obj));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void putJsonArray(Object obj){
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().print(JSONArray.fromObject(obj));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
