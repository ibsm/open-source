package com.hiwan.common;

import java.util.Map;


public abstract class BaseService {

	protected PageSet assemblyPaging(PageSet tmp,Map<String,Object> params){
		PageSet pageSet = (PageSet) params.get("query");
		pageSet.setRows(tmp.getRows());
		pageSet.setRecords(tmp.getTotal());
		return pageSet;
	}
}
