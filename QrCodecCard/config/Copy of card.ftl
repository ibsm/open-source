BEGIN:VCARD
VERSION:3.0
N:${name}
TITLE:${position}
TEL;WORK;VOICE:${phone}
TEL;CELL;VOICE:${cellphone}
TEL;WORK;FAX:${fax}
EMAIL;PREF;INTERNET:${email}
ADR;WORK;POSTAL:${addres};${postcode}
END:VCARD

BEGIN:VCARD
N:姓;名字;;;
FN: 名字  姓 
TITLE:职称等信息
ADR;HOME:;;详细的家庭住址;;;;
ORG:公司/组织名称
TEL;CELL,VOICE:移动电话
TEL;HOME,VOICE:家庭电话
URL;WORK:个人网站
EMAIL;INTERNET,HOME:电子邮箱
BDAY:生日格式为xxxx-xx-xx
END:VCARD

http://www.oschina.net/code/snippet_864431_49605



姓名、单位、职务、电话、手机、邮箱、邮编、单位地址。

BEGIN:VCARD 
FN:${name}
ORG:${unit}
TITLE:${position}
TEL;CELL,VOICE:${cellphone}
TEL;WORK,VOICE:${phone}
EMAIL;INTERNET,HOME:${email}
ADR;WORK:;;${addres};;;;
END:VCARD