package com.hiwan.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class Configuration {
	
	public String filePath = "tmp/welcome.png";
	
	public String tmp;

	private static Configuration configuration;
	
	private static Properties properties;
	
	static{
		try {
			properties = new Properties();
			properties.load(new BufferedReader(new InputStreamReader(new FileInputStream("config/init.properties"), "utf-8")));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Configuration(){}
	
	public static Configuration getInstance(){
		if(null == configuration){
			synchronized (Configuration.class) {
				if(null == configuration){
					configuration = new Configuration();
				}
			}
		}
		return configuration;
	}
	
	public static String get(String key){
		return properties.getProperty(key);
	}
	
	public static Integer getInt(String key){
		return Integer.parseInt(get(key));
	}
}
