package com.hiwan.util;

public interface R {

	public interface QrCode{
//		public String width = "qrCode.width";
//		public String height = "qrCode.height";
//		public String margin = "qrCode.margin";
//		public String color = "qrCode.color";
//		public String logoWidth = "qrCode.logo.width";
//		public String logoHeight = "qrCode.logo.height";
		/**画板尺寸*/
		public String DRAW_BOARD_SIZE = "draw.board.size";
		/**高度*/
		public String SIZE = "qrCode.size";
		/**格式*/
		public String FORMAT = "qrCode.format";
		/**编码*/
		public String CODING = "qrCode.coding";
		/**背景色*/
		public int BACKGROUND_COLOR = 0xFF000000;
		/**数据颜色*/
		public int DATA_COLOR = 0xFFFFFFFF;
		/**边框宽度*/
		public String MARGIN = "qrCode.data.margin";
		/**logo位置*/
		public String LOGO_PATH = "qrCode.logo.path";
		/**logo边距*/
		public String LOGO_MARGIN = "qrCode.logo.margin";
		/**logo占二维码的比例*/
		public String LOGO_BELONG_RATIO = "qrCode.logo.belong.ratio";
		/**logo尺寸*/
		public String LOGO_BELONG_SIZE = "qrCode.logo.size";
		
		public String name = "userInfo.name";
		public String position = "userInfo.position";
		public String unit = "userInfo.unit";
		public String email = "userInfo.email";
		public String phone = "userInfo.phone";
		public String cellphone = "userInfo.cellphone";
		public String addres = "userInfo.addres";
	}
	
	public interface System{
		public String SYSTEM_VERSION = "software.version";
	}
}
