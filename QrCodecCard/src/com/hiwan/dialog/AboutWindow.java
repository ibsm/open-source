package com.hiwan.dialog;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.hiwan.app.MainApplication;
import com.hiwan.util.DataUtil;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 上午11:19:18 2016年2月19日
 * @statement 
 * @team 
 * @email shishun156@gmail.com
 * @describe 关于
 */
@SuppressWarnings("serial")
public class AboutWindow extends JDialog {

	public AboutWindow(MainApplication owner) {
		super(owner, "关于本软件 ", true);
		getContentPane().setLayout(null);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(10, 10, 464, 241);
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		textArea.setAutoscrolls(true);
		try {
			textArea.setText(DataUtil.FileUtil.read("config/about.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		JScrollPane scroll = new JScrollPane(textArea); 
		scroll.setBounds(10, 10, 464, 241);
		scroll.setHorizontalScrollBarPolicy( 
		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED); 
		scroll.setVerticalScrollBarPolicy( 
		JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		getContentPane().add(scroll);
		
		init();
	}
	
	private void init() {
		this.setSize(500, 300);
		this.setLocationRelativeTo(null);
		
	}
}
