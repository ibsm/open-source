package com.hiwan.dialog;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import org.jb2011.lnf.beautyeye.ch3_button.BEButtonUI;

import com.hiwan.app.MainApplication;
import com.hiwan.util.Configuration;
import com.hiwan.util.DataUtil.FtlFactory;
import com.hiwan.util.R;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 上午8:52:08 2016年2月19日
 * @statement 
 * @team 
 * @email shishun156@gmail.com
 * @describe 名片信息维护 
*/
@SuppressWarnings("serial")
public class CallingCardInfoWindow extends JDialog {
	private JTextField name;
	private JTextField position;
	private JTextField unit;
	private JTextField email;
	private JTextField phone;
	private JTextField cellphone;
	private JTextField size;
	private JTextField backgroundColor;
	private JTextField dataColor;
	private JTextField dataMargin;
	private JTextField logoPath;
	private JTextField logoBelongSize;
	private JTextField logoMargin;
	private JTextField logoBelongRatio;
	private JTextField coding;
	private MainApplication application;
	private JTextArea addres;
	
	public CallingCardInfoWindow(MainApplication owner) {
		super(owner, "参数设置 ", true);
		try {
			org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
//			BeautyEyeLNFHelper.translucencyAtFrameInactive=false;
			UIManager.put("ToolBar.isPaintPlainBackground",Boolean.TRUE);
			UIManager.put("ToolBar.background",Color.BLACK);
		} catch (Exception e3) {
			e3.printStackTrace();
		}
		
		this.application = owner;
		getContentPane().setLayout(null);
		this.setIconImage(new ImageIcon("img/logo1.png").getImage());
		
		JLabel lblNewLabel = new JLabel("姓名:");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(20, 23, 63, 15);
		getContentPane().add(lblNewLabel);
		
		name = new JTextField();
		name.setBounds(96, 22, 170, 25);
		name.setText(Configuration.get(R.QrCode.name));
		getContentPane().add(name);
		name.setColumns(10);
		
		JLabel label = new JLabel("职位:");
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBounds(20, 56, 63, 15);
		getContentPane().add(label);
		
		position = new JTextField();
		position.setColumns(10);
		position.setBounds(95, 55, 170, 25);
		position.setText(Configuration.get(R.QrCode.position));
		getContentPane().add(position);
		
		JLabel label_1 = new JLabel("单位/组织:");
		label_1.setBounds(23, 121, 63, 15);
		getContentPane().add(label_1);
		
		unit = new JTextField();
		unit.setColumns(10);
		unit.setBounds(96, 120, 170, 25);
		unit.setText(Configuration.get(R.QrCode.unit));
		getContentPane().add(unit);
		
		JLabel label_2 = new JLabel("电子邮箱:");
		label_2.setHorizontalAlignment(SwingConstants.RIGHT);
		label_2.setBounds(30, 91, 54, 15);
		getContentPane().add(label_2);
		
		email = new JTextField();
		email.setColumns(10);
		email.setBounds(95, 90, 170, 25);
		email.setText(Configuration.get(R.QrCode.email));
		getContentPane().add(email);
		
		JLabel label_3 = new JLabel("工作电话:");
		label_3.setHorizontalAlignment(SwingConstants.RIGHT);
		label_3.setBounds(20, 188, 63, 15);
		getContentPane().add(label_3);
		
		phone = new JTextField();
		phone.setColumns(10);
		phone.setText(Configuration.get(R.QrCode.phone));
		phone.setBounds(95, 187, 170, 25);
		getContentPane().add(phone);
		
		JLabel label_4 = new JLabel("移动电话:");
		label_4.setHorizontalAlignment(SwingConstants.RIGHT);
		label_4.setBounds(31, 153, 54, 15);
		getContentPane().add(label_4);
		
		cellphone = new JTextField();
		cellphone.setColumns(10);
		cellphone.setText(Configuration.get(R.QrCode.cellphone));
		cellphone.setBounds(96, 152, 170, 25);
		getContentPane().add(cellphone);
		
		JLabel label_6 = new JLabel("工作地址:");
		label_6.setHorizontalAlignment(SwingConstants.RIGHT);
		label_6.setBounds(23, 230, 54, 15);
		getContentPane().add(label_6);
		
		addres = new JTextArea();
		addres = new JTextArea();
		addres.setLineWrap(true);
		addres.setText(Configuration.get(R.QrCode.addres));
		addres.setBounds(92, 222, 175, 44);
		
		final JScrollPane scroll = new JScrollPane(addres); 
		
		scroll.setBounds(92, 222, 175, 44);
		scroll.setHorizontalScrollBarPolicy( 
		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED); 
		scroll.setVerticalScrollBarPolicy( 
		JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		getContentPane().add(scroll);
		
		size = new JTextField();
		size.setColumns(10);
		size.setText(Configuration.get(R.QrCode.SIZE));
		size.setBounds(380, 23, 170, 25);
		getContentPane().add(size);
		
		JLabel label_7 = new JLabel("尺寸大小:");
		label_7.setHorizontalAlignment(SwingConstants.RIGHT);
		label_7.setBounds(293, 24, 77, 15);
		getContentPane().add(label_7);
		
		backgroundColor = new JTextField();
		backgroundColor.setColumns(10);
		backgroundColor.setEditable(false);
		backgroundColor.setEnabled(false);
		backgroundColor.setText("0xFF000000");
		backgroundColor.setBounds(380, 57, 170, 25);
		getContentPane().add(backgroundColor);
		
		JLabel label_8 = new JLabel("背景色:");
		label_8.setHorizontalAlignment(SwingConstants.RIGHT);
		label_8.setBounds(293, 58, 77, 15);
		getContentPane().add(label_8);
		
		dataColor = new JTextField();
		dataColor.setColumns(10);
		dataColor.setText("0xFFFFFFFF");
		dataColor.setEnabled(false);
		dataColor.setEditable(false);
		dataColor.setBounds(380, 90, 170, 25);
		getContentPane().add(dataColor);
		
		JLabel label_9 = new JLabel("数据颜色:");
		label_9.setHorizontalAlignment(SwingConstants.RIGHT);
		label_9.setBounds(293, 91, 77, 15);
		getContentPane().add(label_9);
		
		dataMargin = new JTextField();
		dataMargin.setColumns(10);
		dataMargin.setText(Configuration.get(R.QrCode.MARGIN));
		dataMargin.setBounds(380, 120, 170, 25);
		getContentPane().add(dataMargin);
		
		JLabel label_10 = new JLabel("边距宽度:");
		label_10.setHorizontalAlignment(SwingConstants.RIGHT);
		label_10.setBounds(293, 121, 77, 15);
		getContentPane().add(label_10);
		
		logoPath = new JTextField();
		logoPath.setColumns(10);
		logoPath.setText(Configuration.get(R.QrCode.LOGO_PATH));
		logoPath.setBounds(380, 152, 170, 25);
		getContentPane().add(logoPath);
		
		JLabel lblLogo = new JLabel("logo地址:");
		lblLogo.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLogo.setBounds(293, 153, 77, 15);
		getContentPane().add(lblLogo);
		
		logoBelongSize = new JTextField();
		logoBelongSize.setColumns(10);
		logoBelongSize.setText(Configuration.get(R.QrCode.LOGO_BELONG_SIZE));
		logoBelongSize.setBounds(380, 183, 170, 25);
		getContentPane().add(logoBelongSize);
		
		JLabel lblLogo_1 = new JLabel("logo尺寸:");
		lblLogo_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLogo_1.setBounds(293, 184, 77, 15);
		getContentPane().add(lblLogo_1);
		
		logoMargin = new JTextField();
		logoMargin.setColumns(10);
		logoMargin.setBounds(380, 216, 170, 25);
		logoMargin.setText(Configuration.get(R.QrCode.LOGO_MARGIN));
		getContentPane().add(logoMargin);
		
		JLabel lblLogo_2 = new JLabel("logo边距:");
		lblLogo_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLogo_2.setBounds(293, 217, 77, 15);
		getContentPane().add(lblLogo_2);
		
		logoBelongRatio = new JTextField();
		logoBelongRatio.setColumns(10);
		logoBelongRatio.setText(Configuration.get(R.QrCode.LOGO_BELONG_RATIO));
		logoBelongRatio.setBounds(380, 253, 170, 25);
		getContentPane().add(logoBelongRatio);
		
		JLabel lblLogo_3 = new JLabel("logo百分比:");
		lblLogo_3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLogo_3.setBounds(279, 258, 91, 15);
		getContentPane().add(lblLogo_3);
		
		coding = new JTextField();
		coding.setColumns(10);
		coding.setText(Configuration.get(R.QrCode.CODING));
		coding.setBounds(380, 287, 170, 25);
		getContentPane().add(coding);
		
		JLabel label_11 = new JLabel("编码:");
		label_11.setHorizontalAlignment(SwingConstants.RIGHT);
		label_11.setBounds(293, 288, 77, 15);
		getContentPane().add(label_11);
		
		JButton btnNewButton = new JButton("恢复到默认数据");
		btnNewButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String data = new FtlFactory().build("bak.ftl").process(new HashMap<String,String>());
//					FileWriter fileWriter = null;
					try {
//						fileWriter = new FileWriter("config/init.properties");
//						fileWriter.write(data);
//						fileWriter.flush();
//						fileWriter.close();	
						OutputStreamWriter write = new OutputStreamWriter(new FileOutputStream("config/init.properties"),"UTF-8");
					    BufferedWriter writer=new BufferedWriter(write);   
					    writer.write(data);
					    writer.close();	
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				} catch (Exception e2) {
					e2.printStackTrace();
				}
				
				int ifadd = JOptionPane.showConfirmDialog(CallingCardInfoWindow.this, "是否重启使更改配置生效", "提示信息",JOptionPane.YES_NO_OPTION);
				if(ifadd == JOptionPane.YES_OPTION){
					
					CallingCardInfoWindow.this.dispose();
					application.dispose();
				}
			}
		});
		btnNewButton.setBounds(25, 335, 160, 23);
		getContentPane().add(btnNewButton);
		
		JButton button = new JButton("保存");
		button.setBounds(442, 335, 108, 23);
		button.setUI(new BEButtonUI().setNormalColor(BEButtonUI.NormalColor.green));
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String data = getInitContent();
				try {
					OutputStreamWriter write = new OutputStreamWriter(new FileOutputStream("config/init.properties"),"UTF-8");
				    BufferedWriter writer=new BufferedWriter(write);   
				    writer.write(data);
				    writer.close();	
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				int ifadd = JOptionPane.showConfirmDialog(CallingCardInfoWindow.this, "是否重启使更改配置生效", "提示信息",JOptionPane.YES_NO_OPTION);
				if(ifadd == JOptionPane.YES_OPTION){
					
					CallingCardInfoWindow.this.dispose();
					application.dispose();
				}
			}
		});
		getContentPane().add(button);
		init();
		
	}
	
	private String getInitContent() {
		try {
			Map<String,String> mapper = new HashMap<String,String>();
			//尺寸
			mapper.put("size", size.getText()+"");
			//编码格式
			mapper.put("coding", coding.getText()+"");
			//二维码边框宽度
			mapper.put("dataMargin", dataMargin.getText()+"");
			//二维码logo位置
			mapper.put("logoPath", logoPath.getText()+"");
			//二维码logo边距
			mapper.put("logoMargin", logoMargin.getText()+"");
			//二维码logo尺寸
			mapper.put("logoSize", logoBelongSize.getText()+"");
			//二维码logo占二维码的比例
			mapper.put("ratio", logoBelongRatio.getText()+"");
			
			mapper.put("name", name.getText()+"");
			mapper.put("position", position.getText()+"");
			mapper.put("unit", unit.getText()+"");
			mapper.put("email", email.getText()+"");
			mapper.put("phone", phone.getText()+"");
			mapper.put("cellphone", cellphone.getText()+"");
			mapper.put("addres", addres.getText()+"");
			return new FtlFactory().build("init.ftl").process(mapper);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void init() {
		this.setSize(580, 410);
		this.setLocationRelativeTo(null);
	}
}
