package com.hiwan.app;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileSystemView;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;
import org.jb2011.lnf.beautyeye.ch3_button.BEButtonUI;

import com.hiwan.dialog.AboutWindow;
import com.hiwan.dialog.CallingCardInfoWindow;
import com.hiwan.su.ImagePanel;
import com.hiwan.util.Configuration;
import com.hiwan.util.DataUtil.FileUtil;
import com.hiwan.util.DataUtil.FtlFactory;
import com.hiwan.util.DataUtil.QrCodeUtil;
import com.hiwan.util.R;

@SuppressWarnings("serial")
public class MainApplication extends JFrame {

	private JTextField name;
	private JTextField position;
	private JTextField unit;
	private JTextField email;
	private JTextField phone;
	private JTextField cellphone;
	private JTextArea addres;
	ImagePanel right = new ImagePanel();
	JCheckBox showLogo = new JCheckBox("是");
	private JTextField size;

	public MainApplication() throws HeadlessException {
		
		try {
//			org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
//			BeautyEyeLNFHelper.translucencyAtFrameInactive=false;
			
			BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.osLookAndFeelDecorated;
			org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
		} catch (Exception e3) {
			e3.printStackTrace();
		}
		
		this.setTitle("二维码生成器"+Configuration.get(R.System.SYSTEM_VERSION));
		this.setSize(560, 400);
		this.setLocationRelativeTo(null);
		this.setJMenuBar(createMenuBar());

		getContentPane().setLayout(new BorderLayout());

		JPanel left = new JPanel();
		left.setPreferredSize(new Dimension(250, 350));
		getContentPane().add(left, BorderLayout.WEST);
		left.setLayout(null);

		{
			JLabel label = new JLabel("姓名:");
			label.setHorizontalAlignment(SwingConstants.RIGHT);
			label.setBounds(10, 20, 54, 15);
			left.add(label);

			name = new JTextField();
			name.setBounds(67, 17, 170, 25);
			name.setText(Configuration.get(R.QrCode.name));
			left.add(name);
			name.setColumns(10);

			JLabel label_1 = new JLabel("职位:");
			label_1.setHorizontalAlignment(SwingConstants.RIGHT);
			label_1.setBounds(10, 57, 54, 15);
			left.add(label_1);

			JLabel label_2 = new JLabel("单位/组织:");
			label_2.setHorizontalAlignment(SwingConstants.RIGHT);
			label_2.setBounds(-1, 135, 64, 15);
			left.add(label_2);

			JLabel label_3 = new JLabel("工作电话:");
			label_3.setHorizontalAlignment(SwingConstants.RIGHT);
			label_3.setBounds(10, 215, 54, 15);
			left.add(label_3);

			JLabel label_5 = new JLabel("移动电话:");
			label_5.setHorizontalAlignment(SwingConstants.RIGHT);
			label_5.setBounds(10, 172, 54, 15);
			left.add(label_5);

			JLabel label_6 = new JLabel("电子邮箱:");
			label_6.setBounds(10, 95, 54, 15);
			left.add(label_6);

			position = new JTextField();
			position.setColumns(10);
			position.setText(Configuration.get(R.QrCode.position));
			position.setBounds(67, 55, 170, 25);
			left.add(position);

			unit = new JTextField();
			unit.setColumns(10);
			unit.setText(Configuration.get(R.QrCode.unit));
			unit.setBounds(66, 132, 170, 25);
			left.add(unit);

			email = new JTextField();
			email.setColumns(10);
			email.setBounds(67, 92, 170, 25);
			email.setText(Configuration.get(R.QrCode.email));
			left.add(email);

			phone = new JTextField();
			phone.setBounds(67, 212, 170, 25);
			phone.setText(Configuration.get(R.QrCode.phone));
			left.add(phone);
			phone.setColumns(10);

			cellphone = new JTextField();
			cellphone.setColumns(10);
			cellphone.setText(Configuration.get(R.QrCode.cellphone));
			cellphone.setBounds(67, 171, 170, 25);
			left.add(cellphone);

			JLabel label_7 = new JLabel("工作地址:");
			label_7.setHorizontalAlignment(SwingConstants.RIGHT);
			label_7.setBounds(10, 254, 54, 15);
			left.add(label_7);

			addres = new JTextArea();
			addres.setBounds(63, 249, 175, 44);
			addres.setLineWrap(true);
			final JScrollPane scroll = new JScrollPane(addres); 
			addres.setText(Configuration.get(R.QrCode.addres));
			
			scroll.setBounds(63, 249, 175, 44);
			scroll.setHorizontalScrollBarPolicy( 
			JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED); 
			scroll.setVerticalScrollBarPolicy( 
			JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			
			left.add(scroll);

			JButton button = new JButton("保存本地");
			button.setUI(new BEButtonUI().setNormalColor(BEButtonUI.NormalColor.green));
			button.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					JFileChooser chooser = new JFileChooser();
					chooser.setSelectedFile(new File(System.currentTimeMillis()+".png"));
					chooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
					chooser.showOpenDialog(null);
					File selectedFile = chooser.getSelectedFile();
					if(null != selectedFile){
						try {
							String path = selectedFile.getPath();
							
							FileInputStream inputStream = null;
							FileOutputStream outputStream = null;
							FileChannel inChannel = null;
							FileChannel outChannel = null;
							try {
								inputStream = new FileInputStream(Configuration.getInstance().filePath);
								outputStream = new FileOutputStream(path);
								
								inChannel = inputStream.getChannel();
								outChannel = outputStream.getChannel();
								
								inChannel.transferTo(0, inChannel.size(), outChannel);
								
//								JOptionPane.showMessageDialog(null, "保存成功");
							} finally{
								inputStream.close();
								inChannel.close();
								outputStream.close();
								outChannel.close();
							}
						} catch (Exception e1) {
							e1.printStackTrace();
						}
					}
				}
			});
			
			button.setBounds(67, 303, 80, 23);
			left.add(button);

			JButton button_1 = new JButton("刷新预览");
			button_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					refresh();
				}

			});
			button_1.setBounds(157, 303, 80, 23);
			left.add(button_1);
		}

		JPanel rightPanel = new JPanel();
		rightPanel.setPreferredSize(new Dimension(270, 280));
		{
			getContentPane().add(rightPanel, BorderLayout.CENTER);
			rightPanel.setLayout(null);
			
			JLabel label = new JLabel("规格:");
			label.setBounds(21, 16, 54, 15);
			rightPanel.add(label);
			
			size = new JTextField();
			size.setBounds(54, 13, 81, 25);
			size.setText(Configuration.get(R.QrCode.SIZE));
			rightPanel.add(size);
			size.setColumns(10);
			
			JLabel lblLogo = new JLabel("显示LOGO:");
			lblLogo.setBounds(176, 20, 87, 15);
			rightPanel.add(lblLogo);
			
			showLogo.setBounds(243, 16, 47, 23);
			showLogo.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					refresh();
				}
			});
			showLogo.setSelected(true);
			rightPanel.add(showLogo);
			
			JPanel panel = new JPanel();
			panel.setBounds(10, 50, 280, 280);
			panel.setLayout(new CardLayout(0, 0));
			panel.add(right);
			rightPanel.add(panel);
		}
		

		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		try {
			this.setIconImage(new ImageIcon("img/logo1.png").getImage());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		this.addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				refresh();
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	private void refresh() {
		try {
			Configuration.getInstance().filePath = "tmp/" + UUID.randomUUID() + ".png";
			
			FileOutputStream fileOutputStream = new FileOutputStream(Configuration.getInstance().filePath);
			QrCodeUtil qrCodeUtil = new QrCodeUtil();
			Pattern p = Pattern.compile("^\\d*$");
			Matcher m = p.matcher(size.getText());
			if(m.matches()){
				qrCodeUtil.setSize(Integer.parseInt(size.getText()));
				right.setDrawWidth(Integer.parseInt(size.getText()));
			}
			if(null != showLogo.getSelectedObjects()){
				FileInputStream inputStream = new FileInputStream(Configuration.get(R.QrCode.LOGO_PATH));
				BufferedImage logo = ImageIO.read(inputStream);
				qrCodeUtil.encode(getQrCodeContent(), logo, fileOutputStream, Color.white);
			}else{
				qrCodeUtil.encode(getQrCodeContent(), fileOutputStream);
			}
			fileOutputStream.flush();
			fileOutputStream.close();
			
			right.validate();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}

	private String getQrCodeContent() {
		try {
			Map<String,String> mapper = new HashMap<String,String>();
			mapper.put("name", name.getText()+"");
			mapper.put("position", position.getText()+"");
			mapper.put("unit", unit.getText()+"");
			mapper.put("email", email.getText()+"");
			mapper.put("phone", phone.getText()+"");
			mapper.put("cellphone", cellphone.getText()+"");
			mapper.put("addres", addres.getText()+"");
			return new FtlFactory().build("card.ftl").process(mapper);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	private JMenuBar createMenuBar() {
		JMenuBar jMenuBar = new JMenuBar();

		{
			JMenu menu = new JMenu("文件");
			menu.setEnabled(false);
			jMenuBar.add(menu);
		}
		
		{
			JMenu menu = new JMenu("编辑");
			menu.setEnabled(false);
			jMenuBar.add(menu);
		}
		
		{
			JMenu menu = new JMenu("选项");
			
			{
				JMenuItem idCardInfo = new JMenuItem("高级");
//				menu.add(idCardInfo);
			}
			
			{
				JMenuItem paramsSetting = new JMenuItem("参数设置");
				paramsSetting.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						CallingCardInfoWindow infoWindow = new CallingCardInfoWindow(MainApplication.this);
						infoWindow.show();
					}
				});
				menu.add(paramsSetting);
			}
			
			jMenuBar.add(menu);
		}
		
		{//帮助
			JMenu menu = new JMenu("帮助");
			{
				JMenuItem explan = new JMenuItem("使用说明");
				explan.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						JFileChooser chooser = new JFileChooser();
						chooser.setSelectedFile(new File("config/电子二维码使用说明.doc"));
						chooser.setCurrentDirectory(FileSystemView.getFileSystemView().getHomeDirectory());
						chooser.showOpenDialog(null);
						File selectedFile = chooser.getSelectedFile();
						if(null != selectedFile){
							String path = selectedFile.getPath();
							try {
								byte[] bs = FileUtil.getFile("config/电子二维码使用说明.doc");
								FileUtil.toFile(bs, path);
							} catch (Exception e1) {
								e1.printStackTrace();
							}
							
//							try {
//								String path = selectedFile.getPath();
//								int length=2097152;
//								
//								FileInputStream inputStream = null;
//								FileOutputStream outputStream = null;
//								FileChannel inChannel = null;
//								FileChannel outChannel = null;
//								ByteBuffer buffer=null;
//								try {
//									inputStream = new FileInputStream(Configuration.getInstance().filePath);
//									outputStream = new FileOutputStream(path,true);
//									
//									inChannel = inputStream.getChannel();
//									outChannel = outputStream.getChannel();
//									while(true){
//										if(inChannel.position() == inChannel.size()){
//											inChannel.close();
//											outChannel.close();
//										}
//										
//										if((inChannel.size() - inChannel.position()) < length){
//											length = (int) (inChannel.size() - inChannel.position());
//										}else{
//											length=2097152;
//										}
//										buffer = ByteBuffer.allocateDirect(length);
//										inChannel.read(buffer);
//										buffer.flip();
//										outChannel.write(buffer);
//										outChannel.force(false);
//									}
//								} finally{
//									outputStream.close();
//									inputStream.close();
//								}
//							} catch (Exception e1) {
//								e1.printStackTrace();
//							}
						}
					}
				});
				menu.add(explan);
			}
			{//关于
				JMenuItem about = new JMenuItem("关于本软件");
				about.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						new AboutWindow(MainApplication.this).show();
					}
				});
				menu.add(about);
			}
			jMenuBar.add(menu);
		}

		return jMenuBar;
	}

	public static void main(String[] args) {
		File file = new File("tmp");
		for (File fileCache: file.listFiles()) {
			if(!"welcome.png".equals(fileCache.getName())){
				fileCache.delete();
			}
		}
		new MainApplication();
	}
}
