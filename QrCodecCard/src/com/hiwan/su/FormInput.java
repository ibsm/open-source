package com.hiwan.su;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FormInput extends FormModule {

	private JLabel leftLabel;
	private JTextField rightTextField;
	
	public void add(JPanel panel,String label,String name){
		leftLabel = new JLabel(label);
		panel.add(leftLabel);
		
		rightTextField = new JTextField();
		
		panel.add(rightTextField);
		
		this.setName(name);
	}
	
}
