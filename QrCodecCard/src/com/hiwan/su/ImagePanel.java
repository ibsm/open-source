package com.hiwan.su;

import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import com.hiwan.util.Configuration;
import com.hiwan.util.R;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 上午2:11:34 2016年2月24日
 * @statement 
 * @team 
 * @email shishun156@gmail.com
 * @describe 
 */
@SuppressWarnings("serial")
public class ImagePanel extends JPanel {
	
	private int drawWidth;
	
	public int getDrawWidth() {
		return drawWidth;
	}

	public void setDrawWidth(int drawWidth) {
		int size = Configuration.getInt(R.QrCode.DRAW_BOARD_SIZE);
		if(drawWidth == size){
			this.drawWidth = 0;
		}else{
			this.drawWidth = (size - drawWidth)/2;
		}
	}

	public ImagePanel() {
		super();
		this.setLayout(null);
	}

	@Override
	protected void paintComponent(Graphics g) {
		try {
			super.paintComponent(g);
			this.repaint();
			ImageIcon icon = new ImageIcon(Configuration.getInstance().filePath);
			g.drawImage(icon.getImage(), getDrawWidth(), getDrawWidth(), icon.getIconWidth(), icon.getIconHeight(), icon.getImageObserver());
			g.dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
