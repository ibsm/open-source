/*系统用户表*/
INSERT INTO `cmdb_sys_user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', '初始化数据，系统超级管理员', '1', '1', '2015-12-11 10:01:19');
/*初始化菜单*/
INSERT INTO `cmdb_sys_menu` VALUES ('1', '系统配置', '', '', '', '0', '1', '1', '1', '2015-12-11 10:01:51');
INSERT INTO `cmdb_sys_menu` VALUES ('10', '部署流程', '<ol class=\"breadcrumb\"><li><a href=\"#\">系统配置</a></li><li><a href=\"javascript:alert(\'工作流\')\">工作流</a></li><li class=\"active\">部署流程定义</li></ol>', '#!wflow/deploy/index.do', null, '9', '1', '1', '1', '2016-01-05 17:08:26');
INSERT INTO `cmdb_sys_menu` VALUES ('11', '监控管理', null, null, null, '0', '2', '1', '1', '2016-01-09 19:21:35');
INSERT INTO `cmdb_sys_menu` VALUES ('12', '硬件设备', null, null, null, '11', '1', '1', '1', '2016-01-09 19:22:16');
INSERT INTO `cmdb_sys_menu` VALUES ('13', '服务器资源', '<ol class=\"breadcrumb\"><li><a href=\"#\">监控管理</a></li><li><a href=\"javascript:alert(\'硬件设备\')\">硬件设备</a></li><li class=\"active\">服务器资源</li></ol>', '#!monitor/phyDevice/index.do', null, '12', '1', '1', '1', '2016-01-09 19:22:49');
INSERT INTO `cmdb_sys_menu` VALUES ('14', 'JVM资源', null, null, null, '12', '2', '1', '1', '2016-01-09 19:23:35');
INSERT INTO `cmdb_sys_menu` VALUES ('15', '硬盘', '<ol class=\"breadcrumb\"><li><a href=\"#\">监控管理</a></li><li><a href=\"javascript:alert(\'硬件设备\')\">硬件设备</a></li><li><a href=\"#!monitor/phyDevice/index.do\">服务器资源</a></li><li class=\"active\">磁盘</li></ol>', '#!monitor/phyDevice/diskInfo.do', null, '13', '1', '1', '1', '2016-01-05 17:08:02');
INSERT INTO `cmdb_sys_menu` VALUES ('16', 'CPU', '<ol class=\"breadcrumb\"><li><a href=\"#\">监控管理</a></li><li><a href=\"javascript:alert(\'硬件设备\')\">硬件设备</a></li><li><a href=\"#!monitor/phyDevice/index.do\">服务器资源</a></li><li class=\"active\">CPU</li></ol>', '#!monitor/phyDevice/cpuInfo.do', null, '13', '2', '1', '1', '2016-01-05 17:08:02');
INSERT INTO `cmdb_sys_menu` VALUES ('17', '内存', '<ol class=\"breadcrumb\"><li><a href=\"#\">监控管理</a></li><li><a href=\"javascript:alert(\'硬件设备\')\">硬件设备</a></li><li><a href=\"#!monitor/phyDevice/index.do\">服务器资源</a></li><li class=\"active\">内存</li></ol>', '#!monitor/phyDevice/memoryInfo.do', null, '13', '3', '1', '1', '2016-01-05 17:08:02');
INSERT INTO `cmdb_sys_menu` VALUES ('18', '网络', '<ol class=\"breadcrumb\"><li><a href=\"#\">监控管理</a></li><li><a href=\"javascript:alert(\'硬件设备\')\">硬件设备</a></li><li><a href=\"#!monitor/phyDevice/index.do\">服务器资源</a></li><li class=\"active\">网络</li></ol>', '#!monitor/phyDevice/networkInfo.do', null, '13', '4', '1', '1', '2016-01-05 17:08:02');
INSERT INTO `cmdb_sys_menu` VALUES ('19', '节点定义', '<ol class=\"breadcrumb\"><li><a href=\"#\">系统配置</a></li><li><a href=\"javascript:alert(\'工作流\')\">工作流</a></li><li class=\"active\">流程节点定义</li></ol>', '#!wflow/actCusNode/index.do', null, '9', '2', '1', '1', '2016-01-05 17:08:02');
INSERT INTO `cmdb_sys_menu` VALUES ('2', '权限架构', null, null, null, '1', '1', '1', '1', '2015-12-11 10:02:18');
INSERT INTO `cmdb_sys_menu` VALUES ('3', '用户管理', '<ol class=\"breadcrumb\"><li><a href=\"#\">系统配置</a></li><li><a href=\"#!sys/role/jump.do?jType=1\">权限架构</a></li><li class=\"active\">用户管理</li></ol>', '#!sys/user/index.do', null, '2', '1', '1', '1', '2015-12-11 10:02:37');
INSERT INTO `cmdb_sys_menu` VALUES ('4', '系统角色', '<ol class=\"breadcrumb\"><li><a href=\"#\">系统配置</a></li><li><a href=\"#!sys/user/jump.do?jType=1\">权限架构</a></li><li class=\"active\">角色管理</li></ol>', '#!sys/role/index.do', null, '2', '2', '1', '1', '2015-12-11 10:02:57');
INSERT INTO `cmdb_sys_menu` VALUES ('5', '系统菜单', '<ol class=\"breadcrumb\"><li><a href=\"#\">系统配置</a></li><li><a href=\"javascript:alert(\'权限架构\')\">权限架构</a></li><li class=\"active\">系统菜单</li></ol>', '#!sys/menu/index.do', null, '2', '3', '1', '1', '2015-12-11 10:03:15');
INSERT INTO `cmdb_sys_menu` VALUES ('6', '字典类型', '<ol class=\"breadcrumb\"><li><a href=\"#\">系统配置</a></li><li><a href=\"javascript:alert(\'权限架构\')\">权限架构</a></li><li class=\"active\">字典类型</li></ol>', '#!sys/dictType/index.do', null, '2', '4', '1', '1', '2015-12-13 13:29:47');
INSERT INTO `cmdb_sys_menu` VALUES ('7', '字典实例', '<ol class=\"breadcrumb\"><li><a href=\"#\">系统配置</a></li><li><a href=\"javascript:alert(\'权限架构\')\">权限架构</a></li><li class=\"active\">数据字典实例</li></ol>', '#!sys/dictIns/index.do', null, '2', '5', '1', '1', '2015-12-13 13:51:54');
INSERT INTO `cmdb_sys_menu` VALUES ('8', '权限管理', '<ol class=\"breadcrumb\"><li><a href=\"#\">系统配置</a></li><li><a href=\"javascript:alert(\'权限架构\')\">权限架构</a></li><li class=\"active\">权限管理</li></ol>', '#!sys/per/index.do', null, '2', '6', '1', '1', '2016-01-05 02:12:37');
INSERT INTO `cmdb_sys_menu` VALUES ('9', '工作流', null, null, null, '1', '2', '1', '1', '2016-01-05 17:08:02');

/*数据功能权限*/
INSERT INTO `cmdb_sys_per` VALUES ('1', 'd5054faabe5f4a9196b1dbe13800fd64', '添加', 'user:add', '用户管理添加权限', '1', '2015-12-30 10:30:04', '1');
INSERT INTO `cmdb_sys_per` VALUES ('2', 'd5054faabe5f4a9196b1dbe13800fd64', '修改', 'user:update', '用户管理修改权限', '1', '2015-12-30 14:27:00', '1');
INSERT INTO `cmdb_sys_per` VALUES ('3', 'd5054faabe5f4a9196b1dbe13800fd64', '删除', 'user:delete', '用户管理删除权限', '1', '2015-12-30 15:02:39', '1');
INSERT INTO `cmdb_sys_per` VALUES ('4', 'd5054faabe5f4a9196b1dbe13800fd64', '批量删除', 'user:delBatch', '用户管理批量删除权限', '1', '2015-12-30 15:03:14', '1');
INSERT INTO `cmdb_sys_per` VALUES ('5', 'd5054faabe5f4a9196b1dbe13800fd64', '详情', 'user:detail', '用户管理展示详情权限', '1', '2015-12-30 15:28:44', '1');

/*系统角色*/
INSERT INTO `cmdb_sys_role` VALUES ('1', 'student', '测试角色', '1', '2015-12-30 10:29:33', '1');
INSERT INTO `cmdb_sys_role` VALUES ('2', 'teacher', 'ces', '1', '2015-12-30 14:26:17', '1');

/*角色数据权限*/
INSERT INTO `cmdb_sys_role_per` VALUES ('1', '1', '1');
INSERT INTO `cmdb_sys_role_per` VALUES ('2', '1', '2');
INSERT INTO `cmdb_sys_role_per` VALUES ('3', '1', '3');
INSERT INTO `cmdb_sys_role_per` VALUES ('4', '1', '4');
INSERT INTO `cmdb_sys_role_per` VALUES ('5', '1', '5');

/*用户角色数据权限*/
INSERT INTO `cmdb_sys_user_role` VALUES ('1', '1', '1');

/*数据字典类型*/
INSERT INTO `cmdb_sys_dict` VALUES ('1', '数据权限', '系统数据权限配置', '1', '2016-01-05 10:55:03', '1');
INSERT INTO `cmdb_sys_dict` VALUES ('2', '流程类型', '工作流分组', '1', '2016-01-18 08:54:26', '1');

/*数据字典实例*/
INSERT INTO `cmdb_sys_dictins` VALUES ('b62155816f904ad7a5648b262ab43cd4', '系统菜单', '系统菜单权限', '1', '1', '3', '1', '2016-01-05 11:00:12', '1');
INSERT INTO `cmdb_sys_dictins` VALUES ('d5054faabe5f4a9196b1dbe13800fd64', '用户管理', '用户管理权限', '1', '1', '1', '1', '2016-01-05 10:57:56', '1');
INSERT INTO `cmdb_sys_dictins` VALUES ('f5408e808cc043bbb62bf9d4d58a02e0', '系统角色', '系统角色管理', '1', '1', '2', '1', '2016-01-05 10:59:43', '1');
