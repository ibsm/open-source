<div id="${chartId}" class="${css}" style="${style}"></div>
<script type="text/javascript">

function requireCallback_${chartId}(ec) {
	var option = {
	    title : {
	        text: '${title01}',
            subtext: '${title02}',
	        x:'center'
	    },
	    tooltip : {
	        trigger: 'item',
	        formatter: "{a} <br/>{b} : {c}${unit} ({d}%)"
	    },
	    legend: {
	        orient : 'vertical',
	        x : 'left',
	        data:${dataColumns}
	    },
	    toolbox: {
	        show : true,
	        feature : {
	            mark : {show: false},
	            dataView : {show: false, readOnly: false},
	            magicType : {
	                show: false, 
	                type: ['pie', 'funnel'],
	                option: {
	                    funnel: {
	                        x: '25%',
	                        width: '50%',
	                        funnelAlign: 'left',
	                        max: 1548
	                    }
	                }
	            },
	            restore : {show: true},
	            saveAsImage : {show: false}
	        }
	    },
	    calculable : true,
	    series : [
	        {
	            name:'${dataFrom}',
	            type:'pie',
	            radius : '55%',
	            center: ['50%', '60%'],
	            data:${chartDatas}
	        }
	    ]
	};

	var domMain = document.getElementById('${chartId}');
    ec.init(domMain, 'macarons').setOption(option, true);
}

require.config({
	paths: {
		echarts: './resource/ui/plugins/charts/com/www/js'
	}
});

launch_${chartId}();

function launch_${chartId}() {
    require(
        [
            'echarts',
            'resource/ui/plugins/charts/com/theme/macarons',
            'echarts/chart/line',
            'echarts/chart/bar',
            'echarts/chart/scatter',
            'echarts/chart/k',
            'echarts/chart/pie',
            'echarts/chart/radar',
            'echarts/chart/force',
            'echarts/chart/chord',
            'echarts/chart/gauge',
            'echarts/chart/funnel',
            'echarts/chart/eventRiver',
            'echarts/chart/venn',
            'echarts/chart/treemap',
            'echarts/chart/tree',
            'echarts/chart/wordCloud',
            'echarts/chart/heatmap'
        ],
        requireCallback_${chartId}
    );
}
</script>