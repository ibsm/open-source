package com.hm.monitor.phy.res;

import java.util.List;

import org.hyperic.sigar.NetInfo;
import org.hyperic.sigar.NetInterfaceConfig;
import org.hyperic.sigar.NetInterfaceStat;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 网络
 */
public class Network {

	private String domainName;

	private String localIp;

	private String localMac;
	
	private NetInfo netInfo;

	private List<NetworkFlowInfo> networkFlowInfos;

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public String getLocalIp() {
		return localIp;
	}

	public void setLocalIp(String localIp) {
		this.localIp = localIp;
	}

	public String getLocalMac() {
		return localMac;
	}

	public void setLocalMac(String localMac) {
		this.localMac = localMac;
	}

	public List<NetworkFlowInfo> getNetworkFlowInfos() {
		return networkFlowInfos;
	}

	public void setNetworkFlowInfos(List<NetworkFlowInfo> networkFlowInfos) {
		this.networkFlowInfos = networkFlowInfos;
	}

	public static class NetworkFlowInfo {

		private NetInterfaceConfig netInterfaceConfig;

		private NetInterfaceStat netInterfaceStat;

		public NetInterfaceConfig getNetInterfaceConfig() {
			return netInterfaceConfig;
		}

		public void setNetInterfaceConfig(NetInterfaceConfig netInterfaceConfig) {
			this.netInterfaceConfig = netInterfaceConfig;
		}

		public NetInterfaceStat getNetInterfaceStat() {
			return netInterfaceStat;
		}

		public void setNetInterfaceStat(NetInterfaceStat netInterfaceStat) {
			this.netInterfaceStat = netInterfaceStat;
		}

		@Override
		public String toString() {
			return "NetworkFlowInfo [netInterfaceConfig=" + netInterfaceConfig
					+ ", netInterfaceStat=" + netInterfaceStat + "]";
		}

	}
	
	public NetInfo getNetInfo() {
		return netInfo;
	}

	public void setNetInfo(NetInfo netInfo) {
		this.netInfo = netInfo;
	}

	@Override
	public String toString() {
		return "Network [domainName=" + domainName + ", localIp=" + localIp
				+ ", localMac=" + localMac + ", netInfo=" + netInfo
				+ ", networkFlowInfos=" + networkFlowInfos + "]";
	}

}
