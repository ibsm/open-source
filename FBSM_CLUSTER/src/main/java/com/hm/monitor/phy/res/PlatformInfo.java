package com.hm.monitor.phy.res;

import java.util.List;

import org.hyperic.sigar.OperatingSystem;
import org.hyperic.sigar.Who;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 当前操作平台信息
 */
public class PlatformInfo {

	/**
	 * 取到当前操作系统的名称
	 */
	private String osName;
	
	/**
	 * 取当前操作系统的信息
	 */
	private OperatingSystem operatingSystem;
	
	/**
	 * 取当前系统进程表中的用户信息
	 */
	private List<Who> whos;

	public String getOsName() {
		return osName;
	}

	public void setOsName(String osName) {
		this.osName = osName;
	}

	public OperatingSystem getOperatingSystem() {
		return operatingSystem;
	}

	public void setOperatingSystem(OperatingSystem operatingSystem) {
		this.operatingSystem = operatingSystem;
	}

	public List<Who> getWhos() {
		return whos;
	}

	public void setWhos(List<Who> whos) {
		this.whos = whos;
	}

	@Override
	public String toString() {
		return "PlatformInfo [osName=" + osName + ", operatingSystem="
				+ operatingSystem + ", whos=" + whos + "]";
	}
	
	
}
