package com.hm.monitor.phy.res;

import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Swap;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 内存
 */
public class Memory {

	/**
	 * 物理内存信息
	 */
	private Mem physicalMemory;
	
	/**
	 * 交换区内存
	 */
	private Swap warpMemory;

	public Mem getPhysicalMemory() {
		return physicalMemory;
	}

	public void setPhysicalMemory(Mem physicalMemory) {
		this.physicalMemory = physicalMemory;
	}

	public Swap getWarpMemory() {
		return warpMemory;
	}

	public void setWarpMemory(Swap warpMemory) {
		this.warpMemory = warpMemory;
	}

	@Override
	public String toString() {
		return "Memory [physicalMemory=" + physicalMemory + ", warpMemory="
				+ warpMemory + "]";
	}
	
}
