package com.hm.monitor.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.FileSystemUsage;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Who;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.PageSet;
import com.hm.common.util.DataUtil;
import com.hm.monitor.entity.FtlChart;
import com.hm.monitor.phy.res.Cpu;
import com.hm.monitor.phy.res.Disk;
import com.hm.monitor.phy.res.Disk.DiskInfo;
import com.hm.monitor.phy.res.Memory;
import com.hm.monitor.phy.res.PlatformInfo;
import com.hm.monitor.phy.su.PhyDeviceMonitor;

@Service
public class PhyDeviceMonitorService extends BaseService {

	private PhyDeviceMonitor phyDeviceMonitor = new PhyDeviceMonitor();
	
	public Map<String,String> getCpu() throws Exception{
		Cpu cpu = phyDeviceMonitor.getCpu(false);
		FtlChart ftlChart = new FtlChart("CPU使用情况", null,"%");
		{
			List<CpuPerc> cpuPercs = cpu.getCpuPercs();
			double idle = 0,total = 0;
			for (CpuPerc cpuPerc : cpuPercs) {
				idle += cpuPerc.getIdle();
				total += cpuPerc.getCombined();
			}
			
			DecimalFormat format = new DecimalFormat("0.00");//格式化小数
			{
				idle = (float)(idle*10/cpuPercs.size());
				total = (float)(idle*10/cpuPercs.size());
			}
			// 图表描述
			ftlChart.set("已使用", format.format((idle/(total+idle))*100));
			ftlChart.set("未使用", format.format((total/(total+idle))*100));
		}
		return ftlChart.get();
	}
	
	public Map<String,String> getMemory() throws Exception{
		Memory memory = phyDeviceMonitor.getMemory();
		FtlChart ftlChart = new FtlChart("物理内存使用情况", null,"GB");
		{
			Mem physicalMemory = memory.getPhysicalMemory();
			float used = 0,free = 0;
			used = physicalMemory.getUsed();
			free = physicalMemory.getFree();
			
			DecimalFormat format = new DecimalFormat("0.00");//格式化小数
			{
				used = (float)(used/1024);//K -> KB
				used = (float)(used/1024);//KB -> MB
				used = (float)(used/1024);//MB ->GB
				
				free = (float)(free/1024);//K -> KB
				free = (float)(free/1024);//KB -> MB
				free = (float)(free/1024);//MB ->GB
			}
			// 图表描述
			ftlChart.set("已用内存", format.format(used));
			ftlChart.set("剩余内存", format.format(free));
		}
		
		return ftlChart.get();
	}
	
	public Map<String,String> getDiskInfo() throws Exception{
		Disk disk = phyDeviceMonitor.getDisk();
		FtlChart ftlChart = new FtlChart("物理磁盘使用情况", null,"GB");
		{
			List<DiskInfo> diskInfos = disk.getDiskInfos();
			float used = 0,free = 0;
			for (DiskInfo diskInfo : diskInfos) {
				FileSystemUsage fileSystemUsage = diskInfo.getFileSystemUsage();
				if(null != fileSystemUsage){
					used += fileSystemUsage.getUsed();
					free += fileSystemUsage.getFree();
				}
			}
			DecimalFormat format = new DecimalFormat("0.00");//格式化小数
			{
				used = (float)(used/1024);//KB -> MB
				used = (float)(used/1024);//MB ->GB
				
				free = (float)(free/1024);//KB -> MB
				free = (float)(free/1024);//MB ->GB
			}
			// 图表描述
			ftlChart.set("已用磁盘", format.format(used));
			ftlChart.set("剩余磁盘", format.format(free));
		}
		
		return ftlChart.get();
	}

	public PageSet queryPlatformWhoList(PageSet pageSet) throws Exception {
		PlatformInfo platformInfo = phyDeviceMonitor.getPlatformInfo();
		List<Who> whos = platformInfo.getWhos();
		List<JSONObject> items = new ArrayList<JSONObject>();
		for (Who who : whos) {
			JSONObject item = new JSONObject();
			item.put("device", who.getDevice());
			item.put("host", who.getHost());
			item.put("time", 0 != who.getTime()?DataUtil.DateUtil.yyyyMMddhhmm(new Date(who.getTime())):0);
			item.put("user", who.getUser());
			
			items.add(item);
		}
		pageSet.setRows(items);
		pageSet.setTotal(items.size());
		
		return pageSet;
	}
}
