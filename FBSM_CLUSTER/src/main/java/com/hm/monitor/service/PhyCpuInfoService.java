package com.hm.monitor.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.hyperic.sigar.CpuPerc;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.PageSet;
import com.hm.common.config.Configuration;
import com.hm.common.util.DataUtil;
import com.hm.common.util.R;
import com.hm.monitor.entity.FtlChart;
import com.hm.monitor.phy.res.Cpu;
import com.hm.monitor.phy.res.Cpu.CpuUsageRateFormat;
import com.hm.monitor.phy.su.PhyDeviceMonitor;

@Service
public class PhyCpuInfoService extends BaseService {

	private PhyDeviceMonitor phyDeviceMonitor = new PhyDeviceMonitor();
	
	public List<String> getCpuChartInfos() throws Exception{
		List<String> list = new ArrayList<String>();
		
		Cpu cpu = phyDeviceMonitor.getCpu(false);
		List<CpuPerc> cpuPercs = cpu.getCpuPercs();
		int index = 0;
		DecimalFormat format = new DecimalFormat("0.00");//格式化小数
		for (CpuPerc cpuPerc : cpuPercs) {
			FtlChart ftlChart = new FtlChart("CPU"+ ++index +"使用情况", null,"%");
			ftlChart.set("使用率", format.format(cpuPerc.getCombined()*10));
			
			list.add(new DataUtil.FtlFactory().build(Configuration.get(R.Charts.GAUGE1)).process(ftlChart.get()));
		}
		
		return list;
	}
	
	public PageSet queryCpuInfoList(PageSet pageSet) throws Exception{
		
		Cpu cpu = phyDeviceMonitor.getCpu(true);
		List<JSONObject> items = new ArrayList<JSONObject>();
		List<CpuUsageRateFormat> cpuUsageRateFormats = cpu.getCpuUsageRateFormats();
		for (CpuUsageRateFormat cpuUsageRateFormat : cpuUsageRateFormats) {
			JSONObject item = new JSONObject();
			item.put("user", cpuUsageRateFormat.getUser());
			item.put("sys", cpuUsageRateFormat.getSys());
			item.put("wait", cpuUsageRateFormat.getWait());
			item.put("nice", cpuUsageRateFormat.getNice());
			item.put("idle", cpuUsageRateFormat.getIdle());
			item.put("combined", cpuUsageRateFormat.getCombined());

			items.add(item);
		}
		
		pageSet.setRows(items);
		pageSet.setTotal(items.size());
		return pageSet;
	}
}
