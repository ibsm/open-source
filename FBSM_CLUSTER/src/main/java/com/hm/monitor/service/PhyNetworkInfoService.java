package com.hm.monitor.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONObject;

import org.hyperic.sigar.NetInterfaceConfig;
import org.hyperic.sigar.NetInterfaceStat;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.util.DataUtil;
import com.hm.monitor.phy.res.Network;
import com.hm.monitor.phy.res.Network.NetworkFlowInfo;
import com.hm.monitor.phy.su.PhyDeviceMonitor;

@Service
public class PhyNetworkInfoService extends BaseService {

	private PhyDeviceMonitor phyDeviceMonitor = new PhyDeviceMonitor();
	
	public List<String> getNetworkChartInfos() throws Exception {
		
		return null;
	}

	public List<NetInterfaceConfig> getNetworkCardTypes() throws Exception{
		List<NetInterfaceConfig> list = new ArrayList<NetInterfaceConfig>();
		
		Network network = phyDeviceMonitor.getNetwork();
		List<NetworkFlowInfo> networkFlowInfos = network.getNetworkFlowInfos();
		for (NetworkFlowInfo networkFlowInfo : networkFlowInfos) {
			if(null != networkFlowInfo.getNetInterfaceStat()){
				list.add(networkFlowInfo.getNetInterfaceConfig());
			}
		}
		return list;
	}

	public NetInterfaceStat getNetInterfaceStat(String belongNetworkCard) throws Exception{
		Network network = phyDeviceMonitor.getNetwork();
		List<NetworkFlowInfo> networkFlowInfos = network.getNetworkFlowInfos();
		for (NetworkFlowInfo networkFlowInfo : networkFlowInfos) {
			if(null != networkFlowInfo.getNetInterfaceStat()){
				NetInterfaceConfig netInterfaceConfig = networkFlowInfo.getNetInterfaceConfig();
				if(netInterfaceConfig.getName().equals(belongNetworkCard)){
					return networkFlowInfo.getNetInterfaceStat();
				}
			}
		}
		return null;
	}

	public NetInterfaceConfig getNetInterfaceConfig(String belongNetworkCard) throws Exception{
		Network network = phyDeviceMonitor.getNetwork();
		List<NetworkFlowInfo> networkFlowInfos = network.getNetworkFlowInfos();
		for (NetworkFlowInfo networkFlowInfo : networkFlowInfos) {
			if(null != networkFlowInfo.getNetInterfaceStat()){
				NetInterfaceConfig netInterfaceConfig = networkFlowInfo.getNetInterfaceConfig();
				if(netInterfaceConfig.getName().equals(belongNetworkCard)){
					return netInterfaceConfig;
				}
			}
		}
		return null;
	}

	public JSONObject getNetworkChartData(String belongNetworkCard) throws Exception{
		JSONObject item = new JSONObject();
		Network network = phyDeviceMonitor.getNetwork();
		List<NetworkFlowInfo> networkFlowInfos = network.getNetworkFlowInfos();
		for (NetworkFlowInfo networkFlowInfo : networkFlowInfos) {
			if(null != networkFlowInfo.getNetInterfaceStat()){
				NetInterfaceConfig netInterfaceConfig = networkFlowInfo.getNetInterfaceConfig();
				if(netInterfaceConfig.getName().equals(belongNetworkCard)){
					NetInterfaceStat netInterfaceStat = networkFlowInfo.getNetInterfaceStat();
					
					item.put("send", DataUtil.Decimal.k2kb(netInterfaceStat.getRxBytes()/1000));
					item.put("accept", DataUtil.Decimal.k2kb(netInterfaceStat.getTxBytes()/1000));
					item.put("time", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:SSS").format(new Date()));
					
					break;
				}
			}
		}
		return item;
	}

	
}
