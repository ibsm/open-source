package com.hm.monitor.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.hyperic.sigar.FileSystem;
import org.hyperic.sigar.FileSystemUsage;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.PageSet;
import com.hm.common.config.Configuration;
import com.hm.common.util.DataUtil;
import com.hm.common.util.DataUtil.Decimal;
import com.hm.common.util.R;
import com.hm.monitor.entity.FtlChart;
import com.hm.monitor.phy.res.Disk;
import com.hm.monitor.phy.res.Disk.DiskInfo;
import com.hm.monitor.phy.su.PhyDeviceMonitor;

@Service
public class PhyDickInfoService extends BaseService {

	private PhyDeviceMonitor phyDeviceMonitor = new PhyDeviceMonitor();
	
	public List<String> getDickChartInfos() throws Exception{
		List<String> list = new ArrayList<String>();
		
		Disk disk = phyDeviceMonitor.getDisk();
		List<DiskInfo> diskInfos = disk.getDiskInfos();
		for (DiskInfo diskInfo : diskInfos) {
			String name = (diskInfo.getFileSystem().getDevName()+"").replaceAll("\\\\","");
			FtlChart ftlChart = new FtlChart(name, null,"GB");
			{
				float used = 0,free = 0;
				FileSystemUsage fileSystemUsage = diskInfo.getFileSystemUsage();
				if(null != fileSystemUsage){
					used = fileSystemUsage.getUsed();
					free = fileSystemUsage.getFree();
				}
				DecimalFormat format = new DecimalFormat("0.00");//格式化小数
				{
					used = (float)(used/1024);//KB -> MB
					used = (float)(used/1024);//MB ->GB
					
					free = (float)(free/1024);//KB -> MB
					free = (float)(free/1024);//MB ->GB
				}
				// 图表描述
				ftlChart.set("已用磁盘", format.format(used));
				ftlChart.set("剩余磁盘", format.format(free));
			}
			
			list.add(new DataUtil.FtlFactory().build(Configuration.get(R.Charts.PIE)).process(ftlChart.get()));
		}
		
		return list;
	}

	public PageSet queryDiskInfoList(PageSet pageSet) throws Exception{
		
		java.text.DecimalFormat format = new java.text.DecimalFormat("0.00");
		Disk disk = phyDeviceMonitor.getDisk();
		List<DiskInfo> diskInfos = disk.getDiskInfos();
		List<JSONObject> items = new ArrayList<JSONObject>();
		for (DiskInfo diskInfo : diskInfos) {
			JSONObject item = new JSONObject();
			FileSystem fileSystem = diskInfo.getFileSystem();
			item.put("devName", fileSystem.getDevName());
			item.put("sysTypeName", fileSystem.getSysTypeName());
			item.put("typeName", fileSystem.getTypeName());
			item.put("type", fileSystem.getType());
			
			FileSystemUsage fileSystemUsage = diskInfo.getFileSystemUsage();
			if(null != fileSystemUsage){
				item.put("total", Decimal.decimal2(Decimal.kb2gb(fileSystemUsage.getTotal())));
				item.put("free", Decimal.decimal2(Decimal.kb2gb(fileSystemUsage.getFree())));
				item.put("avail", Decimal.decimal2(Decimal.kb2gb(fileSystemUsage.getAvail())));
				item.put("used", Decimal.decimal2(Decimal.kb2gb(fileSystemUsage.getUsed())));
				double usePercent = fileSystemUsage.getUsePercent() * 100D;
				item.put("usePercent", format.format(usePercent));
				item.put("diskReads", Decimal.decimal2(Decimal.kb2gb(fileSystemUsage.getDiskReads())));
				item.put("diskWrites", Decimal.decimal2(Decimal.kb2gb(fileSystemUsage.getDiskWrites())));
			}
			
			items.add(item);
		}
		
		pageSet.setRows(items);
		pageSet.setTotal(items.size());
		return pageSet;
	}
}
