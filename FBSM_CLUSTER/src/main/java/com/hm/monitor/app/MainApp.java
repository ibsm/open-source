package com.hm.monitor.app;
import net.sf.json.JSONObject;

import com.hm.monitor.phy.su.PhyDeviceMonitor;

public class MainApp {

	public static void main(String[] args) throws Exception {
		PhyDeviceMonitor phyDeviceMonitor = new PhyDeviceMonitor();
		System.out.println(JSONObject.fromObject(phyDeviceMonitor.getCpu(true)));
		System.out.println(JSONObject.fromObject(phyDeviceMonitor.getDisk()));
		System.out.println(JSONObject.fromObject(phyDeviceMonitor.getMemory()));
		System.out.println(JSONObject.fromObject(phyDeviceMonitor.getPlatformInfo()));
		System.out.println(JSONObject.fromObject(phyDeviceMonitor.getNetwork()));
		
	}
}
