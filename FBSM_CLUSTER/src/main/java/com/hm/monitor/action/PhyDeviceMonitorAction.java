package com.hm.monitor.action;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.hyperic.sigar.CpuInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hm.common.BaseAction;
import com.hm.common.config.Configuration;
import com.hm.common.util.DataUtil;
import com.hm.common.util.DataUtil.FtlFactory;
import com.hm.common.util.R;
import com.hm.monitor.phy.res.Memory;
import com.hm.monitor.phy.su.PhyDeviceMonitor;
import com.hm.monitor.service.PhyCpuInfoService;
import com.hm.monitor.service.PhyDeviceMonitorService;
import com.hm.monitor.service.PhyDickInfoService;
import com.hm.monitor.service.PhyMemoryInfoService;
import com.hm.monitor.service.PhyNetworkInfoService;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 上午3:23:13 2016年3月11日
 * @statement 
 * @team 
 * @email shishun156@gmail.com
 * @describe 服务器物理设备监控
 */
@Controller
@RequestMapping("monitor/phyDevice")
public class PhyDeviceMonitorAction extends BaseAction{

	private PhyDeviceMonitor phyDeviceMonitor = new PhyDeviceMonitor();
	
	@Resource
	private PhyDeviceMonitorService phyDeviceMonitorService;
	
	@Resource
	private PhyDickInfoService phyDickInfoService;
	
	@Resource
	private PhyCpuInfoService phyCpuInfoService;
	
	@Resource
	private PhyMemoryInfoService phyMemoryInfoService;
	
	@Resource
	private PhyNetworkInfoService phyNetworkInfoService;
	
	@RequestMapping(value="index")
	public String index(Model model) throws Exception{
		FtlFactory ftlFactory = new DataUtil.FtlFactory();
		model.addAttribute("dickInfo", ftlFactory.build(Configuration.get(R.Charts.PIE2)).process(phyDeviceMonitorService.getDiskInfo()));
		model.addAttribute("memoryInfo", ftlFactory.build(Configuration.get(R.Charts.PIE2)).process(phyDeviceMonitorService.getMemory()));
		model.addAttribute("cpuInfo", ftlFactory.build(Configuration.get(R.Charts.PIE2)).process(phyDeviceMonitorService.getCpu()));
		model.addAttribute("platformInfo",phyDeviceMonitor.getPlatformInfo());
		return "monitor/phyDevice/index";
	}
	
	@RequestMapping(value="dickMonitorDetails")
	public String dickMonitorDetails(Model model) throws Exception{
		model.addAttribute("dickInfo", JSONObject.fromObject(phyDeviceMonitor.getDisk()).toString());
		return "monitor/phyDevice/dickMonitorDetails";
	}
	
	@RequestMapping(value="queryPlatformWhoList")
	public void queryPlatformWhoList() throws Exception{
		putJson(phyDeviceMonitorService.queryPlatformWhoList(getPageSet()));
	}
	
	@RequestMapping(value="diskInfo")
	public String diskInfo(Model model) throws Exception{
		model.addAttribute("dickChartInfos",phyDickInfoService.getDickChartInfos());
		return "monitor/phyDevice/diskInfo";
	}
	
	@RequestMapping(value="queryDiskInfoList")
	public void queryDiskInfoList() throws Exception{
		putJson(phyDickInfoService.queryDiskInfoList(getPageSet()));
	}
	
	@RequestMapping(value="cpuInfo")
	public String cpuInfo(Model model) throws Exception{
		List<CpuInfo> cpuInfos = phyDeviceMonitor.getCpu(true).getCpuInfos();
		if(cpuInfos.size() >= 1){
			model.addAttribute("cpuCount",cpuInfos.size());
			model.addAttribute("cpuInfo",cpuInfos.get(0));
		}
		model.addAttribute("cpuChartInfos",phyCpuInfoService.getCpuChartInfos());
		return "monitor/phyDevice/cpuInfo";
	}
	
	@RequestMapping(value="queryCpuInfoList")
	public void queryCpuInfoList() throws Exception{
		putJson(phyCpuInfoService.queryCpuInfoList(getPageSet()));
	}
	
	@RequestMapping(value="memoryInfo")
	public String memoryInfo(Model model) throws Exception{
		Memory memory = phyDeviceMonitor.getMemory();
		model.addAttribute("physicalMemory",phyMemoryInfoService.disposePhysicalMemory(memory.getPhysicalMemory()));
		model.addAttribute("warpMemory",phyMemoryInfoService.disposeWarpMemory(memory.getWarpMemory()));
		model.addAttribute("memoryChartInfos", phyMemoryInfoService.getMemoryChartInfos());
		return "monitor/phyDevice/memoryInfo";
	}
	
	@RequestMapping(value="networkInfo")
	public String networkInfo(Model model) throws Exception{
		model.addAttribute("networkCardTypes",phyNetworkInfoService.getNetworkCardTypes());
		return "monitor/phyDevice/networkInfo";
	}
	
	@RequestMapping(value="networkDetailInfo")
	public String networkDetailInfo(Model model) throws Exception{
		String belongNetworkCard = (String) reqParameter("belongNetworkCard");
		{
			model.addAttribute("executeTime",Integer.parseInt(reqParameter("executeTime")+"")*60*1000);
			model.addAttribute("executeIntervalTime",reqParameter("executeIntervalTime"));
			model.addAttribute("belongNetworkCard",belongNetworkCard);
		}
		model.addAttribute("netInterfaceStat",phyNetworkInfoService.getNetInterfaceStat(belongNetworkCard));
		model.addAttribute("netInterfaceConfig",phyNetworkInfoService.getNetInterfaceConfig(belongNetworkCard));
		model.addAttribute("baseInfo",phyDeviceMonitor.getNetwork());
		return "monitor/phyDevice/networkDetailInfo";
	}
	
	@RequestMapping(value="getNetworkChartData")
	public void getNetworkChartData(Model model) throws Exception{
		String belongNetworkCard = (String) reqParameter("belongNetworkCard");
		putJson(phyNetworkInfoService.getNetworkChartData(belongNetworkCard));
	}
}
