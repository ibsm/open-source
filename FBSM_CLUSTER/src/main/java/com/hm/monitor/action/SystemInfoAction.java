package com.hm.monitor.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hm.common.BaseAction;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 上午3:22:04 2016年3月11日
 * @statement 
 * @team 
 * @email shishun156@gmail.com
 * @describe 应用服务器信息
 */
@Controller
@RequestMapping("monitor/systemInfo")
public class SystemInfoAction extends BaseAction{

	@RequestMapping(value="index")
	public String index(){
		return "monitor/systemInfo/index";
	}
}
