package com.hm.wflow.dao;

import java.util.Map;

import com.hm.common.PageSet;
import com.hm.wflow.entity.ActCusNode;

public interface ActCusNodeMapper {
	int deleteByPrimaryKey(String id);

	int insert(ActCusNode record);

	ActCusNode selectByPrimaryKey(String id);

	int updateByPrimaryKey(ActCusNode record);

	PageSet queryList(Map<String, Object> pageSet);
}