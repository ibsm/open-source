package com.hm.wflow.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.PageSet;
import com.hm.common.util.DataUtil;
import com.hm.common.util.DataUtil.StringUtil;
import com.hm.sys.entity.CmdbSysUser;
import com.hm.wflow.dao.ActCusNodeMapper;
import com.hm.wflow.dao.ActCusNodeRefMapper;
import com.hm.wflow.entity.ActCusNode;
import com.hm.wflow.entity.ActCusNodeRef;

@Service
public class ActCusNodeService extends BaseService {

	@Autowired
	private ActCusNodeMapper actCusNodeMapper;
	
	@Autowired
	private ActCusNodeRefMapper actCusNodeRefMapper;
	
	public PageSet queryList(ActCusNode actCusNode, PageSet pageSet) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("name", actCusNode.getName());
		params.put("query", pageSet);
		pageSet = assemblyPaging(actCusNodeMapper.queryList(params),params);
		@SuppressWarnings("unchecked")
		List<ActCusNode> actCusNodes = (List<ActCusNode>) pageSet.getRows();
		JSONArray items = new JSONArray();
		for (ActCusNode node : actCusNodes) {
			JSONObject item = new JSONObject();
			item.put("id", node.getId());
			item.put("name", node.getName());
			item.put("flowType", transDictInsName4id(node.getFlowType()));
			item.put("note", node.getNote());
			item.put("createTime", DataUtil.DateUtil.yyyyMMddhhmm(node.getCreateTime()));
			
			items.add(item);
		}
		pageSet.setRows(items);
		return pageSet;
	}

	public ActCusNode getActCusNodeId(ActCusNode actCusNode) {
		return actCusNodeMapper.selectByPrimaryKey(actCusNode.getId());
	}

	public void deleteActCusNode(ActCusNode actCusNode) {
		actCusNodeMapper.deleteByPrimaryKey(actCusNode.getId());
	}

	public void updateActCusNode(ActCusNode actCusNode) {
		actCusNodeMapper.updateByPrimaryKey(actCusNode);
	}

	public void addActCusNode(ActCusNode actCusNode) {
		actCusNode.setId(generateId());
		actCusNode.setState(1);
		actCusNode.setCreateTime(new Date());
		actCusNodeMapper.insert(actCusNode);
	}

	public String getUsersByNode(String nodeId) throws Exception{
		List<CmdbSysUser> selectAllUser = sysUserMapper.selectAllUser();
		JSONArray items = new JSONArray();
		ActCusNodeRef record = new ActCusNodeRef(); 
		for (CmdbSysUser cmdbSysUser : selectAllUser) {
			JSONObject item = new JSONObject();
			item.put("id", cmdbSysUser.getId());
			item.put("pId", "0");
			item.put("name", cmdbSysUser.getName());
			{
				record.setNodeId(nodeId);
				record.setExeId(cmdbSysUser.getId());
				item.put("checked",null != actCusNodeRefMapper.selectActCusNodeRef(record)?"true":"false");
			}
			item.put("open", "true");
			items.add(item);
		}
		return items.toString();
	}

	public void saveExeConfig(String nodeId, String users) {
		{//清空数据
			actCusNodeRefMapper.deleteByNodeId(nodeId);
		}
		{//添加数据
			if(!StringUtil.isBlank(users)){
				String[] userIds = StringUtil.dislogeBatchTagSEIds(users,false);
				for (String userId : userIds) {
					ActCusNodeRef record = new ActCusNodeRef();
					record.setId(generateId());
					record.setExeId(userId);
					record.setNodeId(nodeId);
					actCusNodeRefMapper.insert(record);
				}
			}
		}
	}
	
}
