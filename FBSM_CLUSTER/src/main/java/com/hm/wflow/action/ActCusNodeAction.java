package com.hm.wflow.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hm.common.BaseAction;
import com.hm.wflow.entity.ActCusNode;
import com.hm.wflow.service.ActCusNodeService;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 流程节点定义 
 */
@Controller
@RequestMapping("wflow/actCusNode")
public class ActCusNodeAction extends BaseAction {

	@Resource
	private ActCusNodeService actCusNodeService;
	
	@RequestMapping(value="index")
	public String index(){
		return "wflow/actCusNode/index";
	}
	
	@RequestMapping(value="add")
	public String add(){
		return "wflow/actCusNode/add";
	}
	
	@RequestMapping(value="update")
	public String update(ActCusNode actCusNode,Model model){
		model.addAttribute("result", actCusNodeService.getActCusNodeId(actCusNode));
		return "wflow/actCusNode/update";
	}
	
	@RequestMapping(value="detail")
	public String detail(ActCusNode actCusNode,Model model){
		model.addAttribute("result", actCusNodeService.getActCusNodeId(actCusNode));
		return "wflow/actCusNode/detail";
	}
	
	@RequestMapping(value="exeConfig")
	public String exeConfig(ActCusNode actCusNode,Model model) throws Exception{
		model.addAttribute("nodeId",actCusNode.getId());
		model.addAttribute("flowNodeExeUsers", actCusNodeService.getUsersByNode(actCusNode.getId()));
		return "wflow/actCusNode/exeConfig";
	}
	
	@RequestMapping(value="saveExeConfig")
	public void saveExeConfig(HttpServletRequest request,HttpServletResponse response,ActCusNode actCusNode){
		actCusNodeService.saveExeConfig(actCusNode.getId(),reqParameter("pres")+"");
	}
	
	@RequestMapping(value="saveAdd")
	public void saveAdd(HttpServletRequest request,HttpServletResponse response,ActCusNode actCusNode){
		actCusNode.setCreateUser(getLoginUser());
		actCusNodeService.addActCusNode(actCusNode);
	}
	
	@RequestMapping(value="saveUpdate")
	public void saveUpdate(HttpServletRequest request,HttpServletResponse response,ActCusNode actCusNode){
		actCusNodeService.updateActCusNode(actCusNode);
	}
	
	@RequestMapping(value="saveDelete")
	public void saveDelete(HttpServletRequest request,HttpServletResponse response,ActCusNode actCusNode){
		actCusNodeService.deleteActCusNode(actCusNode);
	}
	
	@RequestMapping(value="queryList")
	public void queryList(ActCusNode actCusNode){
		putJson(actCusNodeService.queryList(actCusNode, getPageSet()));
	}
}
