package com.hm.wflow.action;

import java.io.InputStream;
import java.io.OutputStream;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hm.common.BaseAction;
import com.hm.wflow.entity.Deployment;
import com.hm.wflow.service.DeploymentProcessDefineService;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 部属流程定义
 */
@Controller
@RequestMapping("wflow/deploy")
public class DeployProcessDefineAction extends BaseAction {

	@Resource
	private DeploymentProcessDefineService deploymentProcessDefineService;
	
	@RequestMapping(value="index")
	public String index(){
		return "wflow/deploy/index";
	}
	
	@RequestMapping(value="addDeploy")
	public String addDeploy(){
		return "wflow/deploy/addDeploy";
	}
	
	@RequestMapping(value="saveDeployProcess")
	public void saveDeployProcess(HttpServletRequest request,HttpServletResponse response,Deployment deployment) throws Exception{
		deploymentProcessDefineService.deploymentProcessDefine(deployment);
	}
	
	@RequestMapping(value="saveDeleteDeployProcess")
	public void saveDeleteDeployProcess(HttpServletRequest request,HttpServletResponse response,Deployment deployment) throws Exception{
		deploymentProcessDefineService.deleteDeployProcess(deployment);
	}
	
	@RequestMapping(value="queryDeployProcessList")
	public void queryDeployProcessList(Deployment deployment) throws Exception{
		putJson(deploymentProcessDefineService.queryDeployProcessList(deployment, getPageSet()));
	}
	
	@RequestMapping(value="queryDeployProcessListByDeployment")
	public void queryDeployProcessListByDeployment(Deployment deployment) throws Exception{
		putJson(deploymentProcessDefineService.queryDeployProcessListByDeployment(deployment, getPageSet()));
	}
	
	@RequestMapping(value="outPutFlowPic")
	public void outPutFlowPic() throws Exception{
		InputStream inputStream = deploymentProcessDefineService.outPutFlowPic(reqParameter("deploymentId")+"", reqParameter("resourceName")+"");
		OutputStream outputStream = response.getOutputStream();
		for(int b=-1;(b = inputStream.read()) != -1;){
			outputStream.write(b);
		}
		outputStream.flush();
		outputStream.close();
		inputStream.close();
	}
}
