package com.hm.sys.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hm.common.BaseAction;
import com.hm.common.config.Configuration;
import com.hm.common.util.CellNature;
import com.hm.common.util.DataUtil.FtlFactory;
import com.hm.common.util.DataUtil.StringUtil;
import com.hm.common.util.ExportExcel;
import com.hm.common.util.R.Report;
import com.hm.sys.entity.CmdbSysUser;
import com.hm.sys.service.SysUserService;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 用户管理
 */
@Controller
@RequestMapping("sys/user")
public class SysUserAction extends BaseAction {
	
	@Resource
	private SysUserService sysUserService;
	
	@RequestMapping(value="index")
	public String index(){
		return "sys/user/index";
	}
	
	@RequestMapping(value="add")
	public String add(){
		return "sys/user/add";
	}
	
	@RequestMapping(value="update")
	public String update(CmdbSysUser sysUser,Model model) {
		model.addAttribute("result", sysUserService.getSysUserById(sysUser));
		return "sys/user/update";
	}
	
	@RequestMapping(value="detail")
	public String detail(CmdbSysUser sysUser,Model model){
		model.addAttribute("result", sysUserService.getSysUserById(sysUser));
		return "sys/user/detail";
	}
	
	@RequestMapping(value="userRole")
	public String userRole(CmdbSysUser sysUser,Model model){
		model.addAttribute("userId", sysUser.getId());
		model.addAttribute("userRoles", sysUserService.getSysUserRoles(sysUser.getId()));
		return "sys/user/userRole";
	}

	@RequestMapping(value="saveAdd")
	public void saveAdd(HttpServletRequest request,HttpServletResponse response,CmdbSysUser sysUser){
		sysUser.setCreateUser(getLoginUser());
		sysUserService.addSysUser(sysUser);
	}

	@RequestMapping(value="saveUpdate")
	public void saveUpdate(HttpServletRequest request,HttpServletResponse response,CmdbSysUser sysUser){
		sysUserService.updateSysUser(sysUser);
	}
	
	@RequestMapping(value="saveDelete")
	public void saveDelete(HttpServletRequest request,HttpServletResponse response,CmdbSysUser sysUser){
		sysUserService.deleteSysUser(sysUser);
	}
	
	@RequestMapping(value="saveUserRole")
	public void saveUserRole(HttpServletRequest request,HttpServletResponse response,CmdbSysUser sysUser){
		sysUserService.saveUserRoles(sysUser,request.getParameter("roles"));
	}
	
	@RequestMapping(value="queryList")
	public void queryList(CmdbSysUser sysUser){
		putJson(sysUserService.queryList(sysUser, getPageSet()));
	}

	@RequestMapping(value="exportExcelByUserId")
	public void exportExcelByUserId(CmdbSysUser sysUser) throws Exception{
		ExportExcel excel = new ExportExcel().createAdvanced();
		{//设置头
			excel.setCell(new CellNature().simple("编号", "id"));
			excel.setCell(new CellNature().simple("名称", "name"));
			excel.setCell(new CellNature().simple("创建日期", "createTime"));
			excel.setCell(new CellNature().simple("使用状态", "state"));
		}
		
		{//设置内容
			List<CmdbSysUser> content = new ArrayList<CmdbSysUser>();
			String ids[] = StringUtil.dislogeBatchTagSEIds(sysUser.getId(),false);
			for (String id : ids) {
				sysUser.setId(id);
				content.add(sysUserService.getSysUserById(sysUser));
			}
			excel.content(content, CmdbSysUser.class);
		}
		//完成对外输出
		excel.write2(response);
		
	}
	
	@RequestMapping(value="exportWord")
	public void exportWord() throws Exception{
		String[] args = {"B","C","D","E","F","G","H","I"};
		Map<String,String> mapper = new HashMap<String,String>();
		for (String str : args) {
			mapper.put(str+"7", (Math.random()*100)+"");
		}
		
		mapper.put("cardNumber", "9527");
		mapper.put("mouth", "02");
		mapper.put("time", "2016年01月26日");
		
		response.reset();
		response.setContentType("application/vnd.ms-excel;charset=utf-8");
		response.addHeader("Content-Disposition", "attachment;filename="+UUID.randomUUID()+".xls");
		
		new FtlFactory().rootDirectory(Configuration.get(Report.ROOT_DIRECTORY)).build("etc_demo.xml").process(mapper,response.getWriter());
	}
	
	@RequestMapping(value="print")
	public String print(){
		return "sys/user/print";
	}
}
