package com.hm.sys.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hm.common.BaseAction;
import com.hm.sys.entity.CmdbSysPer;
import com.hm.sys.service.CmdbSysPerService;

/**
 * @author shishun.wang
 * @date 2:06:34 AM Jan 5, 2016
 * @version 1.0
 * @describe 权限管理
 */
@Controller
@RequestMapping("sys/per")
public class SysPerAction extends BaseAction {

	@Resource
	private CmdbSysPerService sysPerService;
	
	@RequestMapping(value="index")
	public String index(){
		return "sys/per/index";
	}
	
	@RequestMapping(value="add")
	public String add(){
		return "sys/per/add";
	}
	
	@RequestMapping(value="update")
	public String update(CmdbSysPer sysPer,Model model){
		model.addAttribute("result", sysPerService.getSysPerById(sysPer));
		return "sys/per/update";
	}
	
	@RequestMapping(value="detail")
	public String detail(CmdbSysPer sysPer,Model model){
		model.addAttribute("result", sysPerService.getSysPerById(sysPer));
		return "sys/per/detail";
	}
	
	@RequestMapping(value="saveAdd")
	public void saveAdd(HttpServletRequest request,HttpServletResponse response,CmdbSysPer sysPer){
		sysPer.setCreateUser(getLoginUser());
		sysPerService.addSysPer(sysPer);
	}
	
	@RequestMapping(value="saveUpdate")
	public void saveUpdate(HttpServletRequest request,HttpServletResponse response,CmdbSysPer sysPer){
		sysPerService.updateSysPer(sysPer);
	}
	
	@RequestMapping(value="saveDelete")
	public void saveDelete(HttpServletRequest request,HttpServletResponse response,CmdbSysPer sysPer){
		sysPerService.deleteSysPer(sysPer);
	}
	
	@RequestMapping(value="queryList")
	public void queryList(CmdbSysPer sysPer){
		putJson(sysPerService.queryList(sysPer, getPageSet()));
	}
}
