package com.hm.sys.action;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hm.common.BaseAction;
import com.hm.common.util.DataUtil.SessionUtil;
import com.hm.common.util.Encrypt.Md5;
import com.hm.common.util.LoginUser;
import com.hm.sys.entity.CmdbSysMenu;
import com.hm.sys.entity.CmdbSysUser;

@Controller
public class AccessAction extends BaseAction{

	@RequestMapping(value={"index","/","login"})
	public String index(@RequestParam(value="isError",defaultValue="undefine") String isError,Model model){
		if(!"undefine".equals(isError)){
			model.addAttribute("msg", "<span style='color:red;font-size:14px;font-weight:500;'>用户名或密码错误</span>");
		}
		return "index";
	}
	
	@RequestMapping("unauthorized")
	public String unauthorized() throws Exception{
		return "unauthorized";
	}
	
	@RequestMapping(value="main")
	public String main(Model model) throws Exception{
//		CmdbSysUser user = (CmdbSysUser) SecurityUtils.getSubject().getSession().getAttribute("LOGIN_USER_INFO");
		LoginUser user = (LoginUser) SessionUtil.getSession(request).getAttribute("_LOGIN_USER_INFO");
		if(null != user){
			model.addAttribute("loginUser", user.getName());
			model.addAttribute("lastLoginTime",user.getLastLoginTime());
			return "main";
		}
		return "redirect:/login.do?code="+Math.random();
	}
	
	@RequestMapping("quitSystem")
	public String quitSystem() throws Exception{
//		Subject subject=SecurityUtils.getSubject();
//		subject.logout();
		SessionUtil.getSession(request).setAttribute("_LOGIN_USER_INFO",null);
		return "redirect:/login.do?code="+Math.random();
	}
	
	@RequestMapping("check")
	public String enter(CmdbSysUser user,Model model) throws Exception{
		String password = Md5.getMD5Code(user.getPwd());
//		SessionUtil.getSession(request).setAttribute("LOGIN_PWD", password);
//		Subject subject=SecurityUtils.getSubject();
//		UsernamePasswordToken token = new UsernamePasswordToken(user.getName(), password);
		try{
//			subject.login(token);
			
			CmdbSysUser sysUser = sysUserService.selectByAccount(user.getName());
			if(null != sysUser){//将用户信息放入到session中
				if(sysUser.getPwd().equals(password)){
					if(sysUser.getState() == 1){
						LoginUser loginUser = sysSessionService.addSession(sysUser);
						SessionUtil.getSession(request).setAttribute("_LOGIN_USER_INFO", loginUser);
						return "redirect:/main.do";
					}else{
						request.setAttribute("user", user);
						request.setAttribute("errorMsg", "该用户以被系统管理员注销禁止使用,请联系系统管理员");
						return "redirect:/login.do?isError=ok&code="+Math.random();
					}
				}
			}
			
			request.setAttribute("user", user);
			request.setAttribute("errorMsg", "用户名或密码错误！");
			return "redirect:/login.do?isError=ok&code="+Math.random();
		}catch(Exception e){
			e.printStackTrace();
			request.setAttribute("user", user);
			request.setAttribute("errorMsg", "用户名或密码错误！");
			return "redirect:/login.do?isError=ok&code="+Math.random();
		}
	}
	
	@RequestMapping(value="loadNav",method=RequestMethod.POST,produces = {"text/json;charset=UTF-8"})
	public @ResponseBody String loadNav(CmdbSysMenu entity) throws Exception{
		if("1".equals(getLoginUser())){
			return sysMenuService.loadSuperNav(entity,Boolean.parseBoolean(request.getParameter("recursive"))).toString();
		}
		return sysMenuService.loadNav(entity,
				Boolean.parseBoolean(request.getParameter("recursive")),
				sysMenuService.getMenusByLoginUser(getLoginUser()
						)).toString();
	}
}
