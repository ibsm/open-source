package com.hm.sys.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hm.common.BaseAction;
import com.hm.sys.entity.CmdbSysDict;
import com.hm.sys.service.SysDictTypeService;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 数据字典类型管理
 */
@Controller
@RequestMapping("sys/dictType")
public class SysDictTypeAction extends BaseAction {
	
	@Resource
	private SysDictTypeService dictTypeService;
	
	@RequestMapping(value="index")
	public String index(){
		return "sys/dictType/index";
	}
	
	@RequestMapping(value="add")
	public String add(){
		return "sys/dictType/add";
	}
	
	@RequestMapping(value="update")
	public String update(CmdbSysDict sysDict,Model model){
		model.addAttribute("result", dictTypeService.getSysDictById(sysDict));
		return "sys/dictType/update";
	}
	
	@RequestMapping(value="detail")
	public String detail(CmdbSysDict sysDict,Model model){
		model.addAttribute("result", dictTypeService.getSysDictById(sysDict));
		return "sys/dictType/detail";
	}
	
	@RequestMapping(value="saveAdd")
	public void saveAdd(HttpServletRequest request,HttpServletResponse response,CmdbSysDict sysDict){
		sysDict.setCreateUser(getLoginUser());
		dictTypeService.addSysDict(sysDict);
	}
	
	@RequestMapping(value="saveUpdate")
	public void saveUpdate(HttpServletRequest request,HttpServletResponse response,CmdbSysDict sysDict){
		dictTypeService.updateSysDict(sysDict);
	}
	
	@RequestMapping(value="saveDelete")
	public void saveDelete(HttpServletRequest request,HttpServletResponse response,CmdbSysDict sysDict){
		dictTypeService.deleteSysDict(sysDict);
	}
	
	@RequestMapping(value="queryList")
	public void queryList(CmdbSysDict sysDict){
		putJson(dictTypeService.queryList(sysDict, getPageSet()));
	}
	
}
