package com.hm.sys.dao;

import com.hm.sys.entity.CmdbSysSession;

public interface CmdbSysSessionMapper {
	
	int deleteByPrimaryKey(String id);

	int insert(CmdbSysSession record);

	CmdbSysSession selectByPrimaryKey(String id);
	
	CmdbSysSession selectByUserId(String userId);

	int update(CmdbSysSession record);
	
	int clearAll();
}