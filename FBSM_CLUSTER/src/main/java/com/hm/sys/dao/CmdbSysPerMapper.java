package com.hm.sys.dao;

import java.util.List;
import java.util.Map;

import com.hm.common.PageSet;
import com.hm.sys.entity.CmdbSysPer;

public interface CmdbSysPerMapper {
    int deleteByPrimaryKey(String id);

    int insert(CmdbSysPer record);

    CmdbSysPer selectByPrimaryKey(String id);

    int updateByPrimaryKey(CmdbSysPer record);
    
    PageSet queryList(Map<String, Object> pageSet);
    
    List<CmdbSysPer> getSysPerByType(String preType);
}