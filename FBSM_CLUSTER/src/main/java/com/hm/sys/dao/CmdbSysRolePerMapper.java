package com.hm.sys.dao;

import java.util.List;

import com.hm.sys.entity.CmdbSysRolePer;

public interface CmdbSysRolePerMapper {
	
	int clearSysRolePers(String roleId);
	
	List<CmdbSysRolePer> getRoleAllPres(String roleId);
	
	CmdbSysRolePer selectSysRolePer(CmdbSysRolePer record);
	
    int deleteByPrimaryKey(String id);

    int insert(CmdbSysRolePer record);

    CmdbSysRolePer selectByPrimaryKey(String id);

    int updateByPrimaryKey(CmdbSysRolePer record);
}