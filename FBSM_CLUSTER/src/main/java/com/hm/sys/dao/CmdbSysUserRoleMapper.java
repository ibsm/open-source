package com.hm.sys.dao;

import java.util.List;

import com.hm.sys.entity.CmdbSysUserRole;

public interface CmdbSysUserRoleMapper {
	
	void clearSysUserRoles(CmdbSysUserRole record);
	
	CmdbSysUserRole selectSysUserRole(CmdbSysUserRole record);
	
	List<CmdbSysUserRole> getUserAllRoles(String uerId);
	
    int deleteByPrimaryKey(String id);

    int insert(CmdbSysUserRole record);

    CmdbSysUserRole selectByPrimaryKey(String id);

    int updateByPrimaryKey(CmdbSysUserRole record);
}