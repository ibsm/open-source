package com.hm.sys.dao;

import java.util.List;

import com.hm.sys.entity.CmdbRoleMenu;

public interface CmdbSysRoleMenuMapper {
    int deleteByPrimaryKey(String id);

    int insert(CmdbRoleMenu record);

    List<CmdbRoleMenu> getRoleAllMenus(String roleId);
    
    void clearRoleMenus(CmdbRoleMenu record);
    
    CmdbRoleMenu selectByPrimaryKey(String id);
    
    CmdbRoleMenu selectRoleMenu(CmdbRoleMenu record);

    int updateByPrimaryKey(CmdbRoleMenu record);
}