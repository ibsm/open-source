package com.hm.sys.dao;

import java.util.List;
import java.util.Map;

import com.hm.common.PageSet;
import com.hm.sys.entity.CmdbSysDictIns;

public interface CmdbSysDictInsMapper {
    int deleteByPrimaryKey(String id);

    int insert(CmdbSysDictIns record);

    CmdbSysDictIns selectByPrimaryKey(String id);

    int updateByPrimaryKey(CmdbSysDictIns record);
    
    PageSet queryList(Map<String, Object> pageSet);
    
    List<CmdbSysDictIns> getSysDictInsByDictType(String dictType);
}