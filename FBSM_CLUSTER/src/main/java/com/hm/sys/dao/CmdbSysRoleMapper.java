package com.hm.sys.dao;

import java.util.List;
import java.util.Map;

import com.hm.common.PageSet;
import com.hm.sys.entity.CmdbSysRole;

public interface CmdbSysRoleMapper {
    int deleteByPrimaryKey(String id);

    int insert(CmdbSysRole record);

    List<CmdbSysRole> selectSysRole(CmdbSysRole record);
    
    CmdbSysRole selectByPrimaryKey(String id);

    int updateByPrimaryKey(CmdbSysRole record);
    
    PageSet queryList(Map<String, Object> pageSet);
}