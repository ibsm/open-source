package com.hm.sys.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.config.Configuration;
import com.hm.common.util.DataUtil;
import com.hm.common.util.LoginUser;
import com.hm.common.util.R;
import com.hm.sys.dao.CmdbSysSessionMapper;
import com.hm.sys.entity.CmdbSysSession;
import com.hm.sys.entity.CmdbSysUser;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 下午8:54:48 2016年3月19日
 * @statement 
 * @team 
 * @email shishun156@gmail.com
 * @describe 系统登录用户session处理
 */
@Service("getSysSessionService")
public class SysSessionService extends BaseService {

	@Autowired
	private CmdbSysSessionMapper cmdbSysSessionMapper;
	
	public CmdbSysSession selectByUserId(String userId){
		return cmdbSysSessionMapper.selectByUserId(userId);
	}
	
	public LoginUser addSession(CmdbSysUser sysUser){
		long version = 1l;
		boolean openCas = Configuration.getBoolean(R.SystemInit.FBSM_SYSTEM_CAS);
		LoginUser loginUser = new LoginUser();
		
		if(openCas){
			CmdbSysSession sysSession = cmdbSysSessionMapper.selectByUserId(sysUser.getId());
			if(null == sysSession){
				loginUser.setLastLoginTime(DataUtil.DateUtil.yyyyMMddhhmm(new Date()));
				
				CmdbSysSession session = new CmdbSysSession();
				session.setId(generateId());
				session.setState(1);
				session.setCreateTime(new Date());
				session.setUpdateTime(new Date());
				session.setCreateUser(sysUser.getId());
				session.setVersion(version);
				session.setSessionId(generateId());
				cmdbSysSessionMapper.insert(session);
			}else{
				loginUser.setLastLoginTime(DataUtil.DateUtil.yyyyMMddhhmm(sysSession.getUpdateTime()));
				
				version = sysSession.getVersion() + 1;
				sysSession.setUpdateTime(new Date());
				sysSession.setSessionId(generateId());
				cmdbSysSessionMapper.update(sysSession);
			}
		}
		
		//封装登录用户信息
		loginUser.setId(sysUser.getId());
		loginUser.setName(sysUser.getName());
		loginUser.setState(1);
		loginUser.setVersion(version);
	
		return loginUser;
	}
	
	public void clearAll(){
		cmdbSysSessionMapper.clearAll();
	}
}
