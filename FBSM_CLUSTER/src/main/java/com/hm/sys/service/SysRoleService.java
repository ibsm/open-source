package com.hm.sys.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.PageSet;
import com.hm.common.util.DataUtil;
import com.hm.common.util.DataUtil.StringUtil;
import com.hm.common.util.R.DictType;
import com.hm.sys.dao.CmdbSysPerMapper;
import com.hm.sys.dao.CmdbSysRoleMenuMapper;
import com.hm.sys.dao.CmdbSysMenuMapper;
import com.hm.sys.dao.CmdbSysRoleMapper;
import com.hm.sys.dao.CmdbSysRolePerMapper;
import com.hm.sys.entity.CmdbRoleMenu;
import com.hm.sys.entity.CmdbSysDictIns;
import com.hm.sys.entity.CmdbSysMenu;
import com.hm.sys.entity.CmdbSysPer;
import com.hm.sys.entity.CmdbSysRole;
import com.hm.sys.entity.CmdbSysRolePer;

@Service
@SuppressWarnings("unchecked")
public class SysRoleService extends BaseService{

	@Autowired
	private CmdbSysRoleMapper sysRoleMapper;
	
	@Autowired
	private CmdbSysMenuMapper sysMenuMapper;
	
	@Autowired
	private CmdbSysRoleMenuMapper roleMenuMapper;
	
	@Autowired
	private CmdbSysRolePerMapper sysRolePerMapper;
	
	@Autowired
	private CmdbSysPerMapper sysPerMapper;
	
	public PageSet queryList(CmdbSysRole sysRole, PageSet pageSet){
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("name", sysRole.getName());
		params.put("query", pageSet);
		pageSet = assemblyPaging(sysRoleMapper.queryList(params),params);
		List<CmdbSysRole> cmdbSysRoles = (List<CmdbSysRole>) pageSet.getRows();
		JSONArray items = new JSONArray();
		for (CmdbSysRole cmdbSysRole : cmdbSysRoles) {
			JSONObject item = new JSONObject();
			item.put("id", cmdbSysRole.getId());
			item.put("name", cmdbSysRole.getName());
			item.put("note", cmdbSysRole.getNote());
			item.put("createTime", DataUtil.DateUtil.yyyyMMddhhmm(cmdbSysRole.getCreateTime()));
			
			items.add(item);
		}
		pageSet.setRows(items);
		return pageSet;
	}

	public void addSysRole(CmdbSysRole sysRole) {
		sysRole.setId(generateId());
		sysRole.setState(1);
		sysRole.setCreateTime(new Date());
		sysRoleMapper.insert(sysRole);
	}

	public void updateSysRole(CmdbSysRole sysRole) {
		sysRoleMapper.updateByPrimaryKey(sysRole);
	}

	public void deleteSysRole(CmdbSysRole sysRole) {
		sysRoleMapper.deleteByPrimaryKey(sysRole.getId());
	}

	public CmdbSysRole getSysRoleById(CmdbSysRole sysRole) {
		return sysRoleMapper.selectByPrimaryKey(sysRole.getId());
	}

	public JSONArray getSysRoleMenuPers(CmdbSysRole sysRole,String parentId) {
		
		JSONArray items = new JSONArray();
		List<CmdbSysMenu> selectByParents = sysMenuMapper.selectByParentId(parentId);
		CmdbRoleMenu record = new CmdbRoleMenu();
		for (CmdbSysMenu cmdbSysMenu : selectByParents) {
			JSONObject item = new JSONObject();
			item.put("id", cmdbSysMenu.getId());
			item.put("pId", cmdbSysMenu.getParentId());
			item.put("name", cmdbSysMenu.getName());
			{
				record.setMenuId(cmdbSysMenu.getId());
				record.setRoleId(sysRole.getId());
				item.put("checked", null!=roleMenuMapper.selectRoleMenu(record)?"true":"false");
			}
			item.put("open", "true");
			items.add(item);
			
			JSONArray pers = getSysRoleMenuPers(sysRole, cmdbSysMenu.getId());
			for (int i = 0; i < pers.size(); i++) {
				items.add(pers.get(i));
			}
		}
		return items;
	}

	public void saveRoleMenus(CmdbSysRole sysRole, String menus) {
		//清空之前所有数据
		{
			CmdbRoleMenu record = new CmdbRoleMenu();
			record.setRoleId(sysRole.getId());
			roleMenuMapper.clearRoleMenus(record);
		}
		//添加数据
		if(!StringUtil.isBlank(menus)){
			String[] menuIds = StringUtil.dislogeBatchTagSEIds(menus,false);
			for (String menuId : menuIds) {
				CmdbRoleMenu record = new CmdbRoleMenu();
				record.setId(generateId());
				record.setMenuId(menuId);
				record.setRoleId(sysRole.getId());
				roleMenuMapper.insert(record);
			}
		}
	}

	public String getSysRoleDataPers(CmdbSysRole sysRole) {
		List<CmdbSysDictIns> sysDictInsArray = getSysDictInsByDictType(DictType.PER_TYPE);
		JSONArray items = new JSONArray();
		int index = -1;
		CmdbSysRolePer record = new CmdbSysRolePer();
		for (CmdbSysDictIns sysDictIns : sysDictInsArray) {
			{
				JSONObject item = new JSONObject();
				
				item.put("id", index);
				item.put("name", sysDictIns.getName());
				item.put("pId", "0");
				item.put("checked","false");
				item.put("open", "true");
				
				items.add(item);
			}
			List<CmdbSysPer> perByTypes = sysPerMapper.getSysPerByType(sysDictIns.getId());
			for (CmdbSysPer sysPer : perByTypes) {
				JSONObject item = new JSONObject();
				
				item.put("id", sysPer.getId());
				item.put("name", sysPer.getName());
				item.put("pId", index);
				{
					record.setPerId(sysPer.getId());
					record.setRoleId(sysRole.getId());
					item.put("checked",null != sysRolePerMapper.selectSysRolePer(record)?"true":"false");
				}
				item.put("open", "true");
				
				items.add(item);
			}
			-- index;
		}
		return items.toString();
	}

	public void saveRoleDataPres(CmdbSysRole sysRole, String pres) {
		//清空之前所有数据
		{
			sysRolePerMapper.clearSysRolePers(sysRole.getId());
		}
		//添加数据
		if(!StringUtil.isBlank(pres)){
			String[] preIds = StringUtil.dislogeBatchTagSEIds(pres,false);
			for (String preId : preIds) {
				if(!preId.startsWith("-")){
					CmdbSysRolePer record = new CmdbSysRolePer();
					record.setId(generateId());
					record.setPerId(preId);
					record.setRoleId(sysRole.getId());
					sysRolePerMapper.insert(record);
				}
			}
		}
		
	}
	
}
