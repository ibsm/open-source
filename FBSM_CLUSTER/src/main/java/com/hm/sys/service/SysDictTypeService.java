package com.hm.sys.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;

import com.hm.common.BaseService;
import com.hm.common.PageSet;
import com.hm.common.util.DataUtil;
import com.hm.sys.entity.CmdbSysDict;

@Service
@SuppressWarnings("unchecked")
public class SysDictTypeService extends BaseService {

	public PageSet queryList(CmdbSysDict sysDict, PageSet pageSet) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("name", sysDict.getName());
		params.put("query", pageSet);
		pageSet = assemblyPaging(sysDictMapper.queryList(params),params);
		List<CmdbSysDict> sysDicts = (List<CmdbSysDict>) pageSet.getRows();
		JSONArray items = new JSONArray();
		for (CmdbSysDict cmdbSysDict : sysDicts) {
			JSONObject item = new JSONObject();
			item.put("id", cmdbSysDict.getId());
			item.put("name", cmdbSysDict.getName());
			item.put("note", cmdbSysDict.getNote());
			item.put("createTime", DataUtil.DateUtil.yyyyMMddhhmm(cmdbSysDict.getCreateTime()));
			
			items.add(item);
		}
		pageSet.setRows(items);
		return pageSet;
	}

	public void addSysDict(CmdbSysDict sysDict) {
		sysDict.setId(generateId());
		sysDict.setState(1);
		sysDict.setCreateTime(new Date());
		sysDictMapper.insert(sysDict);
	}

	public void updateSysDict(CmdbSysDict sysDict) {
		sysDictMapper.updateByPrimaryKey(sysDict);
	}

	public void deleteSysDict(CmdbSysDict sysDict) {
		sysDictMapper.deleteByPrimaryKey(sysDict.getId());
	}

	public CmdbSysDict getSysDictById(CmdbSysDict sysDict) {
		return sysDictMapper.selectByPrimaryKey(sysDict.getId());
	}

	public List<CmdbSysDict> getAllDictTypes() {
		return sysDictMapper.getAllDictTypes();
	}
	
}
