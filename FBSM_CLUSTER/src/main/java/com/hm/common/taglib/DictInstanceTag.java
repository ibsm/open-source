package com.hm.common.taglib;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspWriter;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.web.servlet.tags.RequestContextAwareTag;

import com.hm.common.util.DataUtil.StringUtil;
import com.hm.sys.entity.CmdbSysDictIns;

@SuppressWarnings("serial")
public class DictInstanceTag extends RequestContextAwareTag {

	/**
	 * 编号id
	 */
	private String id;

	/**
	 * 数据字典类型
	 */
	private String dictType;

	/**
	 * 目标数据属性
	 */
	private String name;

	/**
	 * 当前数据字典的值
	 */
	private String value;

	/**
	 * css样式
	 */
	private String cssClass;

	/**
	 * 是否只读
	 */
	private String readOnly;

	/**
	 * 选择事件
	 */
	private String onSelect;

	/**
	 * 点击事件
	 */
	private String onClick;
	
	/**
	 * 样式
	 */
	private String style;
	
	/**
	 * placeholder
	 */
	private String placeholder;
	
	/**
	 * 显示选择
	 */
	private String all;
	
	/**
	 * 必填
	 */
	private String required;
	
	@Override
	protected int doStartTagInternal() throws Exception {
		StringBuffer buffer = new StringBuffer();
		{
			buffer.append("<select ");
			buffer.append(this.getId() + " ");
			buffer.append(this.getName() + " ");
			buffer.append(this.getRequired() + " ");
			buffer.append(this.getReadOnly() + " ");
			buffer.append(this.getOnClick() + " ");
			buffer.append(this.getStyle() + " ");
			buffer.append(this.getPlaceholder() + " ");
			buffer.append(this.getOnSelect() + " ");
			buffer.append(" class='"+ ((StringUtil.isNullEmpty(this.getCssClass())) ? "form-control" : this.getCssClass()) + "' ");
			buffer.append(" >");
			buffer.append(this.getAll());
		}

		if (!StringUtil.isNullEmpty(dictType)) {
			 JdbcTemplate jdbcTemplate = (JdbcTemplate)
			 this.getRequestContext().getWebApplicationContext().getBean("jdbcTemplate");
			 List<CmdbSysDictIns> data = queryDictIns(jdbcTemplate,this.getDictType());
			 if(value != null){
				 for (CmdbSysDictIns dictIns : data) {
					 buffer.append("<option value='"+dictIns.getId()+"' ");
					 if(dictIns.getId().equals(this.getValue())){
						 buffer.append(" selected='true' ");
					 }
					 buffer.append(" >"+dictIns.getName()+"</option>");
				 }
			 }else{
				 for (CmdbSysDictIns dictIns : data) {
					 buffer.append("<option value='"+dictIns.getId()+"'>"+dictIns.getName()+"</option>");
				 }
			 }
		}
		buffer.append("</select>");

		JspWriter jspWriter = pageContext.getOut();
		jspWriter.print(buffer.toString());

		return 0;
	}

		
	private List<CmdbSysDictIns> queryDictIns(JdbcTemplate jdbcTemplate,String dictType){
		String sql = "select id,name from cmdb_sys_dictins where state = 1 and dict_type = ?";
		SqlRowSet rowSet = jdbcTemplate.queryForRowSet(sql,new Object[]{dictType});
		ArrayList<CmdbSysDictIns> dictIns = new ArrayList<CmdbSysDictIns>();
		while(rowSet.next()){
			CmdbSysDictIns sysDictIns = new CmdbSysDictIns();
			sysDictIns.setId(rowSet.getString("id"));
			sysDictIns.setName(rowSet.getString("name"));
			dictIns.add(sysDictIns);
		}
		return dictIns;
	}
	
	public String getDictType() {
		return dictType;
	}

	public void setDictType(String dictType) {
		this.dictType = dictType;
	}

	public String getName() {
		try {
			if (StringUtil.isNullEmpty(name)) {
				return "";
			} else {
				return " name='" + this.name + "'";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	public String getReadOnly() {
		try {
			if (StringUtil.isNullEmpty(readOnly)) {
				return "";
			} else {
				return " disabled='true'";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}

	public String getId() {
		try {
			if (StringUtil.isNullEmpty(id)) {
				return "";
			} else {
				return " id='" + id + "'";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOnSelect() {
		try {
			if (StringUtil.isNullEmpty(onSelect)) {
				return "";
			} else {
				return " onSelect='" + onSelect + "'";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setOnSelect(String onSelect) {
		this.onSelect = onSelect;
	}

	public String getOnClick() {
		try {
			if (StringUtil.isNullEmpty(onClick)) {
				return "";
			} else {
				return " onClick='" + onClick + "'";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setOnClick(String onClick) {
		this.onClick = onClick;
	}


	public String getStyle() {
		try {
			if (StringUtil.isNullEmpty(style)) {
				return "";
			} else {
				return " style='" + style + "'";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


	public void setStyle(String style) {
		this.style = style;
	}

	public String getPlaceholder() {
		try {
			if (StringUtil.isNullEmpty(placeholder)) {
				return "";
			} else {
				return " placeholder='" + placeholder + "'";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setPlaceholder(String placeholder) {
		this.placeholder = placeholder;
	}

	public String getAll() {
		try {
			if (StringUtil.isNullEmpty(all)) {
				return "";
			} else {
				return " <option value='' selected='selected'>"+all+"</option>";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setAll(String all) {
		this.all = all;
	}


	public String getRequired() {
		try {
			if (StringUtil.isNullEmpty(required)) {
				return "";
			} else {
				return " required ";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setRequired(String required) {
		this.required = required;
	}

}
