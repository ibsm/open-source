package com.hm.common.taglib;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.tagext.Tag;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.web.servlet.tags.RequestContextAwareTag;

import com.hm.common.util.DataUtil.SessionUtil;
import com.hm.common.util.LoginUser;

@SuppressWarnings("serial")
public class FuncPerssionTag extends RequestContextAwareTag {

	private String name;
	
	@Override
	protected int doStartTagInternal() throws Exception {
		try {
			JdbcTemplate jdbcTemplate = (JdbcTemplate)
					 this.getRequestContext().getWebApplicationContext().getBean("jdbcTemplate");
			HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
			LoginUser user = (LoginUser) SessionUtil.getSession(request).getAttribute("_LOGIN_USER_INFO");
			String code = getFuncPerCode(jdbcTemplate, user.getId(), name);
			if(null != code){
				 return Tag.EVAL_BODY_INCLUDE;//显示整个页面的内 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Tag.SKIP_BODY;//SKIP_BODY不显示页面的内容
	}

	public String getFuncPerCode(JdbcTemplate jdbcTemplate,String userId,String code){
		StringBuffer sql = new StringBuffer();
		sql.append("select t.code as code from cmdb_sys_per t where t.code = ? and t.id in (                 ");
		sql.append("	select t1.per_id from cmdb_sys_role_per t1 where t1.role_id in (             ");
		sql.append("		select t2.role_id from cmdb_sys_user_role t2 where t2.user_id = ?        ");
		sql.append("	)                                                                            ");
		sql.append(")                                                                                ");
		
		SqlRowSet rowSet = jdbcTemplate.queryForRowSet(sql.toString(),new Object[]{code,userId});
		while(rowSet.next()){
			return rowSet.getString("code");
		}
		return null;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
