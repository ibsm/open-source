package com.hm.common;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.hm.common.abs.ForwardView;
import com.hm.common.abs.Work;
import com.hm.common.config.Configuration;
import com.hm.common.util.DataUtil.SessionUtil;
import com.hm.common.util.LoginUser;
import com.hm.common.util.R;
import com.hm.sys.service.SysMenuService;
import com.hm.sys.service.SysSessionService;
import com.hm.sys.service.SysUserService;

public abstract class BaseAction {
	
	private static Logger logger = Logger.getLogger(BaseAction.class.getName());
	
	@Resource
	protected SysUserService sysUserService;
	
	@Resource
	protected SysMenuService sysMenuService;
	
	@Resource(name="getSysSessionService")
	protected SysSessionService sysSessionService;
	
	protected HttpServletRequest request;
	
	protected HttpServletResponse response;
	
	@ModelAttribute
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response){  
        this.request = request;  
        this.response = response;  
    }
	
	protected PageSet getPageSet(){
		PageSet pageSet = new PageSet();
		//设置当前页
		pageSet.setPage(Integer.parseInt(request.getParameter("page")));
		pageSet.setPageSize(Integer.parseInt(request.getParameter("rows")));
		pageSet.setSidx(request.getParameter("sidx"));
		return pageSet;
	}
	
	protected void putHtml(String data){
		try {
			response.setCharacterEncoding("UTF-8");
			response.setHeader("content-type","text/html;charset=UTF-8");
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void putJson(Object obj){
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.getWriter().print(JSONObject.fromObject(obj));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void putJsonArray(Object obj){
		try {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.getWriter().print(JSONArray.fromObject(obj));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void sendMsg(Exception e){
		JSONObject obj = new JSONObject();
		obj.put("state", "successful");
		obj.put("msg", "operation successful");
		if(null != e){
			obj.put("state", "exception");
			obj.put("msg", e.getMessage());
		}
		putJson(obj);
	}
	
	protected void doWork(Work work,Integer opType){
		try {
			work.execute(opType);
			sendMsg(null);
		} catch (Exception e) {
			sendMsg(e);
			e.printStackTrace();
		}
	}
	
	protected void doWork(Work work){
		try {
			work.execute();
			sendMsg(null);
		} catch (Exception e) {
			sendMsg(e);
			logger.error(e);
			e.printStackTrace();
		}
	}
	
	protected <T> String forward(ForwardView view,Model model,T entity){
		try {
			String url = view.execute(model,entity);
			return (null != url)?url:"redirect:/error/session.do";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected <T> String forward(ForwardView view,Model model,Integer opType,T entity){
		try {
			String url = view.execute(opType,model,entity);
			return (null != url)?url:"redirect:/error/session.do";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	protected String reqGetData(String data) throws Exception{
		if(null != data){
			return new String(data.getBytes("iso8859-1"),"UTF-8");
		}
		return null;
	}
	
	protected Object reqParameter(String key) {
		return request.getParameter(key);
	}
	
	protected String getLoginUser(){
//		Subject subject=SecurityUtils.getSubject();
		LoginUser user = (LoginUser) SessionUtil.getSession(request).getAttribute("_LOGIN_USER_INFO");
		return user.getId();
	}
	
	protected String reqParamStr(String key){
		return reqParameter(key)+"";
	}
	
	protected void formToken(Map<String,Object> mapper){
		String value = UUID.randomUUID().toString().replace("-", "");
		String key = Configuration.get(R.SystemInit.FORM_TOKEN) + value;
		request.getSession().setAttribute(key, value);
		mapper.put("FORM_TOKEN",key+"="+value);
	}
	
	protected boolean checkFormToken(){
		@SuppressWarnings("rawtypes")
		Enumeration parameterNames = request.getParameterNames();
		while(parameterNames.hasMoreElements()){
			String paramKey = (String) parameterNames.nextElement();
			if(paramKey.startsWith(Configuration.get(R.SystemInit.FORM_TOKEN))){
				Object token = request.getSession().getAttribute(paramKey);
				if(null != token){
					String clientValue = reqParamStr(paramKey);
					if(clientValue.equals(token)){
						request.getSession().removeAttribute(paramKey);
						return true;
					}
				}
				return false;
			}
		}
		return false;
	}
}
