package com.hm.common.util;

public interface R {

	public interface ActionJump{
		public static final int INDEX = 1;
		public static final int ADD = 2;
		public static final int UPDATE = 3;
		public static final int DETAIL = 4;
		public static final int DELETE = 5;
		public static final int DELETE_BATCH = 6;
	}
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe 数据字典类型
	 */
	public interface DictType{
		/**数据权限类别*/
		public static final String PER_TYPE = "1";
		/**流程类型*/
		public static final String FLOW_TYPE = "2";
	}
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe 图表
	 */
	public interface Charts{
		/**图表模板所在根目录*/
		public String ROOT_DIRECTORY = "charts.root.directory";
		/**标准扇形统计图*/
		public String PIE = "charts.pie";
		/**标准环形扇形统计图*/
		public String PIE2 = "charts.pie2";
		/**标准仪表盘*/
		public String GAUGE1 = "charts.gauge1";
	}
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe 二维码
	 */
	public interface QrCode{
		/**高度*/
		public String SIZE = "qrCode.size";
		/**格式*/
		public String FORMAT = "qrCode.format";
		/**编码*/
		public String CODING = "qrCode.coding";
		/**背景色*/
		public int BACKGROUND_COLOR = 0xFF000000;
		/**数据颜色*/
		public int DATA_COLOR = 0xFFFFFFFF;
		/**边框宽度*/
		public String MARGIN = "qrCode.data.margin";
		/**logo位置*/
		public String LOGO_PATH = "qrCode.logo.path";
		/**logo边距*/
		public String LOGO_MARGIN = "qrCode.logo.margin";
		/**logo占二维码的比例*/
		public String LOGO_BELONG_RATIO = "qrCode.logo.belong.ratio";
		/**logo尺寸*/
		public String LOGO_BELONG_SIZE = "qrCode.logo.size";
	}
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe 报表
	 */
	public interface Report{
		/**报表模板所在根目录*/
		public String ROOT_DIRECTORY = "report.root.directory";
	}
	
	/**
	 * @author shishun.wang
	 * @version 1.0
	 * @date 下午4:00:20 2016年2月25日
	 * @statement 
	 * @team 
	 * @email shishun156@gmail.com
	 * @describe 文件配置
	 */
	public interface FileConfig{
		
		/**
		 * @author shishun.wang
		 * @version 1.0
		 * @date 下午4:00:27 2016年2月25日
		 * @statement 
		 * @team 
		 * @email shishun156@gmail.com
		 * @describe 后缀名配置
		 */
		public interface Suffix{
			public String PNG = ".png";
			public String ZIP = ".zip";
		}
	}
	
	/**
	 * @author shishun.wang
	 * @version 1.0
	 * @date 上午9:49:47 2016年2月26日
	 * @statement 
	 * @team 
	 * @email shishun156@gmail.com
	 * @describe 系统初始化数据
	 */
	public interface SystemInit{
		/**form表单防重复提交*/
		public String FORM_TOKEN = "form.submit.token.tag";
		/**单点登录本系统*/
		public String FBSM_SYSTEM_CAS = "fbsm.system.cas";
		/**系统登录用户信息*/
		public String LOGIN_USER_INFO = "fbsm.login.user.info";
	}
}
