package com.hm.common.util;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ImportExcel {

	public void read(InputStream inputStream) throws Exception{
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook(inputStream);
		XSSFSheet sheetAt = xssfWorkbook.getSheetAt(0);
		int index = 0;
		for(int i = sheetAt.getFirstRowNum() ; index < sheetAt.getPhysicalNumberOfRows() ; i++){
			XSSFRow row = sheetAt.getRow(i);
			if(null == row){
				continue;
			}else{
				index ++;
			}
			for(int j = row.getFirstCellNum() ; j <= row.getLastCellNum() ; j++){
				XSSFCell cell = row.getCell(j);
				if(cell == null) continue;
				System.out.println(cell.getCellType());
			}
		}
	}
	
	public void read2(InputStream inputStream,Map<String,Cell> data) throws Exception{
		HSSFWorkbook xssfWorkbook = new HSSFWorkbook(inputStream);
		HSSFSheet sheetAt = xssfWorkbook.getSheetAt(0);
		int index = 0;
		
		Map<Integer,Cell> types = new HashMap<Integer,Cell>();
		
		List<Cell> content = new ArrayList<Cell>();
		
		for(int i = sheetAt.getFirstRowNum() ; index < sheetAt.getPhysicalNumberOfRows() ; i++){
			HSSFRow row = sheetAt.getRow(i);
			if(null == row){
				continue;
			}else{
				index ++;
			}
			if(i == 0){//解析头部
				for(int j = row.getFirstCellNum() ; j <= row.getLastCellNum() ; j++){
					HSSFCell cell = row.getCell(j);
					if(cell == null) continue;
					Cell tmp = data.get(cell.toString());
					types.put(j, tmp);
				}
			}else{
				for(int j = row.getFirstCellNum() ; j <= row.getLastCellNum() ; j++){
					HSSFCell cell = row.getCell(j);
					if(cell == null) continue;
					Cell cell2 = types.get(j);
					cell2.setValue(cell.toString());
					content.add(cell2);
				}
			}
		}
		System.out.println(content);
	}
	
	
	public static void main(String[] args) throws Exception{
		Map<String,Cell> map = new HashMap<String,Cell>();
		{
			Cell cell = new Cell();
			cell.setMethod("id");
			map.put("编号", cell);
		}
		{
			Cell cell = new Cell();
			cell.setMethod("name");
			map.put("名称", cell);
		}
		{
			Cell cell = new Cell();
			cell.setMethod("birthday");
			map.put("生日", cell);
		}
		{
			Cell cell = new Cell();
			cell.setMethod("age");
			map.put("年龄", cell);
		}
		
//		System.out.println(map);
		
		new ImportExcel().read2(ImportExcel.class.getResourceAsStream("/test.xls"),map);;
	}
	
}
class Domain{
	private String id;
	private String name;
	private Date birthday;
	private int age;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

}
class Cell{
	private String method;
	private Object value;
	private String dateFormat;
	private DecimalFormat numberFormat;
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public String getDateFormat() {
		return dateFormat;
	}
	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}
	public DecimalFormat getNumberFormat() {
		return numberFormat;
	}
	public void setNumberFormat(DecimalFormat numberFormat) {
		this.numberFormat = numberFormat;
	}
	@Override
	public String toString() {
		return "Cell [method=" + method + ", value=" + value + ", dateFormat="
				+ dateFormat + ", numberFormat=" + numberFormat + "]";
	}
	
}

