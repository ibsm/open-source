package com.hm.common.util;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 设置excel单元格样式
 */
public class CellNature {

	private String name;
	private String code;
	private String value;
	private int lineWidth = 120;
	private HSSFFont hssfFont;
	private HSSFCellStyle style;
	
	public CellNature simple(String name,String code){
		this.name = name;
		this.code = code;
		return this;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getLineWidth() {
		return lineWidth;
	}

	public void setLineWidth(int lineWidth) {
		this.lineWidth = lineWidth;
	}

	public HSSFFont getHssfFont() {
		return hssfFont;
	}

	public void setHssfFont(HSSFFont hssfFont) {
		this.hssfFont = hssfFont;
	}

	public HSSFCellStyle getStyle() {
		return style;
	}

	public void setStyle(HSSFCellStyle style) {
		this.style = style;
	}
	
}
