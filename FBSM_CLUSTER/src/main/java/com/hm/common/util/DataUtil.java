package com.hm.common.util;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.ImageIcon;

import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.Zip64Mode;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.json.JSONObject;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Binarizer;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 11:52:32 AM Apr 4, 2015
 * @statement Without my written permission, any unit and individual shall not
 *            in any way or reason of the above products, services, information,
 *            and any part of the material to use, copy, modify, transcribing,
 *            spread or with other products bound use and marketing. Hereby
 *            solemnly statement!
 * @team 
 * @email shishun156@gmail.com
 * @describe 
 */
public class DataUtil {

	/**
	 * @author 
	 * @version 1.0
	 * @date 下午9:14:47 2014年8月27日
	 * @statement Without my written permission, any unit and individual shall not
	 *            in any way or reason of the above products, services, information,
	 *            and any part of the material to use, copy, modify, transcribing,
	 *            spread or with other products bound use and marketing. Hereby
	 *            solemnly statement!
	 * @team chasing (personal<author></author>)
	 * @email 
	 * @describe 字符串处理
	 */
	public static class StringUtil{
		
		/**
		 * url编码
		 * @param string
		 * @param charset
		 * @return
		 * @throws Exception
		 */
		public static String urlEncode(String string,String charset) throws Exception{
			if(null != charset && !charset.isEmpty()){
				return URLEncoder.encode(string, charset);
			}
			return string;
		}
		
		/**
		 * url解码
		 * @param string
		 * @param charset
		 * @return
		 * @throws Exception
		 */
		public static String urlDecode(String string,String charset){
			if(null != charset && !charset.isEmpty()){
				try {
					return URLDecoder.decode(string, charset);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			return string;
		}
		
		/**
		 * 字符串是否为空
		 * @param string
		 * @return
		 * @throws Exception
		 */
		public static boolean isEmpty(String string){
			return null == string || 0 == string.trim().length();
		}
		
		/**
		 * 排除null字符串
		 * @param string
		 * @return
		 * @throws Exception
		 */
		public static boolean isNullEmpty(String string){
			return isEmpty(string) || "null".equals(string);
		}
		
		/**
		 * 字符串是否为空
		 * @param string
		 * @return
		 * @throws Exception
		 */
		public static boolean isBlank(String string){
			int len;
			if(null == string || (len = string.length()) == 0){
				return true;
			}else{
				for(int i = 0 ; i < len; i++){
					if(!Character.isWhitespace(string.charAt(i))) return false;
				}
			}
			return true;
		}
		
		/**
		 * 去除批量操作前后特殊分隔符号
		 * @param arg0
		 * @param isStr
		 * @return
		 * @throws Exception
		 */
		public static String dislodgeBatchTagSE(String arg0,boolean isStr){
			if(!isEmpty(arg0)){
				arg0 = dislogeBatchTagStart(arg0);
				arg0 = dislodgeBatchEnd(arg0);
				if(isStr){
					String strs[] = arg0.split(",");
					StringBuffer buffer = new StringBuffer();
					for(String str:strs){
						buffer.append("'"+str+"',");
					}
					arg0 = buffer.toString();
					arg0 = dislodgeBatchEnd(arg0);
				}
			}
			return arg0;
		}
		
		/**
		 * 将ids拆分为String[]
		 * @param arg0
		 * @param isStr
		 * @return
		 * @throws Exception
		 */
		public static String[] dislogeBatchTagSEIds(String arg0,boolean isStr){
			if(!isEmpty(arg0)){
				return dislodgeBatchTagSE(arg0,isStr).split(",");
			}
			return new String[]{};
		}

		/**
		 * @param arg0
		 * @return
		 */
		public static String dislodgeBatchEnd(String arg0){
			if(!isEmpty(arg0)){
				return arg0.endsWith(",")?arg0.substring(0, arg0.length() - 1):arg0;
			}
			return null;
		}

		/**
		 * @param arg0
		 * @return
		 */
		public static String dislogeBatchTagStart(String arg0){
			if(!isEmpty(arg0)){
				return arg0.startsWith(",")?arg0.substring(1, arg0.length()):arg0;
			}
			return null;
		}
		
		/**
		 * isEq true区分大小写false不区分大小写
		 */
		public static boolean equals(String arg0,String arg1,boolean isEq){
			arg0 = arg0 +"";
			arg1=arg1+"";
			if(!isEq){
				arg0 = arg0.toLowerCase();
				arg1 = arg1.toLowerCase();
			}
			return arg0.equals(arg1);
		}
	
		/**
		 * 首字母大写
		 * @param arg0
		 * @return
		 */
		public static String capitalize(String arg0){
			char[] chars = arg0.toCharArray();
			if(chars[0] >= 'a' && chars[0] <= 'z'){
				chars[0] -= 'a' - 'A';
			}
			return new String(chars);
		}
		
	}
	
	/**
	 * @author 
	 * @version 1.0
	 * @date 下午9:15:41 2014年8月27日
	 * @statement Without my written permission, any unit and individual shall not
	 *            in any way or reason of the above products, services, information,
	 *            and any part of the material to use, copy, modify, transcribing,
	 *            spread or with other products bound use and marketing. Hereby
	 *            solemnly statement!
	 * @team chasing (personal<author></author>)
	 * @email 
	 * @describe 日期处理
	 */
	public static class DateUtil{
		
		public static String date2str(Date date,String format){
			SimpleDateFormat dateFormat = new SimpleDateFormat(format);
			return dateFormat.format(date);
		}
		
		public static String yyyyMMdd(Date date){
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			if(null != date){
				return dateFormat.format(date);
			}
			return null;
		}
		
		public static String yyyyMMddhhmm(Date date){
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			if(null != date){
				return dateFormat.format(date);
			}
			return null;
		}
	}
	
	/**
	 * @author 
	 * @version 1.0
	 * @date 下午11:50:49 2014年8月31日
	 * @statement Without my written permission, any unit and individual shall not
	 *            in any way or reason of the above products, services, information,
	 *            and any part of the material to use, copy, modify, transcribing,
	 *            spread or with other products bound use and marketing. Hereby
	 *            solemnly statement!
	 * @team chasing (personal<author></author>)
	 * @email 
	 * @describe 
	 */
	public static class ClientUtil{
		
		public static String getRemoteAddress(HttpServletRequest request) throws Exception{
			String ip = request.getHeader("x-forwarded-for");
			if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
	            ip = request.getRemoteAddr();
	        }
	        return ip;
	    }
		
		public static String getMAC(HttpServletRequest request) throws Exception{
			return getMACAddress(getRemoteAddress(request));
		}
		
		public static String getMACAddress(String ip) throws Exception{
	        String str = "";
	        String macAddress = "";
//	        Process p = Runtime.getRuntime().exec("nbtstat -a " + ip);
	        Process p = Runtime.getRuntime().exec("cmd /c C:\\Windows\\sysnative\\nbtstat.exe -a " + ip);
            InputStreamReader ir = new InputStreamReader(p.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            for (int i = 1; i < 100; i++) {
                str = input.readLine();
                if (str != null) {
                    if (str.indexOf("MAC") > 1) {
                        macAddress = str.substring(str.indexOf("=") + 2, str.length());
                        break;
                    }
                }
            }
	        return macAddress;
	    }
	}
	
	/**
	 * @author 
	 * @version 1.0
	 * @date 上午12:26:00 2014年8月31日
	 * @statement Without my written permission, any unit and individual shall not
	 *            in any way or reason of the above products, services, information,
	 *            and any part of the material to use, copy, modify, transcribing,
	 *            spread or with other products bound use and marketing. Hereby
	 *            solemnly statement!
	 * @team chasing (personal<author></author>)
	 * @email 
	 * @describe 
	 */
	public static class SessionUtil{
		
		public static String webKey(HttpServletRequest request){
			String key = UUID.randomUUID().toString();
			getSession(request).setAttribute("webKey", key);
			return key;
		}
		
		public static String getWebKey(HttpServletRequest request){
			return (String) getSession(request).getAttribute("webKey");
		}
		
		public static void bindWebKey(String keyCode,HttpServletRequest request){
			getSession(request).setAttribute("webKey", keyCode);
		}
		
		public static HttpSession getSession(HttpServletRequest request){
			HttpSession session = request.getSession();
			session.setMaxInactiveInterval(60*15);//超时时间5分钟
			return session;
		}
	}
	
	/**
	 * @author shishun.wang
	 * @version 1.0
	 * @date 11:54:45 AM Apr 4, 2015
	 * @statement Without my written permission, any unit and individual shall not
	 *            in any way or reason of the above products, services, information,
	 *            and any part of the material to use, copy, modify, transcribing,
	 *            spread or with other products bound use and marketing. Hereby
	 *            solemnly statement!
	 * @team 
	 * @email shishun156@gmail.com
	 * @describe 
	 */
	public static class KeyUtil{
		
		public static String key() throws Exception{
			String key = java.util.UUID.randomUUID().toString();
			return key;
		}
	}
	
	/**
	 * @author 
	 * @version 1.0
	 * @date 10:04:35 AM Feb 23, 2015
	 * @statement Without my written permission, any unit and individual shall not
	 *            in any way or reason of the above products, services, information,
	 *            and any part of the material to use, copy, modify, transcribing,
	 *            spread or with other products bound use and marketing. Hereby
	 *            solemnly statement!
	 * @team bbsm
	 * @email 
	 * @describe web验证 
	 */
	public static class WebValidation{
		
		/**
		 * 客户端输出验证码
		 */
		public static void vaCode(HttpServletRequest request,HttpServletResponse response) throws Exception {
			int width = 60, height = 20;
			//设置长宽高度自定义
			String w = request.getParameter("w"),h = request.getParameter("h");
			if(!StringUtil.isNullEmpty(w)){
				width = Integer.parseInt(w);
			}
			if(!StringUtil.isNullEmpty(h)){
				height = Integer.parseInt(h);
			}
			HttpSession session = request.getSession();
			BufferedImage image = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_RGB);

			// 获取图形上下文
			Graphics g = image.getGraphics();

			// 设定背景色
			g.setColor(new Color(0xDCDCDC));
			g.fillRect(0, 0, width, height);

			// 画边框
			g.setColor(Color.black);
			g.drawRect(0, 0, width - 1, height - 1);

			// 取随机产生的认证码(4位数字)
			String rand = getStringRandom(4);
			// 将认证码存入SESSION
			session.setAttribute("WEB_VA_CODE", rand);

			// 将认证码显示到图象中
			g.setColor(Color.black);

			g.setFont(new Font("Atlantic Inline", Font.PLAIN, 18));
			String Str = rand.substring(0, 1);
			g.drawString(Str, 8, 17);

			Str = rand.substring(1, 2);
			g.drawString(Str, 20, 15);
			Str = rand.substring(2, 3);
			g.drawString(Str, 35, 18);

			Str = rand.substring(3, 4);
			g.drawString(Str, 45, 15);

			Random random = new Random();
			for (int i = 0; i < 100; i++) {
				int x = random.nextInt(width);
				int y = random.nextInt(height);
				g.drawOval(x, y, 0, 0);
			}

			// 图象生效
			g.dispose();

			// 输出图象到页面
			ImageIO.write(image, "JPEG", response.getOutputStream());
		}
		
		/**
		 * @code 验证码
		 * return true:验证通过；false:验证没通过
		 */
		public static boolean checkVaCode(HttpServletRequest request,String code) throws Exception{
			HttpSession session = request.getSession(false);
			session.setMaxInactiveInterval(60*5);//超时时间5分钟
			if(null != session){
				String tag = session.getAttribute("WEB_VA_CODE")+"";
				if(StringUtil.equals(tag, code, false)){
					return true;
				}
			}
			return false;
		}
		
		/**
		 * 获取随机字母加数字
		 */
		public static String getStringRandom(int length) throws Exception {
			String val = "";
			Random random = new Random();
			// 参数length，表示生成几位随机数
			for (int i = 0; i < length; i++) {
				String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
				// 输出字母还是数字
				if ("char".equalsIgnoreCase(charOrNum)) {
					// 输出是大写字母还是小写字母
					int temp = random.nextInt(2) % 2 == 0 ? 65 : 97;
					val += (char) (random.nextInt(26) + temp);
				} else if ("num".equalsIgnoreCase(charOrNum)) {
					val += String.valueOf(random.nextInt(10));
				}
			}
			return val;
		}
	}
	
	/**
	 * @author shishun.wang
	 * @version 1.0
	 * @date 11:54:01 AM Apr 4, 2015
	 * @statement Without my written permission, any unit and individual shall not
	 *            in any way or reason of the above products, services, information,
	 *            and any part of the material to use, copy, modify, transcribing,
	 *            spread or with other products bound use and marketing. Hereby
	 *            solemnly statement!
	 * @team 
	 * @email shishun156@gmail.com
	 * @describe 
	 */
	public static class FileUtil{
		
		public static String generationUpdateFilePath() throws Exception{
			return generationNewFilePath("_uploadFile");
		}
		
		public static String generationDownloadFilePath() throws Exception{
			return generationNewFilePath("_downloadFile");
		}
		
		public static String generationNewFilePath(String tag) throws Exception{
			StringBuffer buffer = new StringBuffer(tag);
			Calendar cal=Calendar.getInstance();
			buffer.append("/"+cal.get(Calendar.YEAR));
			buffer.append("/"+(cal.get(Calendar.MONTH) + 1));
			buffer.append("/"+cal.get(Calendar.YEAR)+"-"+(cal.get(Calendar.MONTH) + 1)+"-"+cal.get(Calendar.DAY_OF_MONTH));
			return buffer.toString();
		}
		
		 public static String getExtensionName(String filename) { 
	        if ((filename != null) && (filename.length() > 0)) { 
	            int dot = filename.lastIndexOf('.'); 
	            if ((dot >-1) && (dot < (filename.length() - 1))) { 
	                return filename.substring(dot + 1); 
	            } 
	        } 
	        return filename; 
	    } 
	    
	    public static String read(String url) throws Exception{
			FileInputStream fileInputStream = null;
			InputStreamReader inputStreamReader = null;
			BufferedReader bufferedReader = null;
			try {
				exists(url, false);
				fileInputStream = new FileInputStream(url);
				inputStreamReader = new InputStreamReader(fileInputStream,"UTF-8");
				bufferedReader = new BufferedReader(inputStreamReader);
				StringBuffer buffer = new StringBuffer("");
				String temp = null;
				while (null != (temp = bufferedReader.readLine())) {
					buffer.append(temp+"\n");
				}
				return buffer.toString();
			} finally{
				if(null != bufferedReader) bufferedReader.close();
				if(null != inputStreamReader) inputStreamReader.close();
				if(null != fileInputStream) fileInputStream.close();
			}
		}
		
		public static java.io.File exists(String url,boolean isMkdir) throws Exception{
			File  file = new File(url);
			if(!file.exists()){
				if(!isMkdir) throw new FileNotFoundException(url);
				file.mkdirs();
			}
			return file;
		}
		
		public static void download(String path,String fileName,HttpServletResponse response) throws Exception{
			InputStream inputStream = new BufferedInputStream(new FileInputStream(path));
			byte[] buffer = new byte[inputStream.available()];
			inputStream.read(buffer);
			inputStream.close();
			
			File file = new File(path);
			response.reset();
			// 设置response的Header
			response.addHeader("Content-Disposition", "attachment;filename="+ fileName);
			response.addHeader("Content-Length", "" + file.length());
			OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
//			response.setContentType("application/vnd.ms-excel;charset=utf-8");
			response.setContentType("binary/octet-stream");
			outputStream.write(buffer );
			outputStream.flush();
			outputStream.close();
		}
		
		public static String getDownloadPath(String suffix) throws Exception{
			String path = FileUtil.generationDownloadFilePath();
			{//判断文件夹存在否
				FileUtil.exists(com.hm.common.config.Configuration.downloadPath() +"/"+ path, true);
			}
			path += "/"+UUID.randomUUID()+suffix;
			return com.hm.common.config.Configuration.downloadPath() +"/" + path;
		}
		
		public static String getDownloadPath(String fileName,String suffix) throws Exception{
			String path = FileUtil.generationDownloadFilePath();
			{//判断文件夹存在否
				FileUtil.exists( com.hm.common.config.Configuration.downloadPath() +"/"+ path, true);
			}
			path += "/"+fileName + suffix;
			return com.hm.common.config.Configuration.downloadPath() +"/" + path;
		}
	}
	
	/**
	 * @author shishun.wang
	 * @version 1.0
	 * @date 11:54:06 AM Apr 4, 2015
	 * @statement Without my written permission, any unit and individual shall not
	 *            in any way or reason of the above products, services, information,
	 *            and any part of the material to use, copy, modify, transcribing,
	 *            spread or with other products bound use and marketing. Hereby
	 *            solemnly statement!
	 * @team 
	 * @email shishun156@gmail.com
	 * @describe 
	 */
	public static class SerializationUtil{
		
		@SuppressWarnings("deprecation")
		public static Object fromJsonToJava(JSONObject json,Class<?> pojo) throws Exception{
	        // 首先得到pojo所定义的字段
	        Field [] fields = pojo.getDeclaredFields();
	        // 根据传入的Class动态生成pojo对象
	        Object obj = pojo.newInstance();
	        for(Field field: fields){
	            // 设置字段可访问（必须，否则报错）
	            field.setAccessible(true);
	            // 得到字段的属性名
	            String name = field.getName();
	            // 这一段的作用是如果字段在JSONObject中不存在会抛出异常，如果出异常，则跳过。
	            try{
	                    json.get(name);
	            }catch(Exception ex){
	                continue;
	            }
	            if(json.get(name) != null && !"".equals(json.getString(name))){
	                // 根据字段的类型将值转化为相应的类型，并设置到生成的对象中。
	                if(field.getType().equals(Long.class) || field.getType().equals(long.class)){
	                    field.set(obj, Long.parseLong(json.getString(name)));
	                }else if(field.getType().equals(String.class)){
	                    field.set(obj, json.getString(name));
	                } else if(field.getType().equals(Double.class) || field.getType().equals(double.class)){
	                    field.set(obj, Double.parseDouble(json.getString(name)));
	                } else if(field.getType().equals(Integer.class) || field.getType().equals(int.class)){
	                	String num = json.getString(name);
	                	if(-1 != num.indexOf(".")){
	                		String values[] = num.split(".");
	                		field.set(obj, Integer.parseInt(values[0]));
	                	}else{
	                		if(!StringUtil.isNullEmpty(num)){
	                			field.set(obj, Integer.parseInt(num));
	                		}else{
	                			field.set(obj, 0);
	                		}
	                	}
	                } else if(field.getType().equals(java.util.Date.class)){
	                    field.set(obj, Date.parse(json.getString(name)));
	                }else{
	                    continue;
	                }
	            }
	        }
	        return obj;
	    }
	}
	
	/**
	 * @author shishun.wang
	 * @version 1.0
	 * @date 8:11:25 PM Apr 5, 2015
	 * @statement Without my written permission, any unit and individual shall not
	 *            in any way or reason of the above products, services, information,
	 *            and any part of the material to use, copy, modify, transcribing,
	 *            spread or with other products bound use and marketing. Hereby
	 *            solemnly statement!
	 * @team 
	 * @email shishun156@gmail.com
	 * @describe 
	 */
	public interface DateFormat{
		public static String yyyyMMdd = "yyyy-MM-dd";
	}
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe 模板引擎引入
	 */
	public static class FtlFactory{
		
		private Template template;
		
		//默认模板存放位置
		private String rootPath = com.hm.common.config.Configuration.get(R.Charts.ROOT_DIRECTORY);
		
		public FtlFactory rootDirectory(String rootDirectory){
			this.rootPath = rootDirectory;
			return this;
		}
		
		public FtlFactory build(String fileName) throws Exception{
			Configuration configuration = new Configuration();
			configuration.setClassForTemplateLoading(DataUtil.class, rootPath);
			configuration.setDefaultEncoding("UTF-8");
			this.template = configuration.getTemplate(fileName);
			return this;
		}
		
		public String process(Map<String,String> mapper) throws Exception{
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			Writer out = new OutputStreamWriter(byteStream);
			this.template.process(mapper, out);
			
			return new String(byteStream.toByteArray());
		}
		
		public void process(Map<String,String> mapper,Writer out) throws Exception{
			this.template.process(mapper, out);
		}
		
	}
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe 容量转换
	 */
	public static class Decimal{
		
		public static float k2kb(float arg0){
			return (float)(arg0/1024);
		}
		
		public static float kb2mb(float arg0){
			return (float)(arg0/1024);
		}
		
		public static float mb2gb(float arg0){
			return (float)(arg0/1024);
		}
		
		public static float k2mb(float arg0){
			return kb2mb(k2kb(arg0));
		}
		
		public static float k2gb(float arg0){
			return mb2gb(k2mb(arg0));
		}
		
		public static float kb2gb(float arg0){
			return mb2gb(kb2mb(arg0));
		}
		
		public static String decimal2(float arg0) {
			java.text.DecimalFormat format = new java.text.DecimalFormat("0.00");
			return format.format(arg0);
		}
	}
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe 
	 */
	public static class Client{
		
		public static String getPackHost(HttpServletRequest request){
			String ip = getHost(request);
			return ("0:0:0:0:0:0:0:1".equals(ip))?"127.0.0.1":ip;
		}
		
		public static String getHost(HttpServletRequest request) {
	        String ip = request.getHeader("X-Forwarded-For");
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	            ip = request.getHeader("Proxy-Client-IP");
	        }
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	            ip = request.getHeader("WL-Proxy-Client-IP");
	        }
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	            ip = request.getHeader("HTTP_CLIENT_IP");
	        }
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
	        }
	        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
	            ip = request.getRemoteAddr();
	        }
	        return ip;
	    }  
		
	}
	
	/**
	 * @author shishun.wang
	 * @version 1.0
	 * @date 下午3:05:18 2016年2月14日
	 * @statement 
	 * @team 
	 * @email shishun156@gmail.com
	 * @describe 图片处理 
	 */
	public static class ImageUtil{
		
		/**
		 * 调整大小
		 * @param image 目标图片
		 * @param size 将图片缩放到目标尺寸
		 * @return
		 * @throws Exception
		 */
		public static BufferedImage resize(BufferedImage image, int size)
				throws Exception {
			Image resizedImage = null;
			int iWidth = image.getWidth(null);
			int iHeight = image.getHeight(null);
			if (iWidth > iHeight) {
				resizedImage = image.getScaledInstance(size, (size * iHeight)
						/ iWidth, Image.SCALE_SMOOTH);
			} else {
				resizedImage = image.getScaledInstance((size * iWidth)
						/ iHeight, size, Image.SCALE_SMOOTH);
			}
			// This code ensures that all the pixels in the image are loaded.
			Image temp = new ImageIcon(resizedImage).getImage();

			// Create the buffered image.
			BufferedImage bufferedImage = new BufferedImage(
					temp.getWidth(null), temp.getHeight(null),
					BufferedImage.TYPE_INT_RGB);

			// Copy image to buffered image.
			Graphics g = bufferedImage.createGraphics();

			// Clear background and paint the image.
			g.setColor(Color.white);
			g.fillRect(0, 0, temp.getWidth(null), temp.getHeight(null));
			g.drawImage(temp, 0, 0, null);
			g.dispose();

			// Soften.
			float softenFactor = 0.05f;
			float[] softenArray = { 0, softenFactor, 0, softenFactor,
					1 - (softenFactor * 4), softenFactor, 0, softenFactor, 0 };
			Kernel kernel = new Kernel(3, 3, softenArray);
			ConvolveOp cOp = new ConvolveOp(kernel, ConvolveOp.EDGE_NO_OP, null);
			bufferedImage = cOp.filter(bufferedImage, null);

			return bufferedImage;
		}
		
	}
	
	/**
	 * @author shishun.wang
	 * @version 1.0
	 * @date 下午3:01:55 2016年2月25日
	 * @statement 
	 * @team 
	 * @email shishun156@gmail.com
	 * @describe zip文件压缩解压 
	 */
	public static class ZipUtil{
		
		/**
		 * zip 文件打包适用于所有
		 * @param files
		 * @param filename
		 * @return
		 */
		public static File compressFilesZip(List<File> files, String filename) {
			File zipfile = new File(filename);
			ZipArchiveOutputStream zipOutput = null; 
			try {
	            zipOutput = (ZipArchiveOutputStream) new ArchiveStreamFactory().createArchiveOutputStream(ArchiveStreamFactory.ZIP, new FileOutputStream(zipfile));  
	            zipOutput.setEncoding("UTF-8");  
	            zipOutput.setUseZip64(Zip64Mode.AsNeeded);  
	            for(File file : files){  
	                InputStream in = null;  
	                try {  
	                    in = new FileInputStream(file);  
	                    ZipArchiveEntry entry = new ZipArchiveEntry(file, file.getName());//zipOutput.createArchiveEntry(logFile, logFile.getName());  
	                    zipOutput.putArchiveEntry(entry);  
	                    IOUtils.copy(in, zipOutput);  
	                    zipOutput.closeArchiveEntry();  
	                }finally{  
	                    if(in != null){  
	                        try {  
	                            in.close();  
	                        } catch (Exception e) { }  
	                    }  
	                }  
	            }  
	            zipOutput.finish();  
	            zipOutput.close(); 
			} catch (Exception e) {
				e.printStackTrace();
			} finally{
				if(zipOutput != null){  
	                try {  
	                    zipOutput.close();  
	                } catch (Exception e1) {
	                	System.out.println(e1);
	                }  
	            } 
			}
			return zipfile;
		}
		
		/**
		 * 该压缩只支持jdk1.7及以上
		 * @param files
		 * @param filename
		 * @return
		 */
		public static File zip(List<File> files, String filename) {
		    File zipfile = new File(filename);
		    // Create a buffer for reading the files
		    byte[] buf = new byte[1024];
		    try {
		        // create the ZIP file
		        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
		        // compress the files
		        for(int i=0; i<files.size(); i++) {
		            FileInputStream in = new FileInputStream(files.get(i).getCanonicalPath());
		            // add ZIP entry to output stream
		            out.putNextEntry(new ZipEntry(files.get(i).getName()));
		            // transfer bytes from the file to the ZIP file
		            int len;
		            while((len = in.read(buf)) > 0) {
		                out.write(buf, 0, len);
		            }
		            // complete the entry
		            out.closeEntry();
		            in.close();
		        }
		        // complete the ZIP file
		        out.close();
		        return zipfile;
		    } catch (IOException ex) {
		        System.err.println(ex.getMessage());
		    }
		    return null;
		}
	}
	
	/**
	 * @author shishun.wang
	 * @date 10:44:31 AM Dec 11, 2015
	 * @version 1.0
	 * @describe 二维码 
	 */
	public static class QrCodeUtil{
		
		private Map<EncodeHintType,Object> encodeHints = new HashMap<EncodeHintType, Object>();
		{//生成二维码参数设置
			// 设置QR二维码的纠错级别（H为最高级别）具体级别信息,容错越高黑点越多
			encodeHints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
			encodeHints.put(EncodeHintType.CHARACTER_SET, com.hm.common.config.Configuration.get(R.QrCode.CODING));
			encodeHints.put(EncodeHintType.MARGIN, 0);
			encodeHints.put(EncodeHintType.MAX_SIZE, com.hm.common.config.Configuration.get(R.QrCode.SIZE));
			encodeHints.put(EncodeHintType.MIN_SIZE, com.hm.common.config.Configuration.get(R.QrCode.SIZE));
		}
		
		/**
		 * 二维码logo尺寸
		 */
		private int logSize = com.hm.common.config.Configuration.getInt(R.QrCode.LOGO_BELONG_SIZE);
		
		/**
		 * 二维码尺寸
		 */
		private int size = com.hm.common.config.Configuration.getInt(R.QrCode.SIZE);
		
		private Map<DecodeHintType,Object> decodeHints = new HashMap<DecodeHintType, Object>();
		
		public QrCodeUtil putEncodeHint(EncodeHintType key,Object value){
			encodeHints.put(key, value);
			return this;
		}
		
		public QrCodeUtil putDecodeHint(DecodeHintType key,Object value){
			decodeHints.put(key, value);
			return this;
		}
		
		public QrCodeUtil setLogSize(int size){
			this.logSize = size;
			return this;
		}
		
		public QrCodeUtil setSize(int size){
			this.size = size;
			return this;
		}
		
		/**
		 * @param content 内容
		 * @param size 尺寸
		 * @return
		 */
		public BufferedImage getEncodeImage(String content) throws  Exception{
			MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
			// 参数顺序分别为：编码内容，编码类型，生成图片宽度，生成图片高度，设置参数
			BitMatrix bm = multiFormatWriter.encode(content, BarcodeFormat.QR_CODE, size,size, encodeHints);

			int w = bm.getWidth();
			int h = bm.getHeight();
			BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);

			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					image.setRGB(x, y, bm.get(x, y) ? R.QrCode.BACKGROUND_COLOR : R.QrCode.DATA_COLOR);
				}
			}
			
			{//二维码尺寸处理
				int[] rec = bm.getEnclosingRectangle();
				int padding = rec[0];
				int realWidth = rec[3];
				int margin = com.hm.common.config.Configuration.getInt(R.QrCode.MARGIN);
				if(realWidth < size && padding > 0 && (padding - margin > 0)){//图片截取(获取二维码图片真实尺寸)
					//裁剪图片去除空白区域
					image = tailoringMargin(image, padding, realWidth,margin);
					//图片放大，放大到指定尺寸大小
					return ImageUtil.resize(image, size);
				}
			}
			
			return image;
		}
		
		/**
		 * 裁剪图片去除生成二维码边距
		 * @param image
		 * @param padding
		 * @param size
		 * @return
		 * @throws Exception
		 */
		public BufferedImage tailoringMargin(BufferedImage image,int padding,int realSize,int margin) throws Exception{
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			String format = com.hm.common.config.Configuration.get(R.QrCode.FORMAT);
			ImageIO.write(image, format, out);
			image.flush();
			
			InputStream source = new ByteArrayInputStream(out.toByteArray());
			
			//读取图片流
			@SuppressWarnings("rawtypes")
			Iterator readers = ImageIO.getImageReadersByFormatName(format);
			ImageReader reader = (ImageReader) readers.next();
			ImageInputStream inputStream = ImageIO.createImageInputStream(source);
			reader.setInput(inputStream, true);
			// 图片参数
			ImageReadParam param = reader.getDefaultReadParam();
			// 100，200是左上起始位置，300就是取宽度为300的，就是从100开始取300宽，就是横向100~400，同理纵向200~350的区域就取高度150
			// Rectangle rect = new Rectangle(100, 200, 300, 150);//
			Rectangle rect = new Rectangle(padding - margin, padding - margin, realSize + 2*margin, realSize + 2*margin);
			param.setSourceRegion(rect);
			
			return reader.read(0, param);
		}
		
		/**
		 * 生成二维码
		 * @param content
		 * @param stream
		 * @throws Exception
		 */
		public void encode(String content,OutputStream stream) throws Exception{
			String format = com.hm.common.config.Configuration.get(R.QrCode.FORMAT);
			ImageIO.write(getEncodeImage(content), format, stream);
		}
		
		/**
		 * 生成二维码
		 * @param content
		 * @param logo
		 * @param stream
		 * @param logoBorderColor
		 * @throws Exception
		 */
		public void encode(String content,BufferedImage logo,OutputStream stream,Color logoBorderColor) throws Exception{
			String format = com.hm.common.config.Configuration.get(R.QrCode.FORMAT);
			
			BufferedImage image = getEncodeImage(content);
			
			Graphics2D g = image.createGraphics();
			//设置logo的大小为二维码图片的20%,因为过大会盖掉二维码
			int belongRatio = com.hm.common.config.Configuration.getInt(R.QrCode.LOGO_BELONG_RATIO);
			int widthLogo = logo.getWidth(null) > image.getWidth() * belongRatio / 10 ? (image
					.getWidth() * belongRatio / 10) : logo.getWidth(null), heightLogo = logo
					.getHeight(null) > image.getHeight() * belongRatio / 10 ? (image
					.getHeight() * belongRatio / 10) : logo.getWidth(null);
			// 计算logo图片放置位置
			int x = (image.getWidth() - widthLogo) / 2; 
			int y = (image.getHeight() - heightLogo) / 2;
			g.drawImage(logo, x, y, widthLogo, heightLogo, null);
			g.drawRoundRect(x, y, widthLogo, heightLogo, logSize, logSize);
			
			g.setStroke(new BasicStroke(com.hm.common.config.Configuration.getInt(R.QrCode.LOGO_MARGIN)));
			g.setColor(logoBorderColor);
			g.drawRect(x, y, widthLogo, heightLogo);

			g.dispose();
			logo.flush();
			image.flush();
			
			ImageIO.write(image, format, stream);
		}

		/**
		 * 解析二维码
		 * @param input
		 * @return
		 * @throws Exception
		 */
		public String decode(File input) throws Exception{
			BufferedImage image = ImageIO.read(input);
			LuminanceSource source = new BufferedImageLuminanceSource(image);
			Binarizer binarizer = new HybridBinarizer(source);
			BinaryBitmap binaryBitmap = new BinaryBitmap(binarizer);
			
			return new MultiFormatReader().decode(binaryBitmap, decodeHints).getText();
		}
		
	}
}
