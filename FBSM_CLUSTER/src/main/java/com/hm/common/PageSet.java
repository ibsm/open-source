package com.hm.common;

import java.util.List;

public class PageSet {

	private int page;
	
	private int total;
	
	private int records;
	
	private int pageSize;
	
	//以什么排序
	private String sidx;
	
	private List<?> rows;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
		execute();//查询数据总条数触发分页
	}

	public List<?> getRows() {
		return rows;
	}

	public void setRows(List<?> rows) {
		this.rows = rows;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void execute(){
		this.total = this.records/this.pageSize;
		if(this.records%this.pageSize != 0){
			this.total ++;
		}
	}

	public String getSidx() {
		return sidx;
	}

	public void setSidx(String sidx) {
		this.sidx = sidx;
	}

	@Override
	public String toString() {
		return "PageSet [page=" + page + ", total=" + total + ", records="
				+ records + ", pageSize=" + pageSize + ", sidx=" + sidx
				+ ", rows=" + rows + "]";
	}
}
