package com.hm.common.handler;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.hm.sys.dao.CmdbSysPerMapper;
import com.hm.sys.dao.CmdbSysRoleMapper;
import com.hm.sys.dao.CmdbSysRolePerMapper;
import com.hm.sys.dao.CmdbSysUserMapper;
import com.hm.sys.dao.CmdbSysUserRoleMapper;
import com.hm.sys.entity.CmdbSysPer;
import com.hm.sys.entity.CmdbSysRolePer;
import com.hm.sys.entity.CmdbSysUser;
import com.hm.sys.entity.CmdbSysUserRole;

/**
 * @author shishun.wang
 * @time 2015年12月29日 下午4:18:48
 * @version 1.0
 * @description 数据安全验证
 */
public class AuthorizingRealmHandler extends AuthorizingRealm{

	@Autowired
	private CmdbSysUserMapper sysUserMapper;
	
	@Autowired
	private CmdbSysRoleMapper sysRoleMapper;
	
	@Autowired
	private CmdbSysUserRoleMapper sysUserRoleMapper;
	
	@Autowired
	private CmdbSysRolePerMapper sysRolePerMapper;
	
	@Autowired
	private CmdbSysPerMapper sysPerMapper;
	
	/**
	 * 为当前登录的用户授予角色和权限 
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
		//String account = (String) principal.getPrimaryPrincipal();
		CmdbSysUser sysUser = (CmdbSysUser) SecurityUtils.getSubject().getSession().getAttribute("LOGIN_USER_INFO");
		SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
		if(null != sysUser){
			List<CmdbSysUserRole> allRoles = sysUserRoleMapper.getUserAllRoles(sysUser.getId());
//			Set<String> roles = new HashSet<String>();
			Set<String> pres = new HashSet<String>();
			for (CmdbSysUserRole role : allRoles) {
				List<CmdbSysRolePer> roleAllPres = sysRolePerMapper.getRoleAllPres(role.getRoleId());
				for (CmdbSysRolePer per : roleAllPres) {//权限
					CmdbSysPer sysPer = sysPerMapper.selectByPrimaryKey(per.getPerId());
					pres.add(sysPer.getCode());
				}
				//角色
//				CmdbSysRole sysRole = sysRoleMapper.selectByPrimaryKey(role.getRoleId());
//				roles.add(sysRole.getName());
			}
//			authorizationInfo.setRoles(roles);
			authorizationInfo.setStringPermissions(pres);
		}
		
		return authorizationInfo;
	}

	/**
	 * 验证当前登陆用户 
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		String account = (String) token.getPrincipal();
		CmdbSysUser sysUser = sysUserMapper.selectByAccount(account);
		if(null != sysUser){
			{//将用户信息放入到session中
				SecurityUtils.getSubject().getSession().setAttribute("LOGIN_USER_INFO", sysUser);
			}
			AuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(sysUser.getName(), sysUser.getPwd(), "realmName");
			return authenticationInfo;
		}
		return null;
	}

}
