package com.hm.common.handler;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.hm.common.config.Configuration;
import com.hm.common.util.DataUtil.SessionUtil;
import com.hm.common.util.LoginUser;
import com.hm.common.util.R;
import com.hm.sys.entity.CmdbSysSession;
import com.hm.sys.service.SysSessionService;


/**
 * @author shishun.wang
 * @version 1.0
 * @date 12:30:18 AM May 5, 2015
 * @statement Without my written permission, any unit and individual shall not
 *            in any way or reason of the above products, services, information,
 *            and any part of the material to use, copy, modify, transcribing,
 *            spread or with other products bound use and marketing. Hereby
 *            solemnly statement!
 * @team 
 * @email shishun156@gmail.com
 * @describe 
 */
public class SessionHandlerInterceptor implements HandlerInterceptor{

	@Resource(name="getSysSessionService")
	protected SysSessionService sysSessionService;
	
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		// TODO Auto-generated method stub
		String url = request.getRequestURI();
		boolean tag = ("/fbsm/error/drops.do".equals(url) || "/fbsm/error/session.do".equals(url) || "/fbsm/check.do".equals(url) || "/fbsm/login.do".equals(url) || "/fbsm/".equals(url) || "/fbsm/index.do".equals(url));
		if(!tag){
			LoginUser loginUser = (LoginUser) SessionUtil.getSession(request).getAttribute("_LOGIN_USER_INFO");
			if(null == loginUser){
				response.sendRedirect("/fbsm/error/session.do");
				return false;
			}
			
			//判断是否单点登录
			boolean openCas = Configuration.getBoolean(R.SystemInit.FBSM_SYSTEM_CAS);
			if(openCas){
				CmdbSysSession sysSession = sysSessionService.selectByUserId(loginUser.getId());
				if(sysSession.getVersion().longValue() != loginUser.getVersion().longValue()){//当前登录用户已经被人挤下线
					//清除当前登录用户所有登录信息
					SessionUtil.getSession(request).removeAttribute("_LOGIN_USER_INFO");
					
//					response.sendRedirect("/fbsm/error/session.do");
					response.sendRedirect("/fbsm/error/drops.do");
					return false;
				}
			}
			
		}
		return true;
	}

	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
