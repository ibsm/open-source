package com.hm.common.handler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * @author shishun.wang
 * @date 9:56:43 AM Jan 1, 2016
 * @version 1.0
 * @describe 
 */
@Aspect
@Component
public class ActionAspectHandler {
	
	static Logger LOG = Logger.getLogger(ActionAspectHandler.class.getName());
	
	@Around("execution(* com.hm.*.action.*Action.save*(..))")
    public void process(ProceedingJoinPoint point) throws Throwable {
		HttpServletResponse response = (HttpServletResponse) point.getArgs()[1];
		try {
        	point.proceed();
        	sendMsg(response);
		} catch (Exception e) {
			sendMsg((HttpServletRequest) point.getArgs()[0],response,e);
			LOG.error(e,e);
			e.printStackTrace();
		}
    } 
	
	private void putJson(HttpServletResponse response,Object obj){
		try {
			response.setCharacterEncoding("UTF-8");
			response.getWriter().print(JSONObject.fromObject(obj));
		} catch (IOException e) {
			LOG.error(e,e);
			e.printStackTrace();
		}
	}
	
	private void sendMsg(HttpServletRequest request,HttpServletResponse response,Exception e){
		JSONObject obj = new JSONObject();
		obj.put("state", "exception");
		String msg = request.getParameter("msg");
		try {
			obj.put("msg", null!=msg?new String(msg.getBytes("iso8859-1"),"UTF-8"):e.getMessage());
		} catch (UnsupportedEncodingException e1) {
			LOG.error(e1,e1);
			e1.printStackTrace();
		}
		putJson(response,obj);
	}
	
	private void sendMsg(HttpServletResponse response){
		JSONObject obj = new JSONObject();
		obj.put("state", "successful");
		obj.put("msg", "operation successful");
		putJson(response,obj);
	}
}
