package com.hm.common.handler;

import javax.annotation.Resource;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.hm.common.config.Configuration;
import com.hm.common.util.R;
import com.hm.sys.service.SysSessionService;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 下午9:23:42 2016年3月19日
 * @statement 
 * @team 
 * @email shishun156@gmail.com
 * @describe 容器加载完成，初始化系统参数
 */
@Component
public class WebContentInitListener implements ApplicationListener<ContextRefreshedEvent> {

	@Resource(name="getSysSessionService")
	private SysSessionService sessionService;
	
	public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
		boolean openCas = Configuration.getBoolean(R.SystemInit.FBSM_SYSTEM_CAS);
		if(!openCas){//不是单点登录清除cas表所有的数据
			sessionService.clearAll();
		}		
	}

}
