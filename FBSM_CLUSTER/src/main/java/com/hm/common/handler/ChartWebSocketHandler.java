package com.hm.common.handler;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

/**
 * @author shishun.wang
 * @date 10:44:31 AM Dec 11, 2015
 * @version 1.0
 * @describe 图表数据推送
 */
public class ChartWebSocketHandler implements WebSocketHandler {

	private static final ArrayList<WebSocketSession> users = new ArrayList<WebSocketSession>();
	
	static Logger LOG = Logger.getLogger(ChartWebSocketHandler.class.getName());
	
	public void afterConnectionEstablished(WebSocketSession session)
			throws Exception {
		InetSocketAddress remoteAddress = session.getRemoteAddress();
		LOG.info(remoteAddress.getHostName()+",请求实时图表数据");
		users.add(session);
		TextMessage message2 = new TextMessage(System.currentTimeMillis()+"");
		for (WebSocketSession user : users) {
            try {
                if (user.isOpen()) {
                    user.sendMessage(message2);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
	}

	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		System.out.println("接收到前台数据："+message.toString());
	}

	public void handleTransportError(WebSocketSession session,
			Throwable exception) throws Exception {
		if(session.isOpen()){
            session.close();
        }
        users.remove(session);
	}

	public void afterConnectionClosed(WebSocketSession session,CloseStatus closeStatus) throws Exception {
		users.remove(session);
	}

	public boolean supportsPartialMessages() {
		return false;
	}

}
