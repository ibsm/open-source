package com.hm.common.plugin;

/**
 * @author shishun.wang
 * @version 1.0
 * @date 上午9:21:16 2016年2月17日
 * @statement 
 * @team 
 * @email shishun156@gmail.com
 * @describe oracle
 */
public class OracleDialect extends Dialect{
	
	public boolean supportsLimitOffset(){
		return true;
	}
	
    public boolean supportsLimit() {   
        return true;   
    }  
    
	public String getLimitString(String sql, int offset,String offsetPlaceholder, int limit, String limitPlaceholder) {
		StringBuffer pagingSelect = new StringBuffer(sql.length() + 100);    
        pagingSelect.append("select * from ( select row_.*, rownum rownum_ from ( ");
        pagingSelect.append(sql);    
        pagingSelect.append(" ) row_ ) where rownum_ > ").append(offset).append(" and rownum_ <= ").append(offset + limit);    
        return pagingSelect.toString(); 
	}
}
