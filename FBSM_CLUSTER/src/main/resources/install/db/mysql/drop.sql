/*系统用户表*/
DROP TABLE IF EXISTS `cmdb_sys_user`;
/*系统菜单表*/
DROP TABLE IF EXISTS `cmdb_sys_menu`;
/*系统管理角色表*/
DROP TABLE IF EXISTS `cmdb_sys_role`;
/*字典类型*/
DROP TABLE IF EXISTS `cmdb_sys_dict`;
/*字典实例*/
DROP TABLE IF EXISTS `cmdb_sys_dictins`;
/*系统数据权限*/
DROP TABLE IF EXISTS `cmdb_sys_per`;
/*用户角色*/
DROP TABLE IF EXISTS `cmdb_sys_user_role`;
/*角色系统数据权限*/
DROP TABLE IF EXISTS `cmdb_sys_role_per`;
/*角色菜单权限*/
DROP TABLE IF EXISTS `cmdb_role_menu`;
/*工作流节点定义*/
DROP TABLE IF EXISTS `act_cus_node`;
/*工作流节点和处理人之间关系定义*/
DROP TABLE IF EXISTS `act_cus_node_ref`;
/*用户session控制，单点登录*/
DROP TABLE IF EXISTS `cmdb_sys_session`;

