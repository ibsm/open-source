<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<style type="text/css" media="screen">
body {
	background-color: #FFFFFF;
	margin: 0;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

.container {
	margin: 50px auto 40px auto;
	width: 600px;
	text-align: center;
}

a {
	color: #4183c4;
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}

h1 {
	width: 800px;
	position: relative;
	left: -100px;
	letter-spacing: -1px;
	line-height: 60px;
	font-size: 60px;
	font-weight: 100;
	margin: 0px 0 50px 0;
	text-shadow: 0 1px 0 #fff;
}

p {
	color: rgba(0, 0, 0, 0.5);
	margin: 20px 0;
	line-height: 1.6;
}

ul {
	list-style: none;
	margin: 25px 0;
	padding: 0;
}

li {
	display: table-cell;
	font-weight: bold;
	width: 1%;
}

.logo {
	display: inline-block;
	margin-top: 35px;
}

.logo-img-2x {
	display: none;
}

@media only screen and (-webkit-min-device-pixel-ratio: 2) , only screen and (
		min--moz-device-pixel-ratio: 2) , only screen and (
		-o-min-device-pixel-ratio: 2/1) , only screen and (
		min-device-pixel-ratio: 2) , only screen and ( min-resolution: 192dpi)
		, only screen and ( min-resolution: 2dppx) {
	.logo-img-1x {
		display: none;
	}
	.logo-img-2x {
		display: inline-block;
	}
}

#suggestions {
	margin-top: 35px;
	color: #ccc;
}

#suggestions a {
	color: #666666;
	font-weight: 200;
	font-size: 14px;
	margin: 0 10px;
}
</style>
</head>
<body>
	<div class="container">
		<p>
			<strong>404</strong>
		</p>
		<p>
			<em>原因:系统检测到你访问的页面不存在,系统将在<span id='downnum' style='color:red;padding:3px;font-size:16px;'>5</span>秒钟跳转</em>
		</p>
		<p>
			如需要快速跳转,点击<a href="javascript:jump();" style='color:red;padding:3px;'>跳转</a>
		</p>
	</div>
	<script type="text/javascript">
		var count = 5;
		function jump(){
			top.location.href='login.do?code='+Math.random();
		}
		
		window.setInterval(function(){ 
			count --;
			if(count == 1){
				document.getElementById('downnum').innerHTML = count;
				jump();
			}else if(count > 1){
				document.getElementById('downnum').innerHTML = count;
			} 
		}, 1000);
	</script>
</body>
</html>