<%@page import="com.hm.common.util.R.DictType"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://hm-ssw.do" prefix="hm"%> 

<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<p style="color:#23527C;"><span style="font-size: 14px;padding-right: 10px;color:black;">说明:</span>权限管理。</p>
	   </div>
	</div>
</div>

<div class="col-md-12">
   	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">查询条件</h3>
	   </div>
	   <div class="panel-body">
	   		
	   		<div class="form-inline">
	   		  <hm:DictInstance placeholder="权限类型" dictType="<%=DictType.PER_TYPE %>" name="preType" id="preType" cssClass="span3" all="--请选择权限类型--" style="width:150px;height:24px;"/>
			  <input type="text" class="span3" placeholder="名称" id="name">
			  <button class="btn btn-default btn-sm active" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="search()">搜索</button>
			</div>
	   		
	   </div>
	</div>
</div>

<div class="col-md-12">
	<div class="btn-group">
	  <button type="button" class="btn btn-default" onclick="addRow()">添加</button>
	  <button type="button" class="btn btn-default" onClick="printContent()">打印</button>
	  <button type="button" class="btn btn-default">批量删除</button>
	  <div class="btn-group-vertical">
	    <button type="button" class="btn btn-default dropdown-toggle" 
	      data-toggle="dropdown" style="border-left:0px;">更多<span class="caret"></span>
	    </button>
	    <ul class="dropdown-menu">
	      <li><a href="#">导出World</a></li>
	      <li><a href="#">导出Excel</a></li>
	      <li><a href="#">导出PDF</a></li>
	    </ul>
	  </div>
	</div>
	<table id="jqGrid" data-toggle="context"></table>
	<div id="jqGridPager"></div>
</div>

<script type="text/javascript"> 

	function addRow(){
		openWindow('sys/per/add.do','添加权限',480,320,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'添加成功');
				layer.close(index);
				search();
			});
		});
	}
	
	function editRow(id){
		openWindow('sys/per/update.do?id='+id,'修改权限',480,320,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'修改成功');
				layer.close(index);
				search();
			});
		});
	}
	
	function delRow(id){
		cusConfirm(null,'删除提示','你真的要删除该数据？',function(){
			senSynReq('sys/per/saveDelete.do?msg=删除失败&id='+id,null,function(msg){
				opStateMsgTip(msg,'删除成功');
				search();
			});
    	});
	}
	
	function detailRow(id){
		openDetailWindow('sys/per/detail.do?id='+id,'权限详情',480,320);
	}

	function search(){
		var queryData = {
			'name':$('#name').val(),
			'preType':$('#preType').val()
		};
		$("#jqGrid").jqGrid("setGridParam",{ postData: queryData }).trigger("reloadGrid");
	}
	
	$(document).ready(function () {
		
		$("#jqGrid").jqGrid({
	        url: 'sys/per/queryList.do',
	        mtype: 'POST',
	        datatype: "json",
	        colModel: [			
				{ label: '编号', name: 'id', width: 45, key: true,hidden:true },
				{ label: '名称', name: 'name', width: 75 },
				{ label: '权限类别', name: 'perType', width: 75 },
				{ label: '编码', name: 'code', width: 75 },
				{ label: '描述', name: 'note', width: 90 },
				{ label: '创建时间', name: 'createTime', width: 90 },
				{ label: '操作', name: 'id', width: 90,
					formatter:function(cellvalue, options, rowObject){
						var format = '';
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:editRow(\''+cellvalue+'\')">编辑</a>';
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:delRow(\''+cellvalue+'\')">删除</a>';
							format += '<a style="padding-left:10px;cursor: pointer;" href="javascript:detailRow(\''+cellvalue+'\')">详情</a>';
						if(format.length == 0){
							format = '不可操作';
						}
						return format;
					}
				}
	        ],
			loadonce: false,
			viewrecords: true,
			autowidth : true,
			styleUI : "Bootstrap",
			width : '100%',
			height : 200,
	        rowNum: 10,
	        rowList : [5,10,15],
	        rownumbers: true, 
	        rownumWidth: 22, 
	        multiselect: true,
	        pager: "#jqGridPager",
	        jsonReader: {
	        	repeatitems : false
	        }
	    });
	});

</script>