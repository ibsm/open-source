<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include  file="../../global/dialog/head_general.jsp"%>
	<style>
		*{
			font-family: "Microsoft YaHei" ! important;
		}
		#form_layout4table{
			margin-top:20px;
			width: :100%;
		}
		#form_layout4table td{
			text-align: right;
			padding:5px;
		}
	
		#form_layout4table td:first-child{
			padding-right:20px;
		}
		
		#form_layout4table td:nth-child(2){
			padding-right:20px;
		}
		
		input.error { 
			border: 1px solid red; 
		}
		
		span.error{
			color:red;
			font-size:11px;
		}
	</style>
	<script type="text/javascript" src="resource/ui/plugins/validate/jquery.validate.js"></script>
</head>
<body>
<div class="container">
	
	<form id="form">
		<table id="form_layout4table" style="width: 100%">
			<tr>
				<td width="30%">名&nbsp;&nbsp;&nbsp;称:</td>
				<td width="70%"><input type="text" name="name" class="form-control input-sm" minlength="2" required/></td>
			</tr>
			<tr>
				<td width="30%">类&nbsp;&nbsp;&nbsp;型:</td>
				<td width="70%">
					<select name="dictType" class="form-control" required>
				        <c:forEach var="item" items="${dictTypes }">
				        	<option value="${item.id }">${item.name }</option>
				        </c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td width="30%">排&nbsp;&nbsp;&nbsp;序:</td>
				<td width="70%"><input type="number" name="sort" class="form-control input-sm" required onkeyup="if(!/^\d+$/.test(this.value)) {alert('只能输入数字 !'); this.value=this.value.replace(/[^\d]+/g,'');}" value="0"/></td>
			</tr>
			<tr>
				<td width="30%">系统标配:</td>
				<td width="70%" style="text-align: left;">
					<label class="checkbox-inline">
				      <input type="radio" name="system" value="0" checked> 否
				   </label>
				   <label class="checkbox-inline">
				      <input type="radio" name="system" value="1"> 是
				   </label>
				</td>
			</tr>
			<tr>
				<td width="30%">描&nbsp;&nbsp;&nbsp;述:</td>
				<td width="70%"><textarea name="note" class="form-control input-sm"></textarea></td>
			</tr>
		</table>
	</form>
	
	<script type="text/javascript">
		function validate(){
			return $("#form").validate({
				rules: {
					name: {
						required: true,
						minlength: 2
					},
					dictType:{
						required: true,
						minlength: 1
					},
					sort:{
						required: true,
						minlength: 1
					}
				},
				messages: {
					name: {
						required: "<span class='error'>请输入名称</span>",
						minlength: "<span class='error'>名称最少是2个字符</span>"
					},
					dictType:{
						required: "<span class='error'>请输入字典类型</span>",
						minlength: "<span class='error'>字典类型最少是1个字符</span>"
					},
					sort:{
						required: "<span class='error'>请输入字典排列顺序</span>",
						minlength: "<span class='error'>排列顺序最少是1个字符</span>"
					}
				}
			}).form();
		}
		
		function submit(index,call){
			var msg = $.ajax({
		         type : "post",  
		         url : "sys/dictIns/saveAdd.do?msg=添加失败",
		         data : $('#form').serialize(), 
		         async : false 
		    }).responseText;
			layer.close(index);
			call(jQuery.parseJSON(msg));
		}
	</script>
	
</div>
<%@include  file="../../global/dialog/footer_general.jsp"%>
