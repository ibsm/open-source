<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include  file="../../global/dialog/head_general.jsp"%>
	<style>
		*{
			font-family: "Microsoft YaHei" ! important;
		}
		#form_layout4table{
			margin-top:20px;
			width: :100%;
		}
		#form_layout4table td{
			text-align: right;
			padding:5px;
		}
	
		#form_layout4table td:first-child{
			padding-right:20px;
		}
		
		#form_layout4table td:nth-child(2){
			padding-right:20px;
		}
	</style>
</head>
<body>
<div class="container">
	
	<form id="form">
		<table id="form_layout4table" style="width: 100%">
			<tr>
				<td width="30%">名&nbsp;&nbsp;&nbsp;称:</td>
				<td width="70%"><input type="text" name="name" class="form-control input-sm" minlength="2" required value="${result.name }"/></td>
			</tr>
			<tr>
				<td width="30%">类&nbsp;&nbsp;&nbsp;型:</td>
				<td width="70%"><input type="text" name="dictType" class="form-control input-sm" value="${result.dictType }"/></td>
			</tr>
			<tr>
				<td width="30%">排&nbsp;&nbsp;&nbsp;序:</td>
				<td width="70%"><input type="number" name="sort" class="form-control input-sm" required onkeyup="if(!/^\d+$/.test(this.value)) {alert('只能输入数字 !'); this.value=this.value.replace(/[^\d]+/g,'');}" value="${result.sort }" disabled="disabled"/></td>
			</tr>
			<tr>
				<td width="30%">系统标配:</td>
				<td width="70%" style="text-align: left;">
					<label class="checkbox-inline">
						<c:if test="${result.system == 0 }">否</c:if>
						<c:if test="${result.system == 1 }">是</c:if>
					</label>
				</td>
			</tr>
			<tr>
				<td width="30%">描&nbsp;&nbsp;&nbsp;述:</td>
				<td width="70%"><textarea name="note" class="form-control input-sm" disabled="disabled">${result.note }</textarea></td>
			</tr>
		</table>
	</form>
	
	<script type="text/javascript">
		$(function(){
			 $("input:text").attr("disabled",true);
		});
	</script>
	
</div>
<%@include  file="../../global/dialog/footer_general.jsp"%>
