<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<h4 style="font-size: 14px;">说明</h4>
	   		<p style="color:#23527C;">任务管理。</p>
	   </div>
	</div>
</div>


<div class="col-md-12">
   	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">查询条件<button class="btn btn-default btn-sm pull-right" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="add()">新增</button></h3>
	   </div>
	   <div class="panel-body">
	   		
	   		<form class="form-inline">
			  <input type="text" class="span3" placeholder="名称" id="userName">
			  
			  <button class="btn btn-default btn-sm active" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="search()">搜索</button>
			</form>
	   		
	   </div>
	</div>
</div>


<div id="context-menu">
  <ul class="dropdown-menu" role="menu">
    <li><a tabindex="-1" href="#">Action</a></li>
    <li><a tabindex="-1" href="#">Separated link</a></li>
  </ul>
</div>

<div class="col-md-12">
	<table id="jqGrid" data-toggle="context"></table>
	<div id="jqGridPager"></div>
</div>

<script type="text/javascript"> 

	function delData(id,batch){
		delGridRow("workflow/demo/leave/saveOpData.do?opType=3&isBatch="+batch+"&id="+id);
	}
	
	function search(){
		var queryData = {
			'name':$('#userName').val()
		};
		$("#jqGrid").jqGrid("setGridParam",{ postData: queryData }).trigger("reloadGrid");
	}

	$(document).ready(function () {
		$('#jqGrid').contextmenu({
	        target: '#context-menu2',
	        onItem: function (context, e) {
	        	var grid = $("#jqGrid");
	            var rowKey = grid.getGridParam("selrow");
	            if (!rowKey){
	            	error('至少选中一行数据');
	            }else {
	                var selectedIDs = grid.getGridParam("selarrrow");
	                var result = "";
	                for (var i = 0; i < selectedIDs.length; i++) {
	                    //result += "'"+selectedIDs[i] + "',";
	                	 result += selectedIDs[i] + ",";
	                }
	                if($(e.target).text() == '删除选中'){
	                	delData(result,true);
	                }
	            }
	          
	        }
	     });
		
		$("#jqGrid").jqGrid({
	        url: 'workflow/demo/leaveTask/queryList.do',
	        mtype: 'POST',
	        datatype: "json",
	        colModel: [			
				{ label: '编号', name: 'id', width: 45, key: true },
				{ label: '任务名称', name: 'name', width: 75 },
				{ label: '任务办理人', name: 'assignee', width: 90 },
				{ label: '创建时间', name: 'createTime', width: 90 },
				{ label: '流程实例ID', name: 'processInstanceId', width: 90},
				{ label: '操作', name: 'id', width: 90,
					formatter:function(cellvalue, options, rowObject){
						var str = '<a href="javascript:manageTask(\''+cellvalue+'\')">办理任务</a><a href="javascript:delData(\''+cellvalue+'\',false)" style="margin-left:10px;">查看当前流程</a>';
						return str;
					}
				}
	        ],
			loadonce: false,
			viewrecords: true,
			autowidth : true,
			styleUI : "Bootstrap",
			width : '100%',
			height : 200,
	        rowNum: 10,
	        rowList : [5,10,15],
	        rownumbers: true, 
	        rownumWidth: 22, 
	        multiselect: true,
	        pager: "#jqGridPager",
	        jsonReader: {
	        	repeatitems : false
	        }
	    });
	});
	
	
	function add(){
		var tokenTag = true;
		layer.open({
		    type: 2,
		    area: ['420px', '320px'],
		    fix: false, //不固定
		    title:'添加请假单',
		    content: 'workflow/demo/leave/jump.do?opType=1',
		    btn: ['确定', '取消'],
		    yes: function(index, layero){
		    	var iframeWin = window[layero.find('iframe')[0]['name']];
		    	var valid = iframeWin.validate();
		    	if(valid && tokenTag){
		    		tokenTag = false;
		    		var data = iframeWin.saveSysUser();
		    		close(data,index,"添加成功");
		    		search();
		    	}
		    },
		    cancel: function(index){
		    }
		});
	}
	
	function update(id){
		var tokenTag = true;
		layer.open({
		    type: 2,
		    area: ['420px', '320px'],
		    fix: false, //不固定
		    title:'修改请假单',
		    content: 'workflow/demo/leave/jump.do?opType=2&id='+id,
		    btn: ['确定', '取消'],
		    yes: function(index, layero){
		    	var iframeWin = window[layero.find('iframe')[0]['name']];
		    	var valid = iframeWin.validate();
		    	if(valid && tokenTag){
		    		tokenTag = false;
		    		var data = iframeWin.saveSysUser();
		    		close(data,index,"修改成功");
		    		search();
		    	}
		    },
		    cancel: function(index){
		    }
		});
	}
	
	function manageTask(taskId){
		var tokenTag = true;
		layer.open({
		    type: 2,
		    area: ['520px', '400px'],
		    fix: false, //不固定
		    title:'我的任务管理',
		    content: 'workflow/demo/leaveTask/manageTask.do?id='+taskId,
		    btn: ['确定', '取消'],
		    yes: function(index, layero){
		    	var iframeWin = window[layero.find('iframe')[0]['name']];
		    	var valid = iframeWin.validate();
		    	if(valid && tokenTag){
		    		tokenTag = false;
		    		var data = iframeWin.saveData();
		    		close(data,index,"任务办理成功");
		    		search();
		    	}
		    },
		    cancel: function(index){
		    }
		});
	}
</script>













