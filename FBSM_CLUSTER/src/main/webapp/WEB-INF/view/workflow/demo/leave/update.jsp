<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include  file="../../../global/dialog/head_general.jsp"%>
	<style>
		#form_layout4table{
			margin-top:20px;
			width: :100%;
		}
		#form_layout4table td{
			text-align: right;
			padding:5px;
		}
	
		#form_layout4table td:first-child{
			padding-right:20px;
		}
		
		#form_layout4table td:nth-child(2){
			padding-right:20px;
		}
		
		input.error { 
			border: 1px solid red; 
		}
		
		span.error{
			color:red;
			font-size:11px;
		}
	</style>
	<script type="text/javascript" src="resource/ui/plugins/validate/jquery.validate.js"></script>
</head>
<body>
<div class="container">
	
	
	<form id="updateForm">
		<input type="hidden" value="${result.id }" name="id"/>
		<input type="hidden" value="${result.processState }" name="processState"/>
		<table id="form_layout4table" style="width: 100%">
			<tr>
				<td width="30%">请假天数:</td>
				<td width="70%"><input type="text" name="days" class="form-control input-sm" minlength="1" required value="${result.days }"/></td>
			</tr>
			<tr>
				<td width="30%">请假内容:</td>
				<td width="70%"><input type="text" name="content" class="form-control input-sm" value="${result.content }"/></td>
			</tr>
			<tr>
				<td width="30%">备注:</td>
				<td width="70%"><textarea name="remark" class="form-control input-sm">${result.remark }</textarea></td>
			</tr>
		</table>
	</form>
	
	<script type="text/javascript">
	
		function validate(){
			return $("#updateForm").validate({
				rules: {
					name: {
						required: true,
						minlength: 2
					}
				},
				messages: {
					days: {
						required: "<span class='error'>请输入请假天数</span>",
						minlength: "<span class='error'>名称最少是1个字符</span>"
					}
				}
			}).form();
		}
		
		function saveSysUser(){
			var msg = $.ajax({
		         type : "post",  
		         url : "workflow/demo/leave/saveOpData.do?opType=2",
		         data : $('#updateForm').serialize(), 
		         async : false 
		     }).responseText;
			return jQuery.parseJSON(msg);
		}
	</script>
	
</div>
<%@include  file="../../../global/dialog/footer_general.jsp"%>
