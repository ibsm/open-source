<%@page import="com.hm.common.StaticNature.DictTypeCofig"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="hm" uri="http://hm-ssw.do"%>
<%@include  file="../../../global/dialog/head_general.jsp"%>
	<style>
		#form_layout4table{
			margin-top:20px;
			width: :100%;
		}
		#form_layout4table td{
			text-align: right;
			padding:5px;
		}
	
		#form_layout4table td:first-child{
			padding-right:20px;
		}
		
		#form_layout4table td:nth-child(2){
			padding-right:20px;
		}
		
		input.error { 
			border: 1px solid red; 
		}
		
		span.error{
			color:red;
			font-size:11px;
		}
	</style>
	<script type="text/javascript" src="resource/ui/plugins/validate/jquery.validate.js"></script>
</head>
<body>
<div class="container">
	
	
	<form id="myForm">
		<table id="form_layout4table" style="width: 100%">
			<tr>
				<td width="30%">名&nbsp;&nbsp;&nbsp;称:</td>
				<td width="70%"><input type="text" name="name" class="form-control input-sm" minlength="1" required/></td>
			</tr>
			<tr>
				<td width="30%">类&nbsp;&nbsp;&nbsp;别:</td>
				<td width="70%"><hm:DictInstance dictType="<%=DictTypeCofig.processType4deploy %>" name="category"/></td>
			</tr>
			<tr>
				<td width="30%">流&nbsp;&nbsp;&nbsp;程:</td>
				<td width="70%"><input type="text" id="flowFile" name="path" class="form-control input-sm" minlength="1" required onclick="uploadFile()"/></td>
			</tr>
		</table>
	</form>
	
	<script type="text/javascript">
	
		function uploadFile(){
			window.parent.uploadFile4general(function(value){
				$('#flowFile').val(value);
			});
		}
	
		function validate(){
			return $("#myForm").validate({
				rules: {
					name: {
						required: true,
						minlength: 1
					}
				},
				messages: {
					name: {
						required: "<span class='error'>请输入名称</span>",
						minlength: "<span class='error'>名称最少是1个字符</span>"
					}
				}
			}).form();
		}
		
		function deployProcess(){
			var msg = $.ajax({
		         type : "post",  
		         url : "workflow/common/deploy/saveOpData.do?opType=1",
		         data : $('#myForm').serialize(), 
		         async : false 
		     }).responseText;
			return jQuery.parseJSON(msg);
		}
	</script>
	
</div>
<%@include  file="../../../global/dialog/footer_general.jsp"%>
