<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<p style="color:#23527C;"><span style="font-size: 14px;padding-right: 10px;color:black;">说明:</span>部属流程定义</p>
	   </div>
	</div>
</div>

<div class="col-md-12">
   	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">查询条件</h3>
	   </div>
	   <div class="panel-body">
	   		
	   		<form class="form-inline">
			  <input type="text" class="span3" placeholder="名称" id="userName">
			  
			  
			  <button class="btn btn-default btn-sm active" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="search()">搜索</button>
			</form>
	   		
	   </div>
	</div>
</div>

<div class="col-md-12">
	<div class="btn-group">
	  <button type="button" class="btn btn-default" onclick="addRow()">添加</button>
	  <button type="button" class="btn btn-default" onClick="printContent()">打印</button>
	  <button type="button" class="btn btn-default">批量删除</button>
	  <div class="btn-group-vertical">
	    <button type="button" class="btn btn-default dropdown-toggle" 
	      data-toggle="dropdown" style="border-left:0px;">更多<span class="caret"></span>
	    </button>
	    <ul class="dropdown-menu">
	      <li><a href="#">导出World</a></li>
	      <li><a href="#">导出Excel</a></li>
	      <li><a href="#">导出PDF</a></li>
	    </ul>
	  </div>
	</div>
	<table id="jqGrid" data-toggle="context"></table>
	<div id="jqGridPager"></div>
</div>

<script type="text/javascript">
	
	function addRow(){
		openWindow('wflow/deploy/addDeploy.do','添加部署流程定义',480,260,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'部署流程定义成功');
				layer.close(index);
				search();
			});
		});
	}
	
	function editRow(id){
		openWindow('sys/dictType/update.do?id='+id,'修改字典类型',480,240,function(index,window){
			window.submit(index,function(data){
				opStateMsgTip(data,'修改成功');
				layer.close(index);
				search();
			});
		});
	}
	
	function delRow(id){
		cusConfirm(null,'删除提示','你真的要删除该数据？',function(){
			senSynReq('wflow/deploy/saveDeleteDeployProcess.do?msg=删除失败&id='+id,null,function(msg){
				opStateMsgTip(msg,'删除成功');
				search();
			});
		});
	}
	
	function detailRow(id){
		openDetailWindow('sys/dictType/detail.do?id='+id,'字典类型详情',480,240);
	}

	function search(){
		var queryData = {
			'name':$('#userName').val()
		};
		$("#jqGrid").jqGrid("setGridParam",{ postData: queryData }).trigger("reloadGrid");
	}
	
	$(document).ready(function () {
		
		$("#jqGrid").jqGrid({
	        url: 'wflow/deploy/queryDeployProcessList.do',
	        mtype: 'POST',
	        datatype: "json",
	        colModel: [			
				{ label: '编号', name: 'id', width: 45, key: true },
				{ label: '名称', name: 'name', width: 75 },
				{ label: '类型', name: 'category', width: 90 },
				{ label: '时间', name: 'deploymentTime', width: 90 },
				{ label: '操作', name: 'id', width: 90,
					formatter:function(cellvalue, options, rowObject){
						return '<a href="javascript:delRow(\''+cellvalue+'\')" style="margin-left:10px;">删除</a>';
					}
				}
	        ],
			loadonce: false,
			viewrecords: true,
			autowidth : true,
			styleUI : "Bootstrap",
			width : '100%',
			height : 200,
	        rowNum: 10,
	        rowList : [5,10,15],
	        rownumbers: true, 
	        rownumWidth: 22, 
	        multiselect: false,
	        pager: "#jqGridPager",
	        jsonReader: {
	        	repeatitems : false
	        },
	        subGrid: true,
	        subGridOptions: {
	              "plusicon"  : "ui-icon-triangle-1-e",
	              "minusicon" : "ui-icon-triangle-1-s",
	              "openicon"  : "ui-icon-arrowreturn-1-e",
		          // load the subgrid data only once
		          // and the just show/hide
		          "reloadOnExpand" : false,
		          // select the row when the expand column is clicked
		          "selectOnExpand" : true
	        },
	        subGridRowExpanded: function(subgrid_id, row_id) {
	            var subgrid_table_id, pager_id;
	            subgrid_table_id = subgrid_id+"_t";
	            pager_id = "p_"+subgrid_table_id;
	            $("#"+subgrid_id).html("<table id='"+subgrid_table_id+"' class='scroll'></table><div id='"+pager_id+"' class='scroll'></div>");
	            jQuery("#"+subgrid_table_id).jqGrid({
		              url:"wflow/deploy/queryDeployProcessListByDeployment.do?id="+row_id,
		              datatype: "json",
		              colNames: ['定义编号','名称','流程KEY','流程版本','流程资源描述','流程资源名称','查看流程图'],
		              colModel: [
			                {name:"id",index:"id"},
			                {name:"name",index:"name"},
			                {name:"key",index:"key"},
			                {name:"version",index:"version"},
			                {name:"resourceName",index:"resourceName"},
			                {name:"diagramResourceName",index:"diagramResourceName"},
			                {name:"deploymentId",index:"deploymentId",
			                	formatter:function(cellvalue, options, rowObject){
			                		return "<a target='_blank' href='wflow/deploy/outPutFlowPic.do?deploymentId="+cellvalue+"&resourceName="+rowObject.diagramResourceName+"'>查看</a>";
			                	}
			                }
		              ],
		              styleUI : "Bootstrap",
		              viewrecords: true,
	  				  autowidth : true,
	  				  styleUI : "Bootstrap",
	  			      width : '100%',
	                  rowNum:10,
	                  pager: pager_id,
	                  height: '100%'
	            });
	            jQuery("#"+subgrid_table_id).jqGrid('navGrid',"#"+pager_id,{search:false,edit:false,add:false,del:false})
	        }
	    });
	});

</script>