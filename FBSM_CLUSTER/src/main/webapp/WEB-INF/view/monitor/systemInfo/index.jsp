<%@page import="com.hm.common.util.SystemInfo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>

<%
SystemInfo info = SystemInfo.getInstance(request);
%>
<h1>服务器tomcat存放IP地址:<span style="color: red;"><%=info.getOs_ip()+":"+request.getLocalPort() %></span></h1>
<hr/>
<%=info.toString() %>
</body>
</html>