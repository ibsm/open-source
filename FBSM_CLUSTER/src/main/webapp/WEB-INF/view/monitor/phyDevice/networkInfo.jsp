<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="resource/ui/plugins/charts/com/www/js/echarts.js"></script>
<style>
.cus-chart:HOVER{
	border:1px solid red;
	cursor:pointer;
/* 	box-shadow: 5px 5px 2px #888888; */
}
.form_layout4table{
	margin-top:0px;
	width: :100%;
}
.form_layout4table td{
	text-align: left;
	padding:5px;
}

.form_layout4table td:first-child{
	padding-right:20px;
}

.form_layout4table td:nth-child(2){
	padding-right:20px;
}

</style>

<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<p style="color:#23527C;"><span style="font-size: 14px;padding-right: 10px;color:black;">说明:</span>网络设备性能监控</p>
	   </div>
	</div>
</div>

<div class="col-md-12">
   	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">执行监控计划<button class="btn btn-default btn-sm active pull-right" style="height:15px;padding-bottom:16px;line-height:14px;" onclick="execute()">执行</button></h3>
	   </div>
	   <div class="panel-body">
	   		
	   		<form id="form" class="form-inline">
	   		  <label>所属网卡</label>
	   		  <select class="span3" placeholder="所属网卡" style="width:150px;height:24px;" name="belongNetworkCard">
	   		  	<c:forEach var="type" items="${networkCardTypes }">
	   		  		<option value="${type.name }">${type.description }</option>
	   		  	</c:forEach>
	   		  </select>
	   		  <label style="margin-left:20px;">执行时长(分钟)</label>
			  <input type="number" class="span3" placeholder="执行时长(分钟)" value="1" id="executeTime" name="executeTime" onkeyup="if(!/^\d+$/.test(this.value)) {alert('只能输入数字 !'); this.value=this.value.replace(/[^\d]+/g,'');}"/>
			  <label style="margin-left:20px;">执行间隔时间(豪秒)</label>
			  <input type="number" class="span3" placeholder="间隔时间(每次统计间隔时间单位:毫秒)" value="3000" id="executeIntervalTime" name="executeIntervalTime" onkeyup="if(!/^\d+$/.test(this.value)) {alert('只能输入数字 !'); this.value=this.value.replace(/[^\d]+/g,'');}"/>
			</form>
	   </div>
	</div>
</div>
<div id="networkBaseInfoContent"></div>
<script>
	function execute(){
		var executeTime = $('#executeTime').val();
		var executeIntervalTime = $('#executeIntervalTime').val();
		if(executeTime < 1){
			error("执行时长不能小于1分钟");
			return;
		}
		if(executeIntervalTime < 3000){
			error("每次查询数据执行时间间隔不能小于3000");
			return;
		}
		
		$.ajax({
			type:'post',
			url:'monitor/phyDevice/networkDetailInfo.do',
			data : $('#form').serialize(), 
			success: function(data){
				$('#networkBaseInfoContent').html('');
				$('#networkBaseInfoContent').html(data);
			}
		});
	}
</script>



