<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
.cus-chart:HOVER{
	border:1px solid red;
	cursor:pointer;
/* 	box-shadow: 5px 5px 2px #888888; */
}

#form_layout4table{
	margin-top:0px;
	width: :100%;
}
#form_layout4table td{
	text-align: left;
	padding:5px;
}

#form_layout4table td:first-child{
	padding-right:20px;
}

#form_layout4table td:nth-child(2){
	padding-right:20px;
}
</style>
<script src="resource/ui/plugins/charts/com/www/js/echarts.js"></script>

<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<p style="color:#23527C;"><span style="font-size: 14px;padding-right: 10px;color:black;">说明:</span>CPU设备性能监控</p>
	   </div>
	</div>
</div>

<div class="col-md-12">
	<c:forEach var="cpu" items="${cpuChartInfos }">
		<div class="col-md-3">
		   	<div class="panel panel-default cus-chart" style="background: #E9E9E9;">
		   		${cpu }
			</div>
		</div>
	</c:forEach>
</div>


<div class="col-md-12">
	<div class="panel panel-default">
	   <div class="panel-heading">
	      <h3 class="panel-title">CPU概览</h3>
	   </div>
	   <div class="panel-body">
	   		<div class="col-md-5">
				<fieldset>
					<legend>基本信息</legend>
					<table id="form_layout4table" style="width: 100%">
						<tr>
							<td width="30%">CPU个数:</td>
							<td width="70%" class="cus-details-info" colspan="3">${cpuCount }</td>
						</tr>
						<tr>
							<td width="30%">CPU的总量MHz:</td>
							<td width="70%" class="cus-details-info">${cpuInfo.mhz }</td>
						</tr>
						<tr>
							<td width="30%">CPU厂商:</td>
							<td width="70%" class="cus-details-info">${cpuInfo.vendor }</td>
						</tr>
						<tr>
							<td width="30%">CPU的类别:</td>
							<td width="70%" class="cus-details-info">${cpuInfo.model }</td>
						</tr>
						<tr>
							<td width="30%">缓冲存储器数量:</td>
							<td width="70%" class="cus-details-info">${cpuInfo.cacheSize }</td>
						</tr>
					</table>
				</fieldset>
			</div>
			<div class="col-md-7">
				<fieldset>
					<legend>使用率详情</legend>
					<table id="jqGrid" data-toggle="context"></table>
				</fieldset>
			</div>
	   </div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("#jqGrid").jqGrid({
	        url: 'monitor/phyDevice/queryCpuInfoList.do',
	        mtype: 'POST',
	        datatype: "json",
	        colModel: [			
				{ label: '用户使用率', name: 'user', width: 45, key: true },
				{ label: '系统使用率', name: 'sys', width: 75 },
				{ label: '当前等待率', name: 'wait', width: 90 },
				{ label: '文件系统类型2', name: 'nice', width: 90 ,hidden:true},
				{ label: '当前空闲率', name: 'idle', width: 90 },
				{ label: '总的使用率', name: 'combined', width: 90 }
	        ],
			loadonce: false,
			viewrecords: true,
			autowidth : true,
			styleUI : "Bootstrap",
			width : '100%',
			height : 160,
			setGridHeight:'auto',
	        rowNum: 1000,
	        rownumbers: true, 
	        rownumWidth: 22, 
	        jsonReader: {
	        	repeatitems : false
	        }
	    });
	})
</script>
