<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
.cus-chart:HOVER{
	border:1px solid red;
	cursor:pointer;
/* 	box-shadow: 5px 5px 2px #888888; */
}
#form_layout4table{
	margin-top:0px;
	width: :100%;
}
#form_layout4table td{
	text-align: left;
	padding:5px;
}

#form_layout4table td:first-child{
	padding-right:20px;
}

#form_layout4table td:nth-child(2){
	padding-right:20px;
}

.cus-details-info{
	color:green;
}
</style>
<script src="resource/ui/plugins/charts/com/www/js/echarts.js"></script>

<div class="col-md-12">
   	<div class="panel panel-default" style="border-left:2px solid red;background: #E9E9E9;">
	   <div class="panel-body" style="padding:5px 5px 0px 15px;">
	   		<p style="color:#23527C;"><span style="font-size: 14px;padding-right: 10px;color:black;">说明:</span>物理设备性能监控,概览</p>
	   </div>
	</div>
</div>
<div class="col-md-4">
   	<div class="panel panel-default cus-chart" style="background: #E9E9E9;" onclick="window.location.href='#!monitor/phyDevice/diskInfo.do';">
	   ${dickInfo }
	</div>
</div>
<div class="col-md-4">
   	<div class="panel panel-default cus-chart" style="background: #E9E9E9;" onclick="window.location.href='#!monitor/phyDevice/memoryInfo.do';">
	   ${memoryInfo }
	</div>
</div>
<div class="col-md-4">
   	<div class="panel panel-default cus-chart" style="background: #E9E9E9;" onclick="window.location.href='#!monitor/phyDevice/cpuInfo.do';">
	   ${cpuInfo }
	</div>
</div>
<div class="col-md-12">
   	<div class="panel panel-default" style="background: #E9E9E9;">
   		<div class="panel-heading">
	      <h3 class="panel-title">硬件配置详情</h3>
	   </div>
	 	<div class="panel-body" style="padding:5px 5px 0px 15px;height: 400px;">
	 		<fieldset style="margin:0px;padding: 0px;">
    			<legend style="margin:0px;padding: 0px;font-size: 16px;">1、基本信息</legend>
				<table id="form_layout4table" style="width: 100%">
					<tr>
						<td width="10%">计算机名:</td>
						<td width="15%" class="cus-details-info">${platformInfo.osName }</td>
						<td width="10%">内核类型(1):</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.arch }</td>
						<td width="10%">内核类型(2):</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.cpuEndian }</td>
						<td width="10%">内核类型(3):</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.dataModel }</td>
					</tr>
					<tr>
						<td width="10%">系统描述(1):</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.description }</td>
						<td width="10%">系统描述(2):</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.machine }</td>
						<td width="10%">操作系统类型(1):</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.name }</td>
						<td width="10%">操作系统类型(2):</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.patchLevel }</td>
					</tr>
					<tr>
						<td width="10%">操作系统所属卖主:</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.vendor }</td>
						<td width="10%">卖主名称:</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.vendorCodeName }</td>
						<td width="10%">操作系统名称:</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.vendorName }</td>
						<td width="10%">操作系统卖主类型:</td>
						<td width="15%" class="cus-details-info">${platformInfo.operatingSystem.vendorVersion }</td>
					</tr>
					<tr>
						<td width="10%">操作系统版本号:</td>
						<td width="95%" colspan="7" class="cus-details-info">${platformInfo.operatingSystem.version }</td>
					</tr>
				</table>
    		</fieldset>
			<hr style="height: 10px;line-height: 10px;margin: 0px;padding: 0px;"/>
			<fieldset style="margin:0px;padding: 0px;">
    			<legend style="margin:0px;padding: 0px;font-size: 16px;">2、当前系统进程表中的用户信息》》</legend>
    			<div style="background: #FFFFFF;">
    				<table id="jqGrid" data-toggle="context"></table>
    			</div>
    		</fieldset>
	 	</div>  
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#jqGrid").jqGrid({
	        url: 'monitor/phyDevice/queryPlatformWhoList.do',
	        mtype: 'POST',
	        datatype: "json",
	        colModel: [			
				{ label: '设备', name: 'device', width: 45, key: true },
				{ label: '当前系统进程用户名', name: 'user', width: 75 },
				{ label: '主机', name: 'host', width: 90 },
				{ label: '时间', name: 'time', width: 90 }
	        ],
			loadonce: false,
			viewrecords: true,
			autowidth : true,
			styleUI : "Bootstrap",
			width : '100%',
			height : 160,
			setGridHeight:'auto',
	        rowNum: 1000,
	        rownumbers: true, 
	        rownumWidth: 22, 
	        jsonReader: {
	        	repeatitems : false
	        }
	    });
	})
</script>