/**
 * 选择数据字典
 * @param obj
 * @param callFun
 */
function selectDictTypeDialog(obj,callFun){
	layer.open({
	    type: 2,
	    area: ['800px', '500px'],
	    fix: false, //不固定
	    title:'选择数据字典类型',
	    //zIndex:20000000,
	    content: 'sys/permission/dictType/selectDictType.do',
	    btn: ['确定', '取消'],
	    yes: function(index, layero){
	    	var iframeWin = window[layero.find('iframe')[0]['name']];
	    	var array = iframeWin.getValue();
	    	if(array.lenth != 0){
	    		layer.close(index);
	    		callFun(array);
	    	}
	    },
	    cancel: function(index){
	    }
	});
}