//---
function getMap(key,value){
	var reg=/,$/gi; 
	var arry = new Array();
	var keys = key.replace(reg,"").split(',');
	var values = value.replace(reg,"").split(',');
	for(var i=0;i<keys.length;i++){
		var obj = new Object();
		obj.key=keys[i];
		obj.value=values[i];
		arry.push(obj);
	}
	return arry; 
}

//信息提示
function cusConfirm(url,title,hint,call){
	var tokenTag = true;
	layer.confirm(hint, {
		icon: 3,
		title:title,
	    //skin: 'layer-ext-moon',
	    skin: 'layui-layer-molv',
	    btn: ['确定','取消'] //按钮
	}, function(index, layero){
		if(null != url){
			var msg = $.ajax({
	   	         type : "post",  
	   	         url : url, 
	   	         async : false 
	   	     }).responseText;
			if(null != call){
				call(jQuery.parseJSON(msg));
			}
		}else{
			if(null != call){
				call();
			}
		}
	});
}


function opStateMsgTip(data,msg){
	if(data.state == 'successful'){
		successful(msg);
	}else{
		error(data.msg);
	}
}

function openWindow(url,title,width,height,call){
	var tokenTag = true;
	layer.open({
	    type: 2,
	    skin: 'layui-layer-molv',
	    area: [width+'px', height+'px'],
	    fix: false, //不固定
	    title:title,
	    content:  [url, 'no'],
	    btn: ['确定', '取消'],
	    yes: function(index, layero){
	    	var iframeWin = window[layero.find('iframe')[0]['name']];
	    	var valid = iframeWin.validate();
	    	if(valid && tokenTag){
	    		tokenTag = false;
	    		call(index,iframeWin);
	    	}
	    },
	    cancel: function(index){
	    }
	});
}

function openDetailWindow(url,title,width,height){
	var tokenTag = true;
	layer.open({
	    type: 2,
	    skin: 'layui-layer-molv',
	    area: [width+'px', height+'px'],
	    fix: false, //不固定
	    title:title,
	    content:  [url, 'no'],
	    btn: ['取消'],
	    cancel: function(index){
	    	
	    }
	});
}

//--------删除数据------
function delGridRow(url){
	var tokenTag = true;
	layer.confirm('你真的要删除该数据？', {
		icon: 3,
	    skin: 'layer-ext-moon',
	    btn: ['确定','取消'] //按钮
	}, function(index, layero){
    	if(tokenTag){
    		tokenTag = false;
    		
    		var msg = $.ajax({
    	         type : "post",  
    	         url : url, 
    	         async : false 
    	     }).responseText;
    		var data = jQuery.parseJSON(msg);
    		
    		close(data,index,"删除成功");
    		search();
    	}
	});
}
//--------删除数据------

function close(data,index,msg){
	layer.open({
	    type: 1,
	    closeBtn: false, //不显示关闭按钮
	    shift: 2,
	    title:false,
	    shadeClose: false, //开启遮罩关闭
	    time: 1000,
	    content: '<span style="background:#999;padding:20px;line-height:30px;font-size:14px;">数据提交中,请稍等..</span>'
	});
	translateMsg(data,msg);
	layer.close(index);
}

function error(msg){
	layer.open({
	    type: 0,
	    icon: 2,
	    title: false,
	    shade:false,
	    closeBtn: 0, //不显示关闭按钮
	    btn:[],
	    offset: 0,
	    time: 800, //2秒后自动关闭
	    shift: 0,
	    content: msg
	});
}

function warning(msg){
	layer.open({
	    type: 0,
	    icon: 3,
	    title: false,
	    shade:false,
	    closeBtn: 0, //不显示关闭按钮
	    btn:[],
	    offset: 0,
	    time: 800, //2秒后自动关闭
	    shift: 0,
	    content: msg
	});
}

function successful(msg){
	layer.open({
	    type: 0,
	    icon: 1,
	    title: false,
	    shade:false,
	    closeBtn: 0, //不显示关闭按钮
	    btn:[],
	    offset: 0,
	    time: 800, //2秒后自动关闭
	    shift: 0,
	    content: msg
	});
}

function senSynReq(url,data,call){
	$.ajax({
		url:url,
		type:'post',
		data:data,
		success:function(content){
			var msg = jQuery.parseJSON(content);
			call(msg);
		}
	});
	/*var msg = $.ajax({
        type : "post",  
        url : url,
        data:data,
        async : false 
	}).responseText;
	
	return jQuery.parseJSON(msg);*/
}

function callAjaxPostBak(url){
	$('#hm_from').attr("action",url);
	$('#hm_from').submit();
}

function openNoBtnsWindow(url,title,width,height){
	layer.open({
	    type: 2,
	    skin: 'layui-layer-molv',
	    area: [width+'px', height+'px'],
	    fix: false, //不固定
	    title:title,
	    content:  [url, 'no']
	});
}
