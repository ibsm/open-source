package com.hm.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.hm.common.util.DataUtil.FtlFactory;

public class TestReport {

	@Test
	public void test() throws Exception{
		String[] args = {"B","C","D","E","F","G","H","I"};
		Map<String,String> mapper = new HashMap<String,String>();
		for (String str : args) {
			mapper.put(str+"7", (Math.random()*100)+"");
		}
		
		mapper.put("cardNumber", "9527");
		mapper.put("mouth", "02");
		mapper.put("time", "2016年01月26日");
		System.out.println(new FtlFactory().rootDirectory("/report").build("etc_demo.xml").process(mapper));
	}
}
